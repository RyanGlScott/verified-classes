{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NoNamedWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Derived.SumSpec where

import Data.Foldable
import Data.Singletons.Base.TH
import Derived.PairSpec (Pair)
import GHC.Generics
import Instances ()
import Laws
import SArbitrary
import Utils
import Test.Hspec
import Test.QuickCheck (oneof)
import VerifiedClasses.Eq
import VerifiedClasses.Ord
import VerifiedClasses.Functor
import VerifiedClasses.Generics
import VerifiedClasses.TH.Generics
import VerifiedClasses.Traversable

$(singletons [d|
  data SumEx a = MkSumEx1
               | MkSumEx2 ()
               | MkSumEx3 a
               | MkSumEx4 (Pair () a)
    deriving stock (Foldable, Show)
  |])

-----
-- Verified instances
-----

deriving instance _ => Generic (SumEx a)
deriving instance _ => Generic1 SumEx
$(verifyGeneric0And1 ''SumEx)

$(concatSequence
  [ deriveEqAll [t| forall a. Eq a => Eq (SumEx a) |]
  , deriveFunctorAll [t| Functor SumEx |]
  , deriveOrdAll [t| forall a. Ord a => Ord (SumEx a) |]
  , deriveTraversableAll [t| Traversable SumEx |]
  ])

-----
-- Miscellaneous instances
-----

instance SArbitrary a => SArbitrary (SumEx a) where
  sArbitrary = oneof [ pure $ SomeSing SMkSumEx1
                     , (`bindSS` SomeSing . SMkSumEx2) <$> sArbitrary
                     , (`bindSS` SomeSing . SMkSumEx3) <$> sArbitrary
                     , (`bindSS` SomeSing . SMkSumEx4) <$> sArbitrary
                     ]

-----
-- Tests
-----

main :: IO ()
main = hspec spec

spec :: Spec
spec = traverse_ propLaws
         [ eqLaws        @(SumEx ())
         , genericLaws   @(SumEx ())
         , ordLaws       @(SumEx ())

         , functorLaws     @SumEx
         , generic1Laws    @SumEx
         , traversableLaws @SumEx
         ]
