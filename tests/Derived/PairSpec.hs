{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Derived.PairSpec where

import Data.Foldable
import Data.Singletons.Base.TH
import GHC.Generics
import Instances ()
import Laws
import SArbitrary
import Test.Hspec
import Utils
import VerifiedClasses.AbelianSemigroup
import VerifiedClasses.Applicative
import VerifiedClasses.Eq
import VerifiedClasses.Ord
import VerifiedClasses.Functor
import VerifiedClasses.Generics
import VerifiedClasses.Monoid
import VerifiedClasses.Semigroup
import VerifiedClasses.TH.Generics
import VerifiedClasses.Traversable

$(singletons [d|
  data Pair a b = MkPair a b
    deriving stock (Foldable, Show)
  |])

-----
-- Verified instances
-----

deriving stock instance Generic  (Pair a b)
deriving stock instance Generic1 (Pair a)
$(verifyGeneric0And1 ''Pair)

$(concatSequence
  [ deriveAbelianSemigroupAll [t| forall a b. (AbelianSemigroup a, AbelianSemigroup b) => AbelianSemigroup (Pair a b) |]
  , deriveApplicativeAll [t| forall a. Monoid a => Applicative (Pair a) |]
  , deriveEqAll [t| forall a b. (Eq a, Eq b) => Eq (Pair a b) |]
  , deriveFunctorAll [t| forall a. Functor (Pair a) |]
  , deriveMonoidAll [t| forall a b. (Monoid a, Monoid b) => Monoid (Pair a b) |]
  , deriveOrdAll [t| forall a b. (Ord a, Ord b) => Ord (Pair a b) |]
  , deriveSemigroupAll [t| forall a b. (Semigroup a, Semigroup b) => Semigroup (Pair a b) |]
  , deriveTraversableAll [t| forall a. Traversable (Pair a) |]
  ])

-----
-- Miscellaneous instances
-----

instance (SArbitrary a, SArbitrary b) => SArbitrary (Pair a b) where
  sArbitrary = liftA2 (\a b -> a `bindSS`
                        \a' -> b `bindSS`
                        \b' -> SomeSing (SMkPair a' b'))
                      sArbitrary sArbitrary

-----
-- Tests
-----

main :: IO ()
main = hspec spec

spec :: Spec
spec = traverse_ propLaws
         [ abelianSemigroupLaws @(Pair () ())
         , eqLaws               @(Pair () ())
         , genericLaws          @(Pair () ())
         , monoidLaws           @(Pair () ())
         , ordLaws              @(Pair () ())
         , semigroupLaws        @(Pair () ())

         , applicativeLaws @(Pair ())
         , functorLaws     @(Pair ())
         , generic1Laws    @(Pair ())
         , traversableLaws @(Pair ())
         ]
