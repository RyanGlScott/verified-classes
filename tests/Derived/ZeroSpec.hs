{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Derived.ZeroSpec where

import Data.Foldable
import Data.Singletons.TH
import GHC.Generics
import Laws
import Prelude.Singletons
import SArbitrary
import Test.Hspec
import Utils
import VerifiedClasses.AbelianSemigroup
import VerifiedClasses.Eq
import VerifiedClasses.Ord
import VerifiedClasses.Functor
import VerifiedClasses.Generics
import VerifiedClasses.Semigroup
import VerifiedClasses.TH.Generics
import VerifiedClasses.Traversable

$(singletons [d|
  data Zero a
    deriving stock (Foldable, Show)
  |])

-----
-- Verified instances
-----

deriving stock instance Generic (Zero a)
deriving stock instance Generic1 Zero
$(verifyGeneric0And1 ''Zero)

$(concatSequence
  [ deriveAbelianSemigroupAll [t| forall a. AbelianSemigroup (Zero a) |]
  , deriveEqAll [t| forall a. Eq (Zero a) |]
  , deriveFunctorAll [t| Functor Zero |]
  , deriveOrdAll [t| forall a. Ord (Zero a) |]
  , deriveSemigroupAll [t| forall a. Semigroup (Zero a) |]
  , deriveTraversableAll [t| Traversable Zero |]
  ])

-----
-- Miscellaneous instances
-----

instance SArbitrary (Zero a) where
  sArbitrary = pure $ SomeSing $ let x = x in x

-----
-- Tests
-----

main :: IO ()
main = hspec spec

-- These test that certain laws don't force evaluation more than necessary.
-- A bit silly, but then again, this /is/ Haskell we're talking about here :)
spec :: Spec
spec = traverse_ propLaws
         [ eqLaws @(Zero ())
         , ordLaws @(Zero ())
         ]
