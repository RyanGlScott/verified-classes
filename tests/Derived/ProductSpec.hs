{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NoNamedWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Derived.ProductSpec where

import Control.Applicative
import Data.Foldable
import Data.Singletons.Base.TH
import Derived.PairSpec (Pair)
import GHC.Generics
import Instances ()
import Laws
import SArbitrary
import Test.Hspec
import Utils
import VerifiedClasses.AbelianSemigroup
import VerifiedClasses.Applicative
import VerifiedClasses.Eq
import VerifiedClasses.Ord
import VerifiedClasses.Functor
import VerifiedClasses.Generics
import VerifiedClasses.Monoid
import VerifiedClasses.Semigroup
import VerifiedClasses.TH.Generics
import VerifiedClasses.Traversable

$(singletons [d|
  data ProductEx a = MkProductEx () a (Pair () a)
    deriving stock (Foldable, Show)
  |])

-----
-- Verified instances
-----

deriving stock instance Generic (ProductEx a)
deriving stock instance Generic1 ProductEx
$(verifyGeneric0And1 ''ProductEx)

$(concatSequence
  [ deriveAbelianSemigroupAll [t| forall a. AbelianSemigroup a => AbelianSemigroup (ProductEx a) |]
  , deriveApplicativeAll [t| Applicative ProductEx |]
  , deriveEqAll [t| forall a. Eq a => Eq (ProductEx a) |]
  , deriveFunctorAll [t| Functor ProductEx |]
  , deriveMonoidAll [t| forall a. Monoid a => Monoid (ProductEx a) |]
  , deriveOrdAll [t| forall a. Ord a => Ord (ProductEx a) |]
  , deriveSemigroupAll [t| forall a. Semigroup a => Semigroup (ProductEx a) |]
  , deriveTraversableAll [t| Traversable ProductEx |]
  ])

-----
-- Miscellaneous instances
-----

instance SArbitrary a => SArbitrary (ProductEx a) where
  sArbitrary = liftA3 (\a b c -> a `bindSS`
                          \a' -> b `bindSS`
                          \b' -> c `bindSS`
                          \c' -> SomeSing (SMkProductEx a' b' c'))
                      sArbitrary sArbitrary sArbitrary

-----
-- Tests
-----

main :: IO ()
main = hspec spec

spec :: Spec
spec = traverse_ propLaws
         [ abelianSemigroupLaws @(ProductEx ())
         , eqLaws               @(ProductEx ())
         , genericLaws          @(ProductEx ())
         , monoidLaws           @(ProductEx ())
         , ordLaws              @(ProductEx ())
         , semigroupLaws        @(ProductEx ())

         , applicativeLaws @ProductEx
         , functorLaws     @ProductEx
         , generic1Laws    @ProductEx
         , traversableLaws @ProductEx
         ]
