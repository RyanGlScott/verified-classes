{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.AbelianSemigroup () where

import Control.Monad.Extra
import Data.Functor.Identity
import Data.Void
import GHC.Generics
import Instances.Generics ()
import Instances.Semigroup ()
import VerifiedClasses.AbelianSemigroup

$(concatMapM deriveAbelianSemigroupAll
  [ [t| AbelianSemigroup Void |]
  , [t| AbelianSemigroup () |]
  , [t| forall a b. (AbelianSemigroup a, AbelianSemigroup b) => AbelianSemigroup (a, b) |]
  , [t| forall a. AbelianSemigroup a => AbelianSemigroup (Identity a) |]
  , [t| forall p. AbelianSemigroup p => AbelianSemigroup (Par1 p) |]
  , [t| forall f p. AbelianSemigroup (f p) => AbelianSemigroup (Rec1 f p) |]
  , [t| forall f g p. AbelianSemigroup (f (g p)) => AbelianSemigroup ((f :.: g) p) |]
  ])
