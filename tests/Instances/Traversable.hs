{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Traversable () where

import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Kind
import Data.Type.Equality ((:~:)(..))
import GHC.Generics
import Instances.Functor ()
import Instances.Generics ()
import Prelude.Singletons
import VerifiedClasses.Applicative
import VerifiedClasses.Functor
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.Traversable

infixr 9 %.$$$
(%.$$$) :: forall a b c (f :: b ~> c) (g :: a ~> b).
           Sing f -> Sing g -> Sing (f .@#@$$$ g)
sf %.$$$ sg = singFun1 @(f .@#@$$$ g) (sf %. sg)

data Direction
  = S           -- Stop
  | D Direction -- Down
  | L Direction -- Left
  | R Direction -- Right

type family Rep1Node (d :: Direction) (rep1 :: k -> Type) :: k -> Type where
  Rep1Node 'S     rep1       = rep1
  Rep1Node ('D d) (M1 _ _ f) = Rep1Node d f
  Rep1Node ('L d) (f :+: _)  = Rep1Node d f
  Rep1Node ('L d) (f :*: _)  = Rep1Node d f
  Rep1Node ('R d) (_ :+: f)  = Rep1Node d f
  Rep1Node ('R d) (_ :*: f)  = Rep1Node d f

{-
instance GTraversable [] where
$(deriveTraversableVerifiedOnly [t| Traversable [] |])

instance GTraversable ((,) a) where
$(deriveTraversableVerifiedOnly [t| forall a. Traversable ((,) a) |])
-}

instance GTraversable Identity where
  genericTraverseC :: forall (f :: Type -> Type) a b (g :: a ~> f b) (x :: Identity a).
                      VApplicative f
                   => Sing g -> Sing x
                   -> Traverse g x :~: GenericTraverse g x
  genericTraverseC sg (SIdentity (si :: Sing i))
    | Refl <- fmapCompose sS1Fun sPar1Fun (sg @@ si)
    , Refl <- fmapCompose sC1Fun (sS1Fun %.$$$ sPar1Fun) (sg @@ si)
    , Refl <- fmapCompose sD1Fun (sC1Fun %.$$$ sS1Fun %.$$$ sPar1Fun) (sg @@ si)
    , Refl <- fmapCompose sTo1Fun (sD1Fun %.$$$ sC1Fun %.$$$ sS1Fun %.$$$ sPar1Fun) (sg @@ si)
    , Refl <- --     Fmap (To1Sym0 .@#@$$$ M1Sym0 .@#@$$$ M1Sym0
              --                   .@#@$$$ M1Sym0 .@#@$$$ Par1Sym0) (g @@ i)
              -- :~: Fmap IdentitySym0 (g @@ i))
              Refl @FmapSym0
                `apply` funExt @b @(Identity b)
                               @(To1Sym0 .@#@$$$ M1Sym0 .@#@$$$ M1Sym0
                                         .@#@$$$ M1Sym0 .@#@$$$ Par1Sym0)
                               @IdentitySym0
                               lemma
                `apply` Refl @(g @@ i)
    = Refl
    where
      sPar1Fun :: Sing (Par1Sym0 :: b ~> Par1 b)
      sPar1Fun = singFun1 SPar1

      sD1Fun :: Sing (M1Sym0 :: _ b ~> Rep1Node 'S (Rep1 Identity) b)
      sD1Fun = singFun1 SM1

      sC1Fun :: Sing (M1Sym0 :: _ b ~> Rep1Node ('D 'S) (Rep1 Identity) b)
      sC1Fun = singFun1 SM1

      sS1Fun :: Sing (M1Sym0 :: _ b ~> Rep1Node ('D ('D 'S)) (Rep1 Identity) b)
      sS1Fun = singFun1 SM1

      sTo1Fun :: Sing (To1Sym0 :: Rep1 Identity b ~> Identity b)
      sTo1Fun = singFun1 sTo1'

      lemma :: forall (z :: b). Sing z
            -> To1Sym0 @@ ('M1 ('M1 ('M1 ('Par1 z)))) :~: 'Identity z
      lemma = applyTo1Sym0 @_ @Identity . SM1 . SM1 . SM1 . SPar1
$(deriveTraversableVerifiedOnly [t| Traversable Identity |])

instance GTraversable Maybe where
  genericTraverseC :: forall (f :: Type -> Type) a b (g :: a ~> f b) (x :: Maybe a).
                      VApplicative f
                   => Sing g -> Sing x
                   -> Traverse g x :~: GenericTraverse g x
  genericTraverseC sg sm =
    case sm of
      SNothing
        |  Refl <- fmapPure @f sC1FunNothing sU1
        ,  Refl <- fmapPure @f sL1Fun (sC1FunNothing @@ sU1)
        ,  Refl <- fmapPure @f sD1Fun (sL1Fun @@ (sC1FunNothing @@ sU1))
        ,  Refl <- fmapPure @f sTo1Fun (sD1Fun @@ (sL1Fun @@ (sC1FunNothing @@ sU1)))
        ,  Refl <- applyTo1Sym0 @_ @Maybe (sD1Fun @@ (sL1Fun @@ (sC1FunNothing @@ sU1)))
        -> Refl
      SJust (sn :: Sing n)
        | Refl <- fmapCompose sS1Fun sPar1Fun (sg @@ sn)
        , Refl <- fmapCompose sC1FunJust (sS1Fun %.$$$ sPar1Fun) (sg @@ sn)
        , Refl <- fmapCompose sR1Fun (sC1FunJust %.$$$ sS1Fun %.$$$ sPar1Fun) (sg @@ sn)
        , Refl <- fmapCompose sD1Fun (sR1Fun %.$$$ sC1FunJust %.$$$ sS1Fun %.$$$ sPar1Fun)
                              (sg @@ sn)
        , Refl <- fmapCompose
                    sTo1Fun
                    (sD1Fun %.$$$ sR1Fun %.$$$ sC1FunJust %.$$$ sS1Fun %.$$$ sPar1Fun)
                    (sg @@ sn)
        , Refl <- --     Fmap (To1Sym0 .@#@$$$ M1Sym0 .@#@$$$ R1Sym0 .@#@$$$ M1Sym0
                  --                   .@#@$$$ M1Sym0 .@#@$$$ Par1Sym0) (g @@ n)
                  -- :~: Fmap JustSym0 (g @@ n)
                  Refl @FmapSym0
                    `apply` funExt @b @(Maybe b)
                                   @(To1Sym0 .@#@$$$ M1Sym0 .@#@$$$ R1Sym0 .@#@$$$ M1Sym0
                                             .@#@$$$ M1Sym0 .@#@$$$ Par1Sym0)
                                   @JustSym0
                                   lemma
                    `apply` Refl @(g @@ n)
        -> Refl
    where
      sU1 :: Sing ('U1 :: U1 b)
      sU1 = SU1

      sD1Fun :: Sing (M1Sym0 :: _ b ~> Rep1Node 'S (Rep1 Maybe) b)
      sD1Fun = singFun1 SM1

      sL1Fun :: Sing (L1Sym0 :: _ b ~> Rep1Node ('D 'S) (Rep1 Maybe) b)
      sL1Fun = singFun1 SL1

      sR1Fun :: Sing (R1Sym0 :: _ b ~> Rep1Node ('D 'S) (Rep1 Maybe) b)
      sR1Fun = singFun1 SR1

      sC1FunNothing :: Sing (M1Sym0 :: _ b ~> Rep1Node ('D ('L 'S)) (Rep1 Maybe) b)
      sC1FunNothing = singFun1 SM1

      sC1FunJust :: Sing (M1Sym0 :: _ b ~> Rep1Node ('D ('R 'S)) (Rep1 Maybe) b)
      sC1FunJust = singFun1 SM1

      sS1Fun :: Sing (M1Sym0 :: _ b ~> Rep1Node ('D ('R ('D 'S))) (Rep1 Maybe) b)
      sS1Fun = singFun1 SM1

      sPar1Fun :: Sing (Par1Sym0 :: b ~> Par1 b)
      sPar1Fun = singFun1 SPar1

      sTo1Fun :: Sing (To1Sym0 :: Rep1 Maybe b ~> Maybe b)
      sTo1Fun = singFun1 sTo1'

      lemma :: forall (z :: b). Sing z
            -> To1Sym0 @@ ('M1 ('R1 ('M1 ('M1 ('Par1 z))))) :~: 'Just z
      lemma = applyTo1Sym0 @_ @Maybe . SM1 . SR1 . SM1 . SM1 . SPar1
$(deriveTraversableVerifiedOnly [t| Traversable Maybe |])
