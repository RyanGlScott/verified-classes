{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Instances.Generics () where

import Control.Monad.Extra
import Data.Kind
import Data.Void
import GHC.Generics
import Prelude.Singletons
import VerifiedClasses.Generics
import VerifiedClasses.TH.Generics

$(concatMapM verifyGeneric0
  [ ''Void, ''(), ''Bool, ''(:.:) ])
$(concatMapM verifyGeneric0And1
  [ ''[], ''(,), ''Maybe, ''V1, ''U1, ''K1
  , ''M1, ''(:+:), ''(:*:), ''Par1, ''Rec1
  ])

$(genSingletonsWithTypeGeneric
  [t|    forall f g. Functor f
      => Generic1 ((f :: Type -> Type) :.: g)
    |])
