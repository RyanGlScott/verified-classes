{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Applicative () where

import Data.Type.Equality ((:~:)(..))
import Instances.Functor ()
import Instances.Generics ()
import Prelude.Singletons
import VerifiedClasses.Applicative
import VerifiedClasses.Monoid

instance VMonoid a => GApplicative ((,) a) where
  genericPureC _ = Refl
  genericApC (STuple2 x1 _) (STuple2 x2 _)
    | Refl <- monoidMappend x1 x2
    = Refl
  liftA2FmapApC _ STuple2{} STuple2{} = Refl
$(deriveApplicativeVerifiedOnly [t| forall a. Monoid a => Applicative ((,) a) |])
