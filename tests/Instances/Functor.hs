{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Functor () where

import Data.Type.Equality ((:~:)(..))
import Instances.Generics ()
import Prelude.Singletons
import VerifiedClasses.Functor

instance GFunctor [] where
  genericFmapC _ SNil    = Refl
  genericFmapC _ SCons{} = Refl
  replaceFmapConstC _ SNil    = Refl
  replaceFmapConstC x (SCons _ ys)
    | Refl <- replaceFmapConstC x ys
    = Refl
$(deriveFunctorVerifiedOnly [t| Functor [] |])

instance GFunctor ((,) a) where
  genericFmapC _ STuple2{} = Refl
  replaceFmapConstC _ STuple2{} = Refl
$(deriveFunctorVerifiedOnly [t| forall a. Functor ((,) a) |])

instance GFunctor Maybe where
  genericFmapC _ SNothing = Refl
  genericFmapC _ SJust{}  = Refl
  replaceFmapConstC _ SNothing = Refl
  replaceFmapConstC _ SJust{}  = Refl
$(deriveFunctorVerifiedOnly [t| Functor Maybe |])
