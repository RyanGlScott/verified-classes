{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Monoid () where

import Control.Monad.Extra
import Data.Functor.Identity
import Data.Type.Equality ((:~:)(..))
import GHC.Generics
import Instances.Generics ()
import Instances.Semigroup ()
import Prelude.Singletons
import VerifiedClasses.Monoid

instance GMonoid () where
  genericMemptyC = Refl
$(deriveMonoidVerifiedOnly [t| Monoid () |])

instance ( Semigroup a, PSemigroup a, SSemigroup a
         , Monoid a, PMonoid a, SMonoid a
         , Semigroup b, PSemigroup b, SSemigroup b
         , Monoid b, PMonoid b, SMonoid b )
    => GMonoid (a, b) where
  genericMemptyC = Refl
$(deriveMonoidVerifiedOnly
  [t| forall a b. (Monoid a, Monoid b) => Monoid (a, b) |])

instance ( Semigroup a, PSemigroup a, SSemigroup a
         , Monoid a, PMonoid a, SMonoid a )
    => GMonoid (Identity a) where
  genericMemptyC = Refl
$(deriveMonoidVerifiedOnly [t| forall a. Monoid a => Monoid (Identity a) |])

$(concatMapM deriveMonoidSingletons
  [ [t| forall p. Monoid p => Monoid (Par1 p) |]
  , [t| forall f p. Monoid (f p) => Monoid (Rec1 f p) |]
  , [t| forall f g p. Monoid (f (g p)) => Monoid ((f :.: g) p) |]
  ])
