{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Semigroup () where

import Control.Monad.Extra
import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Type.Equality ((:~:)(..))
import Data.Void
import GHC.Generics
import Instances.Generics ()
import Prelude.Singletons
import VerifiedClasses.Semigroup

instance GSemigroup Void where
  genericAppendC x _ = case x of {}
$(deriveSemigroupVerifiedOnly [t| Semigroup Void |])

instance GSemigroup () where
  genericAppendC STuple0 STuple0 = Refl
$(deriveSemigroupVerifiedOnly [t| Semigroup () |])

instance ( Semigroup a, PSemigroup a, SSemigroup a
         , Semigroup b, PSemigroup b, SSemigroup b )
    => GSemigroup (a, b) where
  genericAppendC STuple2{} STuple2{} = Refl
$(deriveSemigroupVerifiedOnly
  [t| forall a b. (Semigroup a, Semigroup b) => Semigroup (a, b) |])

instance (Semigroup a, PSemigroup a, SSemigroup a)
    => GSemigroup (Identity a) where
  genericAppendC SIdentity{} SIdentity{} = Refl
$(deriveSemigroupVerifiedOnly [t| forall a. Semigroup a => Semigroup (Identity a) |])

$(concatMapM deriveSemigroupSingletons
  [ [t| forall p. Semigroup p => Semigroup (Par1 p) |]
  , [t| forall f p. Semigroup (f p) => Semigroup (Rec1 f p) |]
  , [t| forall f g p. Semigroup (f (g p)) => Semigroup ((f :.: g) p) |]
  ])
