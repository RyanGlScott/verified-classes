{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Eq () where

import Control.Monad.Extra
import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Type.Equality ((:~:)(..))
import Data.Void
import GHC.Generics
import Instances.Generics ()
import Prelude.Singletons
import VerifiedClasses.Eq

instance (Eq a, PEq a, SEq a) => GEq [a] where
  genericEqC SNil sy =
    case sy of
      SNil    -> Refl
      SCons{} -> Refl
  genericEqC SCons{} sy =
    case sy of
      SNil    -> Refl
      SCons{} -> Refl
$(deriveEqVerifiedOnly [t| forall a. Eq a => Eq [a] |])

instance GEq Void where
  genericEqC _ _ = Refl
$(deriveEqVerifiedOnly [t| Eq Void |])

instance GEq () where
  genericEqC STuple0 STuple0 = Refl
$(deriveEqVerifiedOnly [t| Eq () |])

instance GEq Bool where
  genericEqC SFalse sy =
    case sy of
      SFalse -> Refl
      STrue  -> Refl
  genericEqC STrue sy =
    case sy of
      SFalse -> Refl
      STrue  -> Refl
$(deriveEqVerifiedOnly [t| Eq Bool |])

instance ( Eq a, PEq a, SEq a
         , Eq b, PEq b, SEq b )
    => GEq (a, b) where
  genericEqC STuple2{} STuple2{} = Refl
$(deriveEqVerifiedOnly [t| forall a b. (Eq a, Eq b) => Eq (a, b) |])

instance (Eq a, PEq a, SEq a) => GEq (Maybe a) where
  genericEqC SNothing sy =
    case sy of
      SNothing -> Refl
      SJust {} -> Refl
  genericEqC (SJust {}) sy =
    case sy of
      SNothing -> Refl
      SJust {} -> Refl
$(deriveEqVerifiedOnly [t| forall a. Eq a => Eq (Maybe a) |])

instance (Eq a, PEq a, SEq a) => GEq (Identity a) where
  genericEqC SIdentity{} SIdentity{} = Refl
$(deriveEqVerifiedOnly [t| forall a. Eq a => Eq (Identity a) |])

$(concatMapM deriveEqSingletons
  [ [t| forall p. Eq p => Eq (Par1 p) |]
  , [t| forall f p. Eq (f p) => Eq (Rec1 f p) |]
  , [t| forall f g p. Eq (f (g p)) => Eq ((f :.: g) p) |]
  ])
