{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Ord () where

import Control.Monad.Extra
import Data.Type.Equality ((:~:)(..))
import Data.Void
import GHC.Generics
import Instances.Eq ()
import Instances.Generics ()
import Prelude.Singletons
import VerifiedClasses.Ord

instance GOrd Void where
  genericLeqC _ _ = Refl
$(deriveOrdVerifiedOnly [t| Ord Void |])

instance GOrd () where
  genericLeqC STuple0 STuple0 = Refl
$(deriveOrdVerifiedOnly [t| Ord () |])

instance GOrd Bool where
  genericLeqC SFalse sy =
    case sy of
      SFalse -> Refl
      STrue  -> Refl
  genericLeqC STrue sy =
    case sy of
      SFalse -> Refl
      STrue  -> Refl
$(deriveOrdVerifiedOnly [t| Ord Bool |])

{-
instance VOrd a => GOrd [a] where
$(deriveOrdVerifiedOnly [t| forall a. Ord a => Ord [a] |])

instance (VOrd a, VOrd b) => GOrd (a, b) where
$(deriveOrdVerifiedOnly [t| forall a b. (Ord a, Ord b) => Ord (a, b) |])

instance VOrd a => GOrd (Maybe a) where
$(deriveOrdVerifiedOnly [t| forall a. Ord a => Ord (Maybe a) |])

instance VOrd a => GOrd (Identity a) where
$(deriveOrdVerifiedOnly [t| forall a. Ord a => Ord (Identity a) |])
-}

$(concatMapM deriveOrdSingletons
  [ [t| forall p. Ord p => Ord (Par1 p) |]
  , [t| forall f p. Ord (f p) => Ord (Rec1 f p) |]
  , [t| forall f g p. Ord (f (g p)) => Ord ((f :.: g) p) |]
  ])
