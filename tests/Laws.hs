{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Laws where

import Data.Foldable
import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Kind
import Data.Singletons.Base.SomeSing ()
import Data.Singletons.ShowSing
import Data.Singletons.TH
import GHC.Generics
import GHC.TypeLits.Singletons
import Prelude.Singletons
import SArbitrary
import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck

import VerifiedClasses.AbelianSemigroup
import VerifiedClasses.Alternative
import VerifiedClasses.Applicative
import VerifiedClasses.Applicative.Transformer
import VerifiedClasses.Eq
import VerifiedClasses.Functor
import VerifiedClasses.Generics
import VerifiedClasses.Monad
import VerifiedClasses.MonadPlus
import VerifiedClasses.MonadZip
import VerifiedClasses.Monoid
import VerifiedClasses.Ord
import VerifiedClasses.Semigroup
import VerifiedClasses.So
import VerifiedClasses.Traversable

data Laws = Laws
  { lawsTypeclass  :: String
  , lawsProperties :: [(String, Property)]
  }

$(singletonsOnly [d|
  func1' :: Natural -> (Natural, Natural)
  func1' i = (div (i + 5) 3, i * i - 2 * i + 1)

  func2' :: (Natural, Natural) -> (Bool, Either Natural Natural)
  func2' (a, b) = (a < 0, if a >= 0 then Left (a - b) else Right (b + 2))

  natTrans' :: Identity a -> Identity a
  natTrans' (Identity x) = Identity x
  |])

sFunc1 :: Sing Func1'Sym0
sFunc1 = singFun1 sFunc1'

sFunc2 :: Sing Func2'Sym0
sFunc2 = singFun1 sFunc2'

compPure :: forall f a b (g :: a ~> b). SApplicative f
         => Sing g -> Sing (PureSym0 .@#@$$$ g :: a ~> f b)
compPure sf = singFun1 (singFun1 @PureSym0 sPure %. sf)

sNatTrans :: forall z. Sing (NatTrans'Sym0 :: Identity z ~> Identity z)
sNatTrans = singFun1 sNatTrans'

instance VApplicativeTransformer NatTrans'Sym0 where
  applicativeTransformerPure _ _ = Refl
  applicativeTransformerAp _ SIdentity{} SIdentity{} = Refl

propLaws :: Laws -> Spec
propLaws (Laws className properties) =
  describe className $
    for_ properties $ \(name, p) ->
      prop name p

abelianSemigroupLaws :: forall a. (SArbitrary a, ShowSing a, VAbelianSemigroup a) => Laws
abelianSemigroupLaws = Laws "Semigroup"
  [ ("Commutativity", property $ \(SomeSing x) (SomeSing y) ->
                      Box (semigroupCommutative @a x y))
  ]

alternativeLaws :: forall f. (SArbitrary (f Natural), ShowSing (f Natural), VAlternative f) => Laws
alternativeLaws = Laws "Alternative"
  [ ("Left identity",  property $ \(SomeSing x) -> Box (alternativeLeftIdentity @f @Natural x))
  , ("Right identity", property $ \(SomeSing x) -> Box (alternativeRightIdentity @f @Natural x))
  , ("Associativity",  property $ \(SomeSing x) (SomeSing y) (SomeSing z) ->
                                  Box (alternativeAssociativity @f @Natural x y z))
  ]

applicativeLaws :: forall f. (SArbitrary (f Natural), ShowSing (f Natural), VApplicative f) => Laws
applicativeLaws = Laws "Applicative"
  [ ("Identity",     property $ \(SomeSing x) -> Box (applicativeIdentity @f @Natural x))
  , ("Composition",  property $ \(SomeSing x) ->
                                Box (applicativeComposition @f (sPure sFunc2) (sPure sFunc1) x))
  , ("Homomorphism", property $ \(SomeSing x) -> Box (applicativeHomomorphism @f sFunc1 x))
  , ("Interchange",  property $ \(SomeSing x) -> Box (applicativeInterchange @f (sPure sFunc1) x))
  , ("fmap",         property $ \(SomeSing x) -> Box (applicativeFmap @f sFunc1 x))
  ]

eqLaws :: forall a. (SArbitrary a, ShowSing a, VEq a) => Laws
eqLaws = Laws "Eq"
  [ ("Reflexive",  property $ \(SomeSing x) -> Box (eqReflexive @a x))
  , ("Symmetric",  property propSymmetric)
  , ("Transitive", property propTransitive)
  ]
  where
    propSymmetric :: SomeSing a -> SomeSing a -> Box
    propSymmetric (SomeSing x) (SomeSing y) =
      case x %== y of
        STrue  -> Box (eqSymmetric @a x y Oh)
        SFalse -> Box ()

    propTransitive :: SomeSing a -> SomeSing a -> SomeSing a -> Box
    propTransitive (SomeSing x) (SomeSing y) (SomeSing z) =
      case (x %== y, y %== z) of
        (STrue, STrue) -> Box (eqTransitive @a x y z Oh Oh)
        (_,     _)     -> Box ()

functorLaws :: forall f. (SArbitrary (f Natural), ShowSing (f Natural), VFunctor f) => Laws
functorLaws = Laws "Functor"
  [ ("Identity",    property $ \(SomeSing x) -> Box (fmapId @f @Natural x))
  , ("Composition", property $ \(SomeSing x) -> Box (fmapCompose @f sFunc2 sFunc1 x))
  ]

genericLaws :: forall a.
               ( SArbitrary a, SArbitrary (Rep a ())
               , ShowSing a, ShowSing (Rep a ())
               , VGeneric a
               )
            => Laws
genericLaws = Laws "Generic"
  [ ("tof", property $ \(SomeSing x) -> Box (sTof @a @() x))
  , ("fot", property $ \(SomeSing x) -> Box (sFot @a @() x))
  ]

generic1Laws :: forall f.
                ( SArbitrary (f Natural), SArbitrary (Rep1 f Natural)
                , ShowSing (f Natural), ShowSing (Rep1 f Natural)
                , VGeneric1 f
                )
             => Laws
generic1Laws = Laws "Generic1"
  [ ("tof1", property $ \(SomeSing x) -> Box (sTof1 @Type @f @Natural x))
  , ("fot1", property $ \(SomeSing x) -> Box (sFot1 @Type @f @Natural x))
  ]

monadLaws :: forall m. (SArbitrary (m Natural), ShowSing (m Natural), VMonad m) => Laws
monadLaws = Laws "Monad"
  [ ("Left identity",  property propLeftIdentity)
  , ("Right identity", property $ \(SomeSing x) -> Box (monadRightIdentity @m @Natural x))
  , ("Associativity",  property $ \(SomeSing x) ->
                       Box (monadAssociativity @m x (compPure sFunc1) (compPure sFunc2)))
  ]
  where
    propLeftIdentity :: SomeSing Natural -> Box
    propLeftIdentity (SomeSing x) =
      Box (monadLeftIdentity @m @Natural x (compPure sFunc1))

monadPlusLaws :: forall m. (SArbitrary (m Natural), ShowSing (m Natural), VMonadPlus m) => Laws
monadPlusLaws = Laws "MonadPlus"
  [ ("Left zero",  property propLeftZero)
  , ("Right zero", property $ \(SomeSing x) -> Box (rightZero @m @Natural @Natural x))
  ]
  where
    propLeftZero :: Box
    propLeftZero = Box (leftZero @m @_ @_ @(PureSym0 .@#@$$$ Func1'Sym0)
                                 (compPure sFunc1))

monadZipLaws :: forall m. (SArbitrary (m Natural), ShowSing (m Natural), VMonadZip m) => Laws
monadZipLaws = Laws "MonadZip"
  [ ("Naturality", property $ \(SomeSing x) (SomeSing y) ->
                              Box (monadZipNaturality @m sFunc1 sFunc1 x y))
  -- TODO: Information preservation
  ]

monoidLaws :: forall a. (SArbitrary a, ShowSing a, VMonoid a) => Laws
monoidLaws = Laws "Monoid"
  [ ("Left identity",  property $ \(SomeSing x) -> Box (monoidLeftIdentity @a x))
  , ("Right identity", property $ \(SomeSing x) -> Box (monoidRightIdentity @a x))
  ]

ordLaws :: forall a. (SArbitrary a, ShowSing a, VOrd a) => Laws
ordLaws = Laws "Ord"
  [ ("Transitive",    property propTransitive)
  , ("Reflexive",     property $ \(SomeSing x)              -> Box (leqReflexive @a x))
  , ("Antisymmetric", property propAntisymmetric)
  , ("Total",         property $ \(SomeSing x) (SomeSing y) -> Box (leqTotal @a x y))
  ]
  where
    propTransitive :: SomeSing a -> SomeSing a -> SomeSing a -> Box
    propTransitive (SomeSing x) (SomeSing y) (SomeSing z) =
      case (x %<= y, y %<= z) of
        (STrue, STrue) -> Box (leqTransitive @a x y z Oh Oh)
        (_,     _)     -> Box ()

    propAntisymmetric :: SomeSing a -> SomeSing a -> Box
    propAntisymmetric (SomeSing x) (SomeSing y) =
      case (x %<= y, y %<= x) of
        (STrue, STrue) -> Box (leqAntisymmetric @a x y Oh Oh)
        (_,     _)     -> Box ()

semigroupLaws :: forall a. (SArbitrary a, ShowSing a, VSemigroup a) => Laws
semigroupLaws = Laws "Semigroup"
  [ ("Associative", property $ \(SomeSing x) (SomeSing y) (SomeSing z) -> Box (semigroupAssociative @a x y z))
  ]

traversableLaws :: forall t. (SArbitrary (t Natural), ShowSing (t Natural), VTraversable t) => Laws
traversableLaws = Laws "Traversable"
  [ ("Naturality",  property $ \(SomeSing x) ->
                               Box (traversableNaturality @t sNatTrans (compPure sFunc1) x))
  , ("Identity",    property $ \(SomeSing x) -> Box (traversableIdentity @t @Natural x))
  , ("Composition", property $ \(SomeSing x) ->
                               Box (traversableComposition @t @_ @_ @_ @Identity @Identity
                                                           (compPure sFunc1) (compPure sFunc2) x))
  ]
