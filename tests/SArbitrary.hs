{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module SArbitrary
  ( SArbitrary(..), Box(..)
  , bindSS, sequenceSS
  ) where

import Control.Applicative
import Control.DeepSeq
import Control.Monad
import Data.Functor.Const.Singletons (SConst(..))
import Data.Functor.Identity
import Data.Functor.Identity.Singletons (SIdentity(..))
import Data.Kind
import Data.List.NonEmpty (NonEmpty(..))
import Data.List.NonEmpty.Singletons (SNonEmpty(..))
import Data.Monoid as Monoid
import Data.Monoid.Singletons as Monoid
import Data.Ord
import Data.Ord.Singletons (SDown(..))
import Data.Semigroup as Semigroup
import Data.Semigroup.Singletons as Semigroup
import GHC.Generics
import Numeric.Natural (Natural)
import Prelude.Singletons hiding (Const)
import Test.QuickCheck hiding (Result)
import Test.QuickCheck.Instances ()
import Test.QuickCheck.Property
import VerifiedClasses.Generics

instance SArbitrary a => Arbitrary (SomeSing a) where
  arbitrary = sArbitrary

class SArbitrary a where
  sArbitrary :: Gen (SomeSing a)

instance SArbitrary a => SArbitrary [a] where
  sArbitrary = sequenceSS <$> do
    sized $ \n -> do
      k <- choose (0, n)
      replicateM k sArbitrary

instance SArbitrary () where
  sArbitrary = pure $ SomeSing STuple0

instance (SArbitrary a, SArbitrary b) => SArbitrary (a, b) where
  sArbitrary = liftA2 (\a b -> a `bindSS`
                        \a' -> b `bindSS`
                        \b' -> SomeSing (STuple2 a' b'))
                      sArbitrary sArbitrary

instance (SArbitrary a, SArbitrary b, SArbitrary c) => SArbitrary (a, b, c) where
  sArbitrary = liftA3 (\a b c -> a `bindSS`
                          \a' -> b `bindSS`
                          \b' -> c `bindSS`
                          \c' -> SomeSing (STuple3 a' b' c'))
                      sArbitrary sArbitrary sArbitrary

instance (SArbitrary a, SArbitrary b, SArbitrary c, SArbitrary d)
    => SArbitrary (a, b, c, d) where
  sArbitrary = (\a b c d ->
                        a `bindSS`
                 \a' -> b `bindSS`
                 \b' -> c `bindSS`
                 \c' -> d `bindSS`
                 \d' -> SomeSing (STuple4 a' b' c' d'))
                 <$> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary

instance ( SArbitrary a, SArbitrary b, SArbitrary c, SArbitrary d
         , SArbitrary e
         ) => SArbitrary (a, b, c, d, e) where
  sArbitrary = (\a b c d e ->
                        a `bindSS`
                 \a' -> b `bindSS`
                 \b' -> c `bindSS`
                 \c' -> d `bindSS`
                 \d' -> e `bindSS`
                 \e' -> SomeSing (STuple5 a' b' c' d' e'))
                 <$> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary

instance ( SArbitrary a, SArbitrary b, SArbitrary c, SArbitrary d
         , SArbitrary e, SArbitrary f
         ) => SArbitrary (a, b, c, d, e, f) where
  sArbitrary = (\a b c d e f ->
                        a `bindSS`
                 \a' -> b `bindSS`
                 \b' -> c `bindSS`
                 \c' -> d `bindSS`
                 \d' -> e `bindSS`
                 \e' -> f `bindSS`
                 \f' -> SomeSing (STuple6 a' b' c' d' e' f'))
                 <$> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary

instance ( SArbitrary a, SArbitrary b, SArbitrary c, SArbitrary d
         , SArbitrary e, SArbitrary f, SArbitrary g
         ) => SArbitrary (a, b, c, d, e, f, g) where
  sArbitrary = (\a b c d e f g ->
                        a `bindSS`
                 \a' -> b `bindSS`
                 \b' -> c `bindSS`
                 \c' -> d `bindSS`
                 \d' -> e `bindSS`
                 \e' -> f `bindSS`
                 \f' -> g `bindSS`
                 \g' -> SomeSing (STuple7 a' b' c' d' e' f' g'))
                 <$> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary
                 <*> sArbitrary

instance SArbitrary Semigroup.All where
  sArbitrary = (`bindSS` SomeSing . SAll) <$> sArbitrary

instance SArbitrary Semigroup.Any where
  sArbitrary = (`bindSS` SomeSing . SAny) <$> sArbitrary

instance (SArbitrary a, SArbitrary b) => SArbitrary (Arg a b) where
  sArbitrary = liftA2 (\a b -> a `bindSS`
                        \a' -> b `bindSS`
                        \b' -> SomeSing (SArg a' b'))
                      sArbitrary sArbitrary

instance SArbitrary Bool where
  sArbitrary = elements [SomeSing SFalse, SomeSing STrue]

instance SArbitrary a => SArbitrary (Const a b) where
  sArbitrary = (`bindSS` SomeSing . SConst) <$> sArbitrary

instance SArbitrary a => SArbitrary (Down a) where
  sArbitrary = (`bindSS` SomeSing . SDown) <$> sArbitrary

instance SArbitrary a => SArbitrary (Dual a) where
  sArbitrary = (`bindSS` SomeSing . SDual) <$> sArbitrary

instance (SArbitrary a, SArbitrary b) => SArbitrary (Either a b) where
  sArbitrary = oneof [ (`bindSS` SomeSing . SLeft)  <$> sArbitrary
                     , (`bindSS` SomeSing . SRight) <$> sArbitrary
                     ]

instance SArbitrary a => SArbitrary (Monoid.First a) where
  sArbitrary = (`bindSS` SomeSing . Monoid.SFirst) <$> sArbitrary

instance SArbitrary a => SArbitrary (Semigroup.First a) where
  sArbitrary = (`bindSS` SomeSing . Semigroup.SFirst) <$> sArbitrary

instance SArbitrary a => SArbitrary (Identity a) where
  sArbitrary = (`bindSS` SomeSing . SIdentity) <$> sArbitrary

instance SArbitrary a => SArbitrary (Monoid.Last a) where
  sArbitrary = (`bindSS` SomeSing . Monoid.SLast) <$> sArbitrary

instance SArbitrary a => SArbitrary (Semigroup.Last a) where
  sArbitrary = (`bindSS` SomeSing . Semigroup.SLast) <$> sArbitrary

instance SArbitrary a => SArbitrary (Maybe a) where
  sArbitrary = frequency [ (1, pure $ SomeSing SNothing)
                         , (3, (`bindSS` SomeSing . SJust) <$> sArbitrary)
                         ]

instance SArbitrary a => SArbitrary (Semigroup.Max a) where
  sArbitrary = (`bindSS` SomeSing . SMax) <$> sArbitrary

instance SArbitrary a => SArbitrary (Semigroup.Min a) where
  sArbitrary = (`bindSS` SomeSing . SMin) <$> sArbitrary

instance SArbitrary Natural where
  sArbitrary = toSing <$> arbitrary

instance SArbitrary a => SArbitrary (NonEmpty a) where
  sArbitrary = liftA2 (\a b -> a `bindSS`
                        \a' -> b `bindSS`
                        \b' -> SomeSing (a' :%| b'))
                      sArbitrary sArbitrary

instance SArbitrary Ordering where
  sArbitrary = elements [SomeSing SLT, SomeSing SEQ, SomeSing SGT]

instance SArbitrary a => SArbitrary (Semigroup.Product a) where
  sArbitrary = (`bindSS` SomeSing . SProduct) <$> sArbitrary

instance SArbitrary a => SArbitrary (Semigroup.Sum a) where
  sArbitrary = (`bindSS` SomeSing . SSum) <$> sArbitrary

instance SArbitrary Symbol where
  sArbitrary = toSing <$> arbitrary

instance SArbitrary m => SArbitrary (WrappedMonoid m) where
  sArbitrary = (`bindSS` SomeSing . SWrapMonoid) <$> sArbitrary

instance SArbitrary (U1 p) where
  sArbitrary = pure $ SomeSing SU1

instance SArbitrary c => SArbitrary (K1 i c p) where
  sArbitrary = (`bindSS` SomeSing . SK1) <$> sArbitrary

instance SArbitrary (f p) => SArbitrary (M1 i c f p) where
  sArbitrary = (`bindSS` SomeSing . SM1) <$> sArbitrary

instance (SArbitrary (f p), SArbitrary (g p)) => SArbitrary ((f :+: g) p) where
  sArbitrary = oneof [ (`bindSS` SomeSing . SL1) <$> sArbitrary
                     , (`bindSS` SomeSing . SR1) <$> sArbitrary
                     ]

instance (SArbitrary (f p), SArbitrary (g p)) => SArbitrary ((f :*: g) p) where
  sArbitrary = liftA2 (\a b -> a `bindSS`
                        \a' -> b `bindSS`
                        \b' -> SomeSing (a' :%*: b'))
                      sArbitrary sArbitrary

instance SArbitrary p => SArbitrary (Par1 p) where
  sArbitrary = (`bindSS` SomeSing . SPar1) <$> sArbitrary

instance SArbitrary (f p) => SArbitrary (Rec1 f p) where
  sArbitrary = (`bindSS` SomeSing . SRec1) <$> sArbitrary

instance SArbitrary (f (g p)) => SArbitrary ((f :.: g) p) where
  sArbitrary = (`bindSS` SomeSing . SComp1) <$> sArbitrary

data Box where
  Box :: (NFData a, IsFunction a ~ False) => a -> Box

type family IsFunction (a :: Type) :: Bool where
  IsFunction (_ -> _) = True
  IsFunction _        = False

instance Testable Box where
  property = property . liftBox
    where
      liftBox :: Box -> Result
      liftBox (Box x) = x `deepseq` succeeded

infixl 1 `bindSS`
bindSS :: SomeSing a -> (forall (x :: a). Sing x -> r) -> r
bindSS (SomeSing x) f = f x

sequenceSS :: [SomeSing a] -> SomeSing [a]
sequenceSS = go
  where
    go []     = SomeSing SNil
    go (x:xs) =         x     `bindSS`
                \x'  -> go xs `bindSS`
                \xs' -> SomeSing (SCons x' xs')
