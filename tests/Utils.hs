module Utils (concatSequence) where

import Control.Monad.Extra

concatSequence :: Monad m => [m [a]] -> m [a]
concatSequence = concatMapM id
