{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Monoid (
    Monoid(..), PMonoid(..), SMonoid(..), GMonoid(..), VMonoid(..)
  , genericMempty, GenericMempty, sGenericMempty
  , defaultMonoidLeftIdentity, defaultMonoidRightIdentity, defaultMonoidMappend

    -- * Template Haskell
  , deriveMonoidAll, deriveMonoidSingletons, deriveMonoidVerifiedOnly
  ) where

import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Generics
import VerifiedClasses.Semigroup
import VerifiedClasses.TH.Deriving

genericMempty :: forall a. (Generic a, Monoid (Rep a ())) => a
genericMempty = to (mempty :: Rep a ())

type GenericMempty :: a
type family GenericMempty @a where
  GenericMempty @a = (To (Mempty :: Rep a ()) :: a)

sGenericMempty :: forall a. (SGeneric a, SMonoid (Rep a ()))
               => Sing (GenericMempty :: a)
sGenericMempty = sTo @a @() sMempty

$(singletonsOnly [d|
  instance Monoid (U1 p) where
    mempty = U1

  instance Monoid c => Monoid (K1 i c p) where
    mempty = K1 mempty

  instance Monoid (f p) => Monoid (M1 i c f p) where
    mempty = M1 mempty

  instance (Monoid (f p), Monoid (g p)) => Monoid ((f :*: g) p) where
    mempty = mempty :*: mempty
  |])

class (Monoid a, PMonoid a, SMonoid a, VSemigroup a) => VMonoid a where
  monoidLeftIdentity  :: forall (x :: a). Sing x -> (Mempty <> x) :~: x
  monoidRightIdentity :: forall (x :: a). Sing x -> (x <> Mempty) :~: x
  monoidMappend       :: forall (x :: a) (y :: a).
                         Sing x -> Sing y -> Mappend x y :~: (x <> y)

defaultMonoidLeftIdentity :: forall a (x :: a).
                             (VGeneric a, SMonoid a, VMonoid (Rep a ()), GMonoid a)
                          => Sing x -> (Mempty <> x) :~: x
defaultMonoidLeftIdentity sx
  | Refl <- genericMemptyC @a
  , Refl <- genericAppendC (sMempty @a) sx
  , Refl <- sFot @a (sMempty @(Rep a ()))
  , Refl <- monoidLeftIdentity (sFrom @a @() @x sx)
  , Refl <- sTof @a @() @x sx
  = Refl

defaultMonoidRightIdentity :: forall a (x :: a).
                              (VGeneric a, SMonoid a, VMonoid (Rep a ()), GMonoid a)
                           => Sing x -> (x <> Mempty) :~: x
defaultMonoidRightIdentity sx
  | Refl <- genericMemptyC @a
  , Refl <- genericAppendC sx (sMempty @a)
  , Refl <- sFot @a (sMempty @(Rep a ()))
  , Refl <- monoidRightIdentity (sFrom @a @() @x sx)
  , Refl <- sTof @a @() @x sx
  = Refl

defaultMonoidMappend :: forall a (x :: a) (y :: a).
                        GMonoid a
                     => Sing x -> Sing y -> Mappend x y :~: (x <> y)
defaultMonoidMappend = mappendAppendC

monoidClass :: Class
monoidClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod 'mempty 'genericMempty TermLevel
                          ]
  }

pMonoidClass :: Class
pMonoidClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''Mempty ''GenericMempty (TypeLevel 0)
                          ]
  }

sMonoidClass :: Class
sMonoidClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod 'sMempty 'sGenericMempty TermLevel
                          ]
  }

vMonoidClass :: Class
vMonoidClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'monoidLeftIdentity  'defaultMonoidLeftIdentity  TermLevel
                          , ClassMethod 'monoidRightIdentity 'defaultMonoidRightIdentity TermLevel
                          , ClassMethod 'monoidMappend       'defaultMonoidMappend       TermLevel
                          ]
  }

deriveMonoidAll :: Q TH.Type -> Q [Dec]
deriveMonoidAll = deriveClasses [monoidClass, pMonoidClass, sMonoidClass, gClass, vMonoidClass]

deriveMonoidSingletons :: Q TH.Type -> Q [Dec]
deriveMonoidSingletons = deriveClasses [pMonoidClass, sMonoidClass, gClass, vMonoidClass]

deriveMonoidVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveMonoidVerifiedOnly = deriveClass vMonoidClass

class GSemigroup a => GMonoid a where
  genericMemptyC         :: (Mempty :: a) :~: GenericMempty
  default genericMemptyC :: (Mempty :: a)  ~  GenericMempty
                         => (Mempty :: a) :~: GenericMempty
  genericMemptyC = Refl

  mappendAppendC         :: forall (x :: a) (y :: a).
                            Sing x -> Sing y -> (Mappend x y) :~: (x <> y)
  default mappendAppendC :: forall (x :: a) (y :: a).
                            (Mappend x y ~ (x <> y))
                         => Sing x -> Sing y -> (Mappend x y) :~: (x <> y)
  mappendAppendC _ _ = Refl

instance VMonoid (U1 p) where
  monoidLeftIdentity  SU1 = Refl
  monoidRightIdentity SU1 = Refl
  monoidMappend _ _ = Refl

instance VMonoid c => VMonoid (K1 i c p) where
  monoidLeftIdentity  (SK1 c) | Refl <- monoidLeftIdentity  c = Refl
  monoidRightIdentity (SK1 c) | Refl <- monoidRightIdentity c = Refl
  monoidMappend _ _ = Refl

instance VMonoid (f p) => VMonoid (M1 i c f p) where
  monoidLeftIdentity  (SM1 c) | Refl <- monoidLeftIdentity  c = Refl
  monoidRightIdentity (SM1 c) | Refl <- monoidRightIdentity c = Refl
  monoidMappend _ _ = Refl

instance (VMonoid (f p), VMonoid (g p)) => VMonoid ((f :*: g) p) where
  monoidLeftIdentity (x :%*: y)
    | Refl <- monoidLeftIdentity x
    , Refl <- monoidLeftIdentity y
    = Refl
  monoidRightIdentity (x :%*: y)
    | Refl <- monoidRightIdentity x
    , Refl <- monoidRightIdentity y
    = Refl
  monoidMappend _ _ = Refl
