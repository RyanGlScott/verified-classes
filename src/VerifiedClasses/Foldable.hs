{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoNamedWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Foldable () where

import Data.Singletons.Base.TH
import GHC.Generics
import VerifiedClasses.Generics

$(singletonsOnly [d|
  deriving instance Foldable V1
  deriving instance Foldable U1
  deriving instance Foldable (K1 i c)
  deriving instance Foldable f => Foldable (M1 i c f)
  deriving instance (Foldable f, Foldable g) => Foldable (f :*: g)
  deriving instance (Foldable f, Foldable g) => Foldable (f :+: g)
  deriving instance Foldable Par1
  deriving instance Foldable f => Foldable (Rec1 f)
  deriving instance (Foldable f, Foldable g) => Foldable (f :.: g)
  |])
