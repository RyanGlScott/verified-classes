module VerifiedClasses.TH.Internal where

import Data.Kind
import qualified Language.Haskell.TH as TH (Type)
import Language.Haskell.TH hiding (Type)

data Repf :: Type -> Type -> Type
data Rep1f :: forall k. (k -> Type) -> k -> Type

decomposeType :: TH.Type -> (Cxt, TH.Type)
decomposeType (ForallT _tvbs ctxt ty) = (ctxt, ty)
decomposeType ty                      = ([],   ty)
