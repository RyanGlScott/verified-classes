module VerifiedClasses.TH.Deriving (
    deriveClass, deriveClasses
  , Class(..), gClass
  , ClassSort(..), ClassMethod(..), ClassMethodSort(..)
  ) where

import Control.Monad.Extra
import Data.Char
import Data.Singletons.TH.Options
import Language.Haskell.TH
import Language.Haskell.TH.Desugar
import VerifiedClasses.TH.Internal

data Class = Class
  { clsSort             :: ClassSort
  , clsMethsAndDefaults :: [ClassMethod]
  } deriving Show

data ClassSort
  = Original
  | Promoted
  | Singled
  | Coincided
  | Verified
  deriving Show

data ClassMethod = ClassMethod
  { clsMethName    :: Name
  , clsMethDefault :: Name
  , clsMethSort    :: ClassMethodSort
  } deriving Show

data ClassMethodSort
  = TermLevel
  | TypeLevel Int -- How many arguments the type family accepts
  deriving Show

gClass :: Class
gClass = Class
  { clsSort             = Coincided
  , clsMethsAndDefaults = []
  }

deriveClasses :: [Class] -> Q Type -> Q [Dec]
deriveClasses classes qInstTy = concatMapM (`deriveClass` qInstTy) classes

deriveClass :: Class -> Q Type -> Q [Dec]
deriveClass Class{clsSort, clsMethsAndDefaults} qInstTy = do
  instTy <- qInstTy
  let (instCxt, instHead) = decomposeType instTy
      prefixClasses = modifyConNameType $ prefixTyCons clsSort
      instCxt' = map prefixClasses instCxt
      (instCls, instClsArgs) = unfoldType instHead
      instCls' = prefixClasses instCls
      instHead' = applyType instCls' instClsArgs

  let mkMethodDefault :: Name -> Name -> ClassMethodSort -> Q Dec
      mkMethodDefault methName defaultName methSort =
        case methSort of
          TermLevel         -> pure $ ValD (VarP methName) (NormalB (VarE defaultName)) []
          TypeLevel numArgs -> do argNames <- replicateM numArgs (newName "a")
                                  let argTys = map VarT argNames
                                  pure $ TySynInstD
                                       $ TySynEqn Nothing
                                                  (foldl' AppT (ConT methName)    argTys)
                                                  (foldl' AppT (ConT defaultName) argTys)

      mkMethodDefaults :: Q [Dec]
      mkMethodDefaults = traverse (\ClassMethod{clsMethName, clsMethDefault, clsMethSort} ->
                                    mkMethodDefault clsMethName clsMethDefault clsMethSort)
                                  clsMethsAndDefaults

  methodDefaults <- mkMethodDefaults
  pure $ case clsSort of
    Coincided -> [StandaloneDerivD (Just AnyclassStrategy) [WildCardT] instHead']
    _         -> [InstanceD Nothing instCxt' instHead' methodDefaults]

prefixTyCons :: ClassSort -> Name -> Name
prefixTyCons Original  = id
prefixTyCons Promoted  = promotedClassName defaultOptions
prefixTyCons Singled   = singledClassName  defaultOptions
prefixTyCons Coincided = prefixName "G" "^"
prefixTyCons Verified  = prefixName "V" "&"

-- Walk a Type, applying a function to all occurrences of constructor names.
modifyConNameType :: (Name -> Name) -> Type -> Type
modifyConNameType mod_con_name = go
  where
    go (ConT n)                  = ConT (mod_con_name n)
    go (ForallT tvbs ctxt p)     = ForallT tvbs (map go ctxt) (go p)
    go (ForallVisT tvbs p)       = ForallVisT tvbs (go p)
    go (AppT     p t)            = AppT     (go p) t
    go (AppKindT p k)            = AppKindT (go p) k
    go (SigT     p k)            = SigT     (go p) k
    go (InfixT t1 n t2)          = InfixT (go t1) n (go t2)
    go (UInfixT t1 n t2)         = UInfixT (go t1) n (go t2)
    go (PromotedInfixT t1 n t2)  = PromotedInfixT (go t1) n (go t2)
    go (PromotedUInfixT t1 n t2) = PromotedUInfixT (go t1) n (go t2)
    go (ParensT t)               = ParensT (go t)
    go (ImplicitParamT n t)      = ImplicitParamT n (go t)
    go p@(VarT _)                = p
    go p@WildCardT               = p
    go p@(LitT {})               = p
    go p@ArrowT                  = p
    go p@MulArrowT               = p
    go p@(PromotedT {})          = p
    go p@(TupleT {})             = p
    go p@(UnboxedTupleT {})      = p
    go p@(UnboxedSumT {})        = p
    go p@EqualityT               = p
    go p@ListT                   = p
    go p@StarT                   = p
    go p@ConstraintT             = p
    go p@PromotedNilT            = p
    go p@(PromotedConsT {})      = p
    go p@(PromotedTupleT {})     = p

-----
-- Taken directly from singletons
-----

-- Put a prefix on a name. Takes two prefixes: one for identifiers
-- and one for symbols.
prefixName :: String -> String -> Name -> Name
prefixName pre tyPre n =
  let str = nameBase n
      first = headNameStr str in
    if isHsLetter first
     then mkName (pre ++ str)
     else mkName (tyPre ++ str)

-- is it a letter or underscore?
isHsLetter :: Char -> Bool
isHsLetter c = isLetter c || c == '_'

-- Return the first character in a Name's string (i.e., nameBase).
-- Precondition: the string is non-empty.
headNameStr :: String -> Char
headNameStr str =
  case str of
    (c:_) -> c
    [] -> error "headNameStr: Expected non-empty string"
