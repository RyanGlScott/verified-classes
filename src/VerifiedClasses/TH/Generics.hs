{-# LANGUAGE TemplateHaskellQuotes #-}

module VerifiedClasses.TH.Generics (
    verifyGeneric0
  , verifyGeneric1
  , verifyGeneric0And1
  , verifyWithTypeGeneric

  , genSingletonsGeneric0
  , genSingletonsGeneric1
  , genSingletonsGeneric0And1
  , genSingletonsWithTypeGeneric
  ) where

import Control.Monad.Extra
import Data.Data (Data)
import Data.Generics (everywhere, mkT)
import qualified Data.Kind as Kind
import Data.List.Extra
import Data.Maybe
import Data.Singletons
import Data.Singletons.TH (singletonsOnly)
import qualified Data.Singletons.TH.Options as SingTH (defaultOptions)
import Data.Singletons.TH.Options (singledDataConName)
import Data.Type.Equality ((:~:)(..))
import qualified Generics.Deriving.TH as GenTH (Options, defaultOptions)
import Generics.Deriving.TH hiding (Options, defaultOptions)
import GHC.Generics
import Language.Haskell.TH
import Language.Haskell.TH.Datatype
import Language.Haskell.TH.Desugar
import VerifiedClasses.TH.Internal

genSingletonsGeneric0 :: Name -> Q [Dec]
genSingletonsGeneric0 = genSingletonsGeneric deriveAll0Options

verifyGeneric0 :: Name -> Q [Dec]
verifyGeneric0 = verifyGeneric deriveAll0Options

genSingletonsGeneric1 :: Name -> Q [Dec]
genSingletonsGeneric1 = genSingletonsGeneric deriveAll1Options

verifyGeneric1 :: Name -> Q [Dec]
verifyGeneric1 = verifyGeneric deriveAll1Options

genSingletonsGeneric0And1 :: Name -> Q [Dec]
genSingletonsGeneric0And1 = genSingletonsGeneric deriveAll0And1Options

verifyGeneric0And1 :: Name -> Q [Dec]
verifyGeneric0And1 = verifyGeneric deriveAll0And1Options

genSingletonsGeneric :: (GenTH.Options -> Name -> Q [Dec]) -> Name -> Q [Dec]
genSingletonsGeneric deriveIt n = do
  decs <- deriveIt defaultOptionsEC n
  singletonsOnlyWithHackery decs

verifyGeneric :: (GenTH.Options -> Name -> Q [Dec]) -> Name -> Q [Dec]
verifyGeneric deriveIt n = do
  decs <- deriveIt defaultOptionsEC n
  let giis = excavateGenericInstInfo decs
  decs1 <- concatMapM deriveVGeneric giis
  decs2 <- singletonsOnlyWithHackery decs
  pure $ decs1 ++ decs2

defaultOptionsEC :: GenTH.Options
defaultOptionsEC = GenTH.defaultOptions
  { -- EmptyCase is easier to handle than calls to the `error` function
    emptyCaseOptions = True
    -- See (1) in Note [TH hackery]
  , kindSigOptions = False
  }

data GenericClass = Generic | Generic1

genSingletonsWithTypeGeneric :: Q Type -> Q [Dec]
genSingletonsWithTypeGeneric = singletonsWithTypeGeneric False

verifyWithTypeGeneric :: Q Type -> Q [Dec]
verifyWithTypeGeneric = singletonsWithTypeGeneric True

singletonsWithTypeGeneric :: Bool -> Q Type -> Q [Dec]
singletonsWithTypeGeneric verify qInstHeader = do
  instHeader <- qInstHeader
  let (instCtxt, instHead) = decomposeType instHeader
      (clsName, clsArg, dataName) =
        case instHead of
          AppT cls' clsArg'
            |  (dataTy', _)   <- unfoldType clsArg'
            ,  Just clsName'  <- typeName_maybe cls'
            ,  Just dataName' <- typeName_maybe dataTy'
            -> (clsName', clsArg', dataName')
          _ -> error $ "Unexpected instance head: " ++ pprint instHead
      genCls
        | clsName == ''Generic  = Generic
        | clsName == ''Generic1 = Generic1
        | otherwise = error $ "Unexpected class: " ++ nameBase clsName
      qInstCtxt = pure $ foldl' AppT (TupleT (length instCtxt)) instCtxt
      qClsArg   = pure clsArg
  decs <-
    case genCls of
      Generic ->
        [d| instance $(qInstCtxt) => Generic $(qClsArg) where
              type Rep $(qClsArg) = $(makeRep0Inline dataName qClsArg)
              from = $(makeFrom0 dataName)
              to   = $(makeTo0   dataName)
          |]
      Generic1 ->
        [d| instance $(qInstCtxt) => Generic1 $(qClsArg) where
              type Rep1 $(qClsArg) = $(makeRep1Inline dataName qClsArg)
              from1 = $(makeFrom1 dataName)
              to1   = $(makeTo1   dataName)
          |]
  let giis = excavateGenericInstInfo decs
  decs1 <- if verify
              then concatMapM deriveVGeneric giis
              else pure []
  decs2 <- singletonsOnlyWithHackery decs
  pure $ decs1 ++ decs2

typeName_maybe :: Type -> Maybe Name
typeName_maybe (ConT n)                = Just n
typeName_maybe (VarT n)                = Just n
typeName_maybe (TupleT n)              = Just $ tupleTypeName n
typeName_maybe ArrowT                  = Just ''(->)
typeName_maybe MulArrowT               = Just ''Kind.FUN
typeName_maybe ListT                   = Just ''[]
typeName_maybe (UnboxedTupleT n)       = Just $ unboxedTupleTypeName n
typeName_maybe (UnboxedSumT n)         = Just $ unboxedSumTypeName n
typeName_maybe EqualityT               = Just ''(~)
typeName_maybe (PromotedT n)           = Just n
typeName_maybe (InfixT _ n _)          = Just n
typeName_maybe (UInfixT _ n _)         = Just n
typeName_maybe (PromotedInfixT _ n _)  = Just n
typeName_maybe (PromotedUInfixT _ n _) = Just n
typeName_maybe (PromotedTupleT n)      = Just $ tupleDataName n
typeName_maybe PromotedNilT            = Just '[]
typeName_maybe PromotedConsT           = Just '(:)
typeName_maybe StarT                   = Just ''Kind.Type
typeName_maybe ConstraintT             = Just ''Kind.Constraint
typeName_maybe ForallT{}               = Nothing
typeName_maybe ForallVisT{}            = Nothing
typeName_maybe SigT{}                  = Nothing
typeName_maybe AppT{}                  = Nothing
typeName_maybe AppKindT{}              = Nothing
typeName_maybe ParensT{}               = Nothing
typeName_maybe LitT{}                  = Nothing
typeName_maybe WildCardT               = Nothing
typeName_maybe ImplicitParamT{}        = Nothing

{-
Note [TH hackery]
~~~~~~~~~~~~~~~~~
In an ideal world, promoting and singling Generic(1) instances would be a simple
matter of doing the following:

$(singletonsOnly (deriveAll0And1 <name>))

Alas, we do not live in an ideal world. This Note is dedicated to explaining
all of the gross hacks that we must employ to make this happen.

1. We disable `kindSigOptions` in `defaultOptionsEC` to ensure that we generate
   instances of the form `instance Generic1 T` rather than
   `instance Generic1 (T :: k -> Type)`. Unfortunately, the extra kind signature
   tends to wreak havoc with singletons' lambda lifting.
2. After promoting/singling, we replace occurrences of `Apply From1Sym0` and
   `ApplyTo1Sym0` with `From1` and `To1`, respectively, since there are no
   `Apply` instances for `From1Sym0` and `To1Sym0`.
   See Note Note [Defunctionalizing From/To] in VerifiedClasses.Generics.
-}

singletonsOnlyWithHackery :: [Dec] -> Q [Dec]
singletonsOnlyWithHackery decs = uglyPostHackery <$> singletonsOnly (pure decs)

uglyPostHackery :: Data a => a -> a
uglyPostHackery = everywhere (mkT unApplyFrom1To1)
  where
    -- See (2) in Note [TH hackery]
    unApplyFrom1To1 :: Type -> Type
    unApplyFrom1To1 (ConT applyN `AppT` ConT n)
      | applyN == ''Apply && nameBase n == "From1Sym0"
      = ConT $ mkName "From1"
      | applyN == ''Apply && nameBase n == "To1Sym0"
      = ConT $ mkName "To1"
    unApplyFrom1To1 t = t

-----
-- Deriving VGeneric(1) instances
-----

deriveVGeneric :: GenericInstInfo -> Q [Dec]
deriveVGeneric (GII{ giiClass = genCls, giiClassArg = clsArg
                   , giiDataName = dataName, giiRep = repTy }) =
  (:[]) <$> case genCls of
              Generic  -> mkInst vGenericName  sTofName  sFotName
              Generic1 -> mkInst vGeneric1Name sTof1Name sFot1Name
  where
    mkInst :: Name -> Name -> Name -> Q Dec
    mkInst clsName tofName fotName = do
      z <- newName "z"
      tofImpl <- deriveSTof dataName z
      fotImpl <- deriveSFot repTy z
      pure $ InstanceD Nothing [] (AppT (ConT clsName) clsArg)
               [ FunD tofName [Clause [VarP z] (NormalB tofImpl) []]
               , FunD fotName [Clause [VarP z] (NormalB fotImpl) []]
               ]

    vGenericName, vGeneric1Name, sTofName, sTof1Name, sFotName, sFot1Name :: Name
    vGenericName  = mkName "VGeneric"
    vGeneric1Name = mkName "VGeneric1"
    sTofName      = mkName "sTof"
    sTof1Name     = mkName "sTof1"
    sFotName      = mkName "sFot"
    sFot1Name     = mkName "sFot1"

deriveSTof :: Name -> Name -> Q Exp
deriveSTof tyName var = CaseE (VarE var) <$> cases
  where
    cases :: Q [Match]
    cases = do
      DatatypeInfo{datatypeCons = constrs} <- reifyDatatype tyName
      traverse (reflMatchQ . constructorName) constrs

deriveSFot :: Type -> Name -> Q Exp
deriveSFot typ var = caseE (varE var) (cases typ)
  where
    cases :: Type -> [Q Match]
    cases ty
      | tyHead == ''D1
      , [_,f] <- tyArgs
      = map (prependPat 'M1) (cases f)
      | tyHead == ''(:+:)
      , [f,g] <- tyArgs
      = map (prependPat 'L1) (cases f) ++ map (prependPat 'R1) (cases g)
      | tyHead == ''C1
      , [_,f] <- tyArgs
      = [prependPat 'M1 (leafCases f)]
      | tyHead == ''V1
      , [] <- tyArgs
      = (:[]) $ do
          x <- newName "x"
          pure $ Match (VarP x) (NormalB (CaseE (VarE x) [])) []
      | otherwise
      = [fail $ "Unexpected type " ++ pprint ty]
      where
        (tyHead, tyArgs) = unfoldDataTypeApp ty

    leafCases :: Type -> Q Match
    leafCases ty
      | tyHead == ''S1
      , [_, f] <- tyArgs
      = prependPat 'M1 (leafCases f)
      | tyHead == ''(:*:)
      , [f, g] <- tyArgs
      = do Match p1 b1 ds1 <- leafCases f
           Match p2 _  _   <- leafCases g
           pure $ Match (ConP (singDataConName '(:*:)) [] [p1, p2]) b1 ds1
      | tyHead == ''U1
      , [] <- tyArgs
      = reflMatchQ 'U1
      | tyHead == ''Rec0
      , [_] <- tyArgs
      = reflMatchQ 'K1
      | tyHead == ''Par1
      , [] <- tyArgs
      = reflMatchQ 'Par1
      | tyHead == ''Rec1
      , [_] <- tyArgs
      = reflMatchQ 'Rec1
      | otherwise
      = fail $ "Unexpected type " ++ pprint ty
      where
        (tyHead, tyArgs) = unfoldDataTypeApp ty

    prependPat :: Name -> Q Match -> Q Match
    prependPat pn qMatch = do
      Match p b ds <- qMatch
      pure $ Match (ConP (singDataConName pn) [] [p]) b ds

    unfoldDataTypeApp :: Type -> (Name, [Type])
    unfoldDataTypeApp ty =
      let (tyHead, tyArgs) = fmap filterTANormals $ unfoldType ty in
      case tyHead of
        ConT tyHeadName -> (tyHeadName, tyArgs)
        _               -> error $ "Expected data type constructor application, received "
                               ++ pprint ty

data GenericInstInfo = GII
  { giiClass    :: GenericClass
  , giiClassArg :: Type
  , giiDataName :: Name
  , giiRep      :: Type
  }

excavateGenericInstInfo :: [Dec] -> [GenericInstInfo]
excavateGenericInstInfo = mapMaybe digForInst
  where
    digForInst :: Dec -> Maybe GenericInstInfo
    digForInst (InstanceD _ _ instTy decs)
      | AppT (ConT clsName) argTy <- instTy
      , (dataTy, _)   <- unfoldType argTy
      , Just dataName <- typeName_maybe dataTy
      , Just gClass   <- ascertainGenericClass clsName
      , Just repTy    <- excavateRep decs
      = Just $ GII{ giiClass = gClass, giiClassArg = argTy
                  , giiDataName = dataName, giiRep = repTy }
    digForInst _ = Nothing

    excavateRep :: [Dec] -> Maybe Type
    excavateRep = firstJust digForRep

    digForRep :: Dec -> Maybe Type
    digForRep (TySynInstD (TySynEqn _ lhs rhs))
      | (ConT fn, _) <- unfoldType lhs
      , fn == ''Rep || fn == ''Rep1
      = Just rhs
    digForRep _
      = Nothing

    ascertainGenericClass :: Name -> Maybe GenericClass
    ascertainGenericClass n
      | n == ''Generic  = Just Generic
      | n == ''Generic1 = Just Generic1
      | otherwise       = Nothing

reflMatchQ :: Name -> Q Match
reflMatchQ n = pure $ Match (RecP (singDataConName n) []) (NormalB (ConE 'Refl)) []

singDataConName :: Name -> Name
singDataConName = singledDataConName SingTH.defaultOptions
