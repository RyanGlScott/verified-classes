{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-noncanonical-monad-instances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Monad (
    Monad(..), PMonad(..), SMonad(..), GMonad(..), VMonad(..)
  , genericBind, GenericBind, sGenericBind
  , defaultMonadLeftIdentity, defaultMonadRightIdentity
  , defaultMonadAssociativity, defaultMonadReturn, defaultMonadSeq

    -- * Template Haskell
  , deriveMonadAll, deriveMonadSingletons, deriveMonadVerifiedOnly

    -- * Miscellaneous utilties
  , fstP, FstP, sFstP, FstPSym0, FstPSym1
  , sndP, SndP, sSndP, SndPSym0, SndPSym1
  , liftM', LiftM', sLiftM', LiftM'Sym0, LiftM'Sym1, LiftM'Sym2
  ) where

import Data.Kind
import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Applicative
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.TH.Deriving
import VerifiedClasses.Utils

infixl 1 `genericBind`
genericBind :: forall m a b. (Generic1 m, Monad (Rep1 m))
            => m a -> (a -> m b) -> m b
x `genericBind` f = to1 (from1 x >>= from1 . f)

infixl 1 `GenericBind`
type GenericBind :: m a -> (a ~> m b) -> m b
type family x `GenericBind` f where
  x `GenericBind` f = To1 (From1 x >>= From1Sym0 .@#@$$$ f)

infixl 1 `sGenericBind`
sGenericBind :: forall m a b (x :: m a) (f :: a ~> m b).
                (SGeneric1 m, SMonad (Rep1 m))
             => Sing x -> Sing f -> Sing (x `GenericBind` f)
sx `sGenericBind` sf = sTo1 (sFrom1 sx %>>= singFun1 @From1Sym0 sFrom1' %.$$$ sf)

$(singletons [d|
  fstP :: (f :*: g) p -> f p
  fstP (a :*: _) = a

  sndP :: (f :*: g) p -> g p
  sndP (_ :*: b) = b

  -- TODO RGS: Explain why we need this
  liftM' :: Monad m => (a -> b) -> m a -> m b
  liftM' f m = m >>= return . f
  |])

$(singletonsOnly [d|
  instance Monad U1 where
    _ >>= _ = U1
    -- This definition of (>>), as well as subsequent definitions of (>>), are
    -- noncanonical. Nevertheless, it's easier to prove statements about
    -- ConstSym1 than it is to prove statements about inscrutable lambda-lifted
    -- type families, so we make an exception here.
    x >> y = x >>= const y

  instance Monad f => Monad (M1 i c f) where
    M1 x >>= f = M1 (x >>= unM1 . f)
    x >> y = x >>= const y

  instance (Monad f, Monad g) => Monad (f :*: g) where
    (m :*: n) >>= f = (m >>= fstP . f) :*: (n >>= sndP . f)
    x >> y = x >>= const y

  instance Monad Par1 where
    Par1 x >>= f = f x
    x >> y = x >>= const y

  instance Monad f => Monad (Rec1 f) where
    Rec1 x >>= f = Rec1 (x >>= unRec1 . f)
    x >> y = x >>= const y
  |])

class (Monad m, PMonad m, SMonad m, VApplicative m) => VMonad m where
  monadLeftIdentity  :: forall a b (x :: a) (k :: a ~> m b).
                        Sing x -> Sing k
                     -> (Return x >>= k) :~: (k @@ x)

  monadRightIdentity :: forall a (x :: m a).
                        Sing x -> (x >>= ReturnSym0) :~: x

  monadAssociativity :: forall a b c (x :: m a) (k :: a ~> m b) (h :: b ~> m c).
                        Sing x -> Sing k -> Sing h
                     -> (x >>= ((=<<@#@$$) h .@#@$$$ k)) :~: ((x >>= k) >>= h)

  monadReturn :: forall a (x :: a).
                 Sing x
              -> (Return x :: m a) :~: Pure x

  -- TODO: This should probably be (>>) = (*>) instead.
  monadSeq    :: forall a b (x :: m a) (y :: m b).
                 Sing x -> Sing y
              -> (x >> y) :~: (x >>= ConstSym1 y)

defaultMonadLeftIdentity :: forall m a b (x :: a) (k :: a ~> m b).
                            (VGeneric1 m, VMonad (Rep1 m), GMonad m)
                         => Sing x -> Sing k
                         -> (Return x >>= k) :~: (k @@ x)
defaultMonadLeftIdentity sx sk
  | Refl <- genericPureC @m sx
  , Refl <- genericBindC (sReturn sx) sk
  , Refl <- sFot1 @Type @m (sReturn sx)
  , Refl <- monadLeftIdentity @(Rep1 m) sx (singFun1 @From1Sym0 sFrom1' %.$$$ sk)
  , Refl <- applyFrom1Sym0 @Type @m skx
  , Refl <- sTof1 skx
  , Refl <- returnPureC @m sx
  , Refl <- monadReturn @(Rep1 m) sx
  = Refl
  where
    skx :: Sing (k @@ x)
    skx = sk @@ sx

defaultMonadRightIdentity :: forall m a (x :: m a).
                             (VGeneric1 m, VMonad (Rep1 m), GMonad m)
                          => Sing x -> (x >>= ReturnSym0) :~: x
defaultMonadRightIdentity sx
  | Refl <- genericBindC sx (singFun1 @ReturnSym0 sReturn)
  , Refl <- --     (From1 x >>= From1Sym0 .@#@$$$ ReturnSym0)
            -- :~: (From1 x >>= From1Sym0 .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0)
            Refl @((>>=@#@$$) (From1 x))
              `apply` (Refl @((.@#@$$) From1Sym0)
                        `apply` funExt @a @(m a)
                                       @ReturnSym0 @(To1Sym0 .@#@$$$ ReturnSym0)
                                       lemma1)
  , Refl <- --     (From1 x >>= From1Sym0 .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0)
            -- :~: (From1 x >>= ReturnSym0)
            Refl @((>>=@#@$$) (From1 x))
              `apply` funExt @a @(Rep1 m a)
                             @((From1Sym0 :: m a ~> Rep1 m a) .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0)
                             @ReturnSym0
                             lemma2
  , Refl <- monadRightIdentity (sFrom1 sx)
  , Refl <- sTof1 sx
  = Refl
  where
    lemma1 :: forall (z :: a). Sing z
           -> Return z :~: (To1Sym0 @@ Return z :: m a)
    lemma1 sz | Refl <- applyTo1Sym0 @Type @m (sReturn sz)
              , Refl <- returnPureC @m sz
              , Refl <- monadReturn @(Rep1 m) sz
              = genericPureC sz

    lemma2 :: forall (z :: a). Sing z
           -> (From1Sym0 @@ (To1Sym0 @@ Return z :: m a)) :~: Return z
    lemma2 sz | Refl <- applyTo1Sym0   @Type @m sReturnZ
              , Refl <- applyFrom1Sym0 @Type @m (sTo1 sReturnZ)
              , Refl <- sFot1          @Type @m sReturnZ
              = Refl
      where
        sReturnZ :: Sing (Return z :: Rep1 m a)
        sReturnZ = sReturn sz

defaultMonadAssociativity :: forall m a b c (x :: m a) (k :: a ~> m b) (h :: b ~> m c).
                             (VGeneric1 m, VMonad (Rep1 m), GMonad m)
                          => Sing x -> Sing k -> Sing h
                          -> (x >>= ((=<<@#@$$) h .@#@$$$ k)) :~: ((x >>= k) >>= h)
defaultMonadAssociativity sx sk sh
{-
(1) x >>= ((=<<) h . k)                                             [assumption]
    to1 (from1 x >>= from1 . ((=<<) h . k))                         [genericBindC, funExt]
(2) to1 (from1 x >>= (from1 . to1) . ((from1 . h) =<<) . from1 . k) [fot1, funExt]
(3) to1 (from1 x >>= ((from1 . h) =<<) . from1 . k)                 [monadRightIdentity]
(4) to1 ((from1 x >>= from1 . k) >>= from1 . h)                     [fot1]
    to1 (from1 (to1 (from1 x >>= from1 . k)) >>= from1 . h)         [assumption]
    ((x >>= k) >>= h)
-}
  | -- (1)
    Refl <- genericBindC sx (singFun1 @((=<<@#@$$) h) ((%=<<) sh) %.$$$ sk)
  , Refl <- --     (From1 x >>= From1Sym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
            -- :~: (From1 x >>= (From1Sym0 .@#@$$$ (To1Sym0 :: Rep1 m c ~> m c)) .@#@$$$ ((=<<@#@$$) (From1Sym0 .@#@$$$ h)) .@#@$$$ From1Sym0 .@#@$$$ k)
            Refl @((>>=@#@$$) (From1 x))
              `apply` funExt @a @(Rep1 m c)
                             @(From1Sym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
                             @((From1Sym0 .@#@$$$ (To1Sym0 :: Rep1 m c ~> m c)) .@#@$$$ ((=<<@#@$$) (From1Sym0 .@#@$$$ h)) .@#@$$$ From1Sym0 .@#@$$$ k)
                             lemma1
    -- (2)
  , Refl <- --     (From1 x >>= (From1Sym0 .@#@$$$ (To1Sym0 :: Rep1 m c ~> m c)) .@#@$$$ ((=<<@#@$$) (From1Sym0 .@#@$$$ h)) .@#@$$$ From1Sym0 .@#@$$$ k)
            -- :~: (From1 x >>= ((=<<@#@$$) (From1Sym0 .@#@$$$ h)) .@#@$$$ From1Sym0 .@#@$$$ k)
            Refl @((>>=@#@$$) (From1 x))
              `apply` funExt @a @(Rep1 m c)
                             @((From1Sym0 .@#@$$$ (To1Sym0 :: Rep1 m c ~> m c)) .@#@$$$ ((=<<@#@$$) (From1Sym0 .@#@$$$ h)) .@#@$$$ From1Sym0 .@#@$$$ k)
                             @(((=<<@#@$$) (From1Sym0 .@#@$$$ h)) .@#@$$$ From1Sym0 .@#@$$$ k)
                             lemma2
    -- (3)
  , Refl <- monadAssociativity sf1x sFrom1CompK sFrom1CompH
    -- (4)
  , Refl <- sFot1 @Type @m (sf1x %>>= sFrom1CompK)
  , Refl <- genericBindC sx sk
  , Refl <- genericBindC (sx %>>= sk) sh
  = Refl
  where
    sFrom1Fun :: forall z. Sing (From1Sym0 :: m z ~> Rep1 m z)
    sFrom1Fun = singFun1 sFrom1'

    sf1x :: Sing (From1 x)
    sf1x = sFrom1 sx

    sFrom1CompK :: Sing (From1Sym0 .@#@$$$ k)
    sFrom1CompK = sFrom1Fun %.$$$ sk

    sFrom1CompH :: Sing (From1Sym0 .@#@$$$ h)
    sFrom1CompH = sFrom1Fun %.$$$ sh

    lemma1 :: forall (z :: a). Sing z
           ->     (From1Sym0 @@ ((k @@ z) >>= h))
              :~: (From1Sym0 @@ ((To1Sym0 :: Rep1 m c ~> m c) @@ ((From1Sym0 @@ (k @@ z)) >>= From1Sym0 .@#@$$$ h)))
    lemma1 sz | Refl <- applyFrom1Sym0 skz
              , Refl <- applyTo1Sym0 @Type @m (sFrom1Fun @@ skz %>>= sFrom1Fun %.$$$ sh)
              , Refl <- genericBindC skz sh
              = Refl
      where
        skz :: Sing (k @@ z)
        skz = sk @@ sz

    lemma2 :: forall (z :: a). Sing z
           ->     (From1Sym0 @@ ((To1Sym0 :: Rep1 m c ~> m c) @@ ((From1Sym0 @@ (k @@ z)) >>= From1Sym0 .@#@$$$ h)))
              :~: ((From1Sym0 @@ (k @@ z)) >>= From1Sym0 .@#@$$$ h)
    lemma2 sz | Refl <- applyTo1Sym0   @Type @m blah
              , Refl <- applyFrom1Sym0 @Type @m (sTo1 blah)
              , Refl <- sFot1          @Type @m blah
              = Refl
       where
         blah :: Sing ((From1Sym0 @@ (k @@ z)) >>= From1Sym0 .@#@$$$ h)
         blah = sFrom1Fun @@ (sk @@ sz) %>>= sFrom1CompH

defaultMonadReturn :: forall m a (x :: a).
                      GMonad m
                   => Sing x
                   -> (Return x :: m a) :~: Pure x
defaultMonadReturn = returnPureC

defaultMonadSeq :: forall m a b (x :: m a) (y :: m b).
                   GMonad m
                => Sing x -> Sing y
                -> (x >> y) :~: (x >>= ConstSym1 y)
defaultMonadSeq = monadSeqBindC

monadClass :: Class
monadClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod '(>>=) 'genericBind TermLevel
                          , ClassMethod '(>>)  '(*>)        TermLevel
                          ]
  }

pMonadClass :: Class
pMonadClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''(>>=) ''GenericBind (TypeLevel 2)
                          ]
  }

sMonadClass :: Class
sMonadClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod '(%>>=) 'sGenericBind TermLevel
                          ]
  }

vMonadClass :: Class
vMonadClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'monadLeftIdentity  'defaultMonadLeftIdentity  TermLevel
                          , ClassMethod 'monadRightIdentity 'defaultMonadRightIdentity TermLevel
                          , ClassMethod 'monadAssociativity 'defaultMonadAssociativity TermLevel
                          , ClassMethod 'monadReturn        'defaultMonadReturn        TermLevel
                          , ClassMethod 'monadSeq           'defaultMonadSeq           TermLevel
                          ]
  }

deriveMonadAll :: Q TH.Type -> Q [Dec]
deriveMonadAll = deriveClasses [monadClass, pMonadClass, sMonadClass, gClass, vMonadClass]

deriveMonadSingletons :: Q TH.Type -> Q [Dec]
deriveMonadSingletons = deriveClasses [pMonadClass, sMonadClass, gClass, vMonadClass]

deriveMonadVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveMonadVerifiedOnly = deriveClass vMonadClass

class ( GApplicative m
      , Monad m, PMonad m, SMonad m
      , Monad (Rep1 m), PMonad (Rep1 m), SMonad (Rep1 m) )
      => GMonad m where
  genericBindC         :: forall a b (x :: m a) (f :: a ~> m b).
                          Sing x -> Sing f
                       -> (x >>= f) :~: (x `GenericBind` f)
  default genericBindC :: forall a b (x :: m a) (f :: a ~> m b).
                          (x >>= f) ~ (x `GenericBind` f)
                       => Sing x -> Sing f
                       -> (x >>= f) :~: (x `GenericBind` f)
  genericBindC _ _ = Refl

  returnPureC         :: forall a (x :: a).
                         Sing x
                      -> (Return x :: m a) :~: Pure x
  default returnPureC :: forall a (x :: a).
                         (Return x :: m a) ~ Pure x
                      => Sing x
                      -> (Return x :: m a) :~: Pure x
  returnPureC _ = Refl

  monadSeqBindC         :: forall a b (x :: m a) (y :: m b).
                           Sing x -> Sing y
                        -> (x >> y) :~: (x >>= ConstSym1 y)
  default monadSeqBindC :: forall a b (x :: m a) (y :: m b).
                           (x >> y) ~ (x >>= ConstSym1 y)
                        => Sing x -> Sing y
                        -> (x >> y) :~: (x >>= ConstSym1 y)
  monadSeqBindC _ _ = Refl

instance VMonad U1 where
  monadLeftIdentity sx sk | SU1 <- sk @@ sx = Refl
  monadRightIdentity SU1 = Refl
  monadAssociativity _ _ _ = Refl
  monadReturn _ = Refl
  monadSeq _ _ = Refl

instance VMonad f => VMonad (M1 i c' f) where
  monadLeftIdentity :: forall a b (x :: a) (k :: a ~> M1 i c' f b).
                       Sing x -> Sing k
                    -> (Return x >>= k) :~: (k @@ x)
  monadLeftIdentity sx sk
    | SM1{} <- sk @@ sx
    , Refl <- monadLeftIdentity sx (singFun1 @UnM1Sym0 sUnM1 %.$$$ sk)
    , Refl <- monadReturn @f sx
    = Refl

  monadRightIdentity :: forall a (x :: M1 i c' f a).
                        Sing x -> (x >>= ReturnSym0) :~: x
  monadRightIdentity (SM1 (fa :: Sing (fa :: f a)))
    | Refl <- -- (fa >>= UnM1Sym0 .@#@$$$ ReturnSym0) :~: (fa >>= ReturnSym0)
              monadRightIdentityLemma (singFun1 @(UnM1Sym0 :: M1 i c' f _ ~> f _) sUnM1)
                                      fa lemma
    , Refl <- monadRightIdentity fa
    = Refl
    where
      lemma :: forall (z :: a). Sing z -> UnM1 (Return z) :~: (Return z :: f a)
      lemma sz
        | Refl <- monadReturn @f sz
        = Refl

  monadAssociativity :: forall a b c (x :: M1 i c' f a)
                               (k :: a ~> M1 i c' f b) (h :: b ~> M1 i c' f c).
                        Sing x -> Sing k -> Sing h
                     -> (x >>= ((=<<@#@$$) h .@#@$$$ k)) :~: ((x >>= k) >>= h)
  monadAssociativity (SM1 (fa :: Sing (fa :: f a))) sk sh
    | Refl <- --     (fa >>= UnM1Sym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
              -- :~: (fa >>= ((=<<@#@$$) (UnM1Sym0 .@#@$$$ h) .@#@$$$ UnM1Sym0 .@#@$$$ k))
              monadAssociativityLemma sUnM1Fun fa sk sh lemma
    , Refl <- monadAssociativity fa (sUnM1Fun %.$$$ sk) (sUnM1Fun %.$$$ sh)
    = Refl
      where
        sUnM1Fun :: forall z. Sing (UnM1Sym0 :: M1 i c' f z ~> f z)
        sUnM1Fun = singFun1 sUnM1

        lemma :: forall (z :: a). Sing z
              -> UnM1 ((k @@ z) >>= h) :~: (UnM1 (k @@ z) >>= UnM1Sym0 .@#@$$$ h)
        lemma sz | SM1{} <- sk @@ sz = Refl

  monadReturn :: forall a (x :: a). Sing x -> (Return x :: M1 i c' f a) :~: Pure x
  monadReturn _ = Refl

  monadSeq :: forall a b (x :: M1 i c' f a) (y :: M1 i c' f b).
              Sing x -> Sing y -> (x >> y) :~: (x >>= ConstSym1 y)
  monadSeq _ _ = Refl

monadRightIdentityLemma
  :: forall a (outer :: Type -> Type) (inner :: Type -> Type)
              (outInSym0 :: forall z. outer z ~> inner z)
              (ia :: inner a).
     (forall z. Sing (outInSym0 :: outer z ~> inner z)) -> Sing ia
  -> (forall (x :: a). Sing x -> outInSym0 @@ (Return x) :~: Return x)
  -> (ia >>= outInSym0 .@#@$$$ ReturnSym0) :~: (ia >>= ReturnSym0)
monadRightIdentityLemma _ _ lemma
  = Refl @((>>=@#@$$) ia)
      `apply` funExt @a @(inner a)
                     @((outInSym0 :: outer a ~> inner a) .@#@$$$ ReturnSym0)
                     @ReturnSym0
                     lemma

monadAssociativityLemma
  :: forall a b c
            (outer :: Type -> Type)
            (inner :: Type -> Type)
            (outInSym0 :: forall z. outer z ~> inner z)
            (ia :: inner a)
            (k :: a ~> outer b) (h :: b ~> outer c).
     (forall z. Sing (outInSym0 :: outer z ~> inner z))
  -> Sing ia -> Sing k -> Sing h
  -> (forall (z :: a). Sing z -> outInSym0 @@ ((k @@ z) >>= h)
                             :~: (outInSym0 @@ (k @@ z) >>= outInSym0 .@#@$$$ h))
  ->     (ia >>= outInSym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
     :~: (ia >>= ((=<<@#@$$) (outInSym0 .@#@$$$ h) .@#@$$$ outInSym0 .@#@$$$ k))
monadAssociativityLemma _ _ _ _ lemma
  = Refl @((>>=@#@$$) ia)
      `apply` funExt @a @(inner c)
                     @(outInSym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
                     @((=<<@#@$$) (outInSym0 .@#@$$$ h) .@#@$$$ outInSym0 .@#@$$$ k)
                     lemma

instance (VMonad f, VMonad g) => VMonad (f :*: g) where
  monadLeftIdentity :: forall a b (x :: a) (k :: a ~> (f :*: g) b).
                       Sing x -> Sing k
                    -> (Return x >>= k) :~: (k @@ x)
  monadLeftIdentity sx sk
    | (:%*:){} <- sk @@ sx
    , Refl <- monadLeftIdentity sx (singFun1 @FstPSym0 sFstP %.$$$ sk)
    , Refl <- monadLeftIdentity sx (singFun1 @SndPSym0 sSndP %.$$$ sk)
    , Refl <- monadReturn @f sx
    , Refl <- monadReturn @g sx
    = Refl

  monadRightIdentity :: forall a (x :: (f :*: g) a).
                        Sing x -> (x >>= ReturnSym0) :~: x
  monadRightIdentity ((sfp :: Sing fp) :%*: (sgp :: Sing gp))
    | Refl <- monadRightIdentity sfp
    , Refl <- monadRightIdentity sgp
    , Refl <- -- (fp >>= (FstPSym0 .@#@$$$ ReturnSym0)) :~: (fp >>= ReturnSym0)
              monadRightIdentityLemma (singFun1 @(FstPSym0 :: (f :*: g) _ ~> f _) sFstP)
                                      sfp fstPLemma
    , Refl <- -- (gp >>= (SndPSym0 .@#@$$$ ReturnSym0)) :~: (gp >>= ReturnSym0)
              monadRightIdentityLemma (singFun1 @(SndPSym0 :: (f :*: g) _ ~> g _) sSndP)
                                      sgp sndPLemma
    = Refl
    where
      fstPLemma :: forall (z :: a). Sing z -> FstP (Return z) :~: (Return z :: f a)
      fstPLemma sz
        | Refl <- monadReturn @f sz
        = Refl

      sndPLemma :: forall (z :: a). Sing z -> SndP (Return z) :~: (Return z :: g a)
      sndPLemma sz
        | Refl <- monadReturn @g sz
        = Refl

  monadAssociativity :: forall a b c (x :: (f :*: g) a)
                               (k :: a ~> (f :*: g) b) (h :: b ~> (f :*: g) c).
                        Sing x -> Sing k -> Sing h
                     -> (x >>= ((=<<@#@$$) h .@#@$$$ k)) :~: ((x >>= k) >>= h)
  monadAssociativity ((sfp :: Sing fp) :%*: (sgp :: Sing gp)) sk sh
    | Refl <- --     (fp >>= FstPSym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
              -- :~: (fp >>= ((=<<@#@$$) (FstPSym0 .@#@$$$ h) .@#@$$$ FstPSym0 .@#@$$$ k))
              monadAssociativityLemma sFstPFun sfp sk sh fstPLemma
    , Refl <- --     (gp >>= SndPSym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
              -- :~: (gp >>= ((=<<@#@$$) (SndPSym0 .@#@$$$ h) .@#@$$$ SndPSym0 .@#@$$$ k))
              monadAssociativityLemma sSndPFun sgp sk sh sndPLemma
    , Refl <- monadAssociativity sfp (sFstPFun %.$$$ sk) (sFstPFun %.$$$ sh)
    , Refl <- monadAssociativity sgp (sSndPFun %.$$$ sk) (sSndPFun %.$$$ sh)
    = Refl
      where
        sFstPFun :: forall z. Sing (FstPSym0 :: (f :*: g) z ~> f z)
        sFstPFun = singFun1 sFstP

        sSndPFun :: forall z. Sing (SndPSym0 :: (f :*: g) z ~> g z)
        sSndPFun = singFun1 sSndP

        fstPLemma :: forall (z :: a). Sing z
                  -> FstP ((k @@ z) >>= h) :~: (FstP (k @@ z) >>= FstPSym0 .@#@$$$ h)
        fstPLemma sz | (:%*:){} <- sk @@ sz = Refl

        sndPLemma :: forall (z :: a). Sing z
                  -> SndP ((k @@ z) >>= h) :~: (SndP (k @@ z) >>= SndPSym0 .@#@$$$ h)
        sndPLemma sz | (:%*:){} <- sk @@ sz = Refl

  monadReturn :: forall a (x :: a). Sing x -> (Return x :: (f :*: g) a) :~: Pure x
  monadReturn _ = Refl

  monadSeq :: forall a b (x :: (f :*: g) a) (y :: (f :*: g) b).
              Sing x -> Sing y -> (x >> y) :~: (x >>= ConstSym1 y)
  monadSeq _ _ = Refl

instance VMonad Par1 where
  monadLeftIdentity _ _ = Refl
  monadRightIdentity SPar1{} = Refl
  monadAssociativity SPar1{} _ _ = Refl
  monadReturn _ = Refl
  monadSeq _ _ = Refl

instance VMonad f => VMonad (Rec1 f) where
  monadLeftIdentity :: forall a b (x :: a) (k :: a ~> Rec1 f b).
                       Sing x -> Sing k
                    -> (Return x >>= k) :~: (k @@ x)
  monadLeftIdentity sx sk
    | SRec1{} <- sk @@ sx
    , Refl <- monadLeftIdentity sx (singFun1 @UnRec1Sym0 sUnRec1 %.$$$ sk)
    , Refl <- monadReturn @f sx
    = Refl

  monadRightIdentity :: forall a (x :: Rec1 f a).
                        Sing x -> (x >>= ReturnSym0) :~: x
  monadRightIdentity (SRec1 (fa :: Sing (fa :: f a)))
    | Refl <- -- (fa >>= UnRec1Sym0 .@#@$$$ ReturnSym0) :~: (fa >>= ReturnSym0)
              monadRightIdentityLemma (singFun1 @UnRec1Sym0 sUnRec1) fa lemma
    , Refl <- monadRightIdentity fa
    = Refl
    where
      lemma :: forall (z :: a). Sing z -> UnRec1 (Return z) :~: (Return z :: f a)
      lemma sz
        | Refl <- monadReturn @f sz
        = Refl

  monadAssociativity :: forall a b c (x :: Rec1 f a)
                               (k :: a ~> Rec1 f b) (h :: b ~> Rec1 f c).
                        Sing x -> Sing k -> Sing h
                     -> (x >>= ((=<<@#@$$) h .@#@$$$ k)) :~: ((x >>= k) >>= h)
  monadAssociativity (SRec1 (fa :: Sing (fa :: f a))) sk sh
    | Refl <- --     (fa >>= UnRec1Sym0 .@#@$$$ ((=<<@#@$$) h .@#@$$$ k))
              -- :~: (fa >>= ((=<<@#@$$) (UnRec1Sym0 .@#@$$$ h) .@#@$$$ UnRec1Sym0 .@#@$$$ k))
              monadAssociativityLemma sUnRec1Fun fa sk sh lemma
    , Refl <- monadAssociativity fa (sUnRec1Fun %.$$$ sk) (sUnRec1Fun %.$$$ sh)
    = Refl
      where
        sUnRec1Fun :: forall z. Sing (UnRec1Sym0 :: Rec1 f z ~> f z)
        sUnRec1Fun = singFun1 sUnRec1

        lemma :: forall (z :: a). Sing z
              -> UnRec1 ((k @@ z) >>= h) :~: (UnRec1 (k @@ z) >>= UnRec1Sym0 .@#@$$$ h)
        lemma sz | SRec1{} <- sk @@ sz = Refl

  monadReturn :: forall a (x :: a). Sing x -> (Return x :: Rec1 f a) :~: Pure x
  monadReturn _ = Refl

  monadSeq :: forall a b (x :: Rec1 f a) (y :: Rec1 f b).
              Sing x -> Sing y -> (x >> y) :~: (x >>= ConstSym1 y)
  monadSeq _ _ = Refl
