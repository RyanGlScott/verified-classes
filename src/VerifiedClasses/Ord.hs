{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Ord (
    Ord(..), POrd(..), SOrd(..), GOrd(..), VOrd(..)
  , genericLeq, GenericLeq, sGenericLeq
  , defaultLeqTransitive, defaultLeqReflexive
  , defaultLeqAntisymmetric, defaultLeqTotal

    -- * Template Haskell
  , deriveOrdAll, deriveOrdSingletons, deriveOrdVerifiedOnly

    -- * Miscellaneous utilities
  , ltTransitive
  ) where

import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Eq
import VerifiedClasses.Generics
import VerifiedClasses.So
import VerifiedClasses.TH.Deriving

infix 4 `genericLeq`
genericLeq :: forall a. (Generic a, Ord (Rep a ()))
           => a -> a -> Bool
genericLeq x y = from @a @() x <= from y

infix 4 `GenericLeq`
type GenericLeq :: a -> a -> Bool
type family x `GenericLeq` y where
  (x :: a) `GenericLeq` (y :: a) = (From x :: Rep a ()) <= From y

infix 4 `sGenericLeq`
sGenericLeq :: forall a (x :: a) (y :: a).
               (SGeneric a, SOrd (Rep a ()))
            => Sing x -> Sing y -> Sing (x `GenericLeq` y)
sx `sGenericLeq` sy = sFrom @a @() @x sx %<= sFrom sy

$(singletonsOnly [d|
  instance Ord (V1 p) where
    _ <= _ = True

  instance Ord (U1 p) where
    _ <= _ = True

  instance Ord c => Ord (K1 i c p) where
    K1 x <= K1 y = x <= y

  instance Ord (f p) => Ord (M1 i c f p) where
    M1 x <= M1 y = x <= y

  instance (Ord (f p), Ord (g p)) => Ord ((f :*: g) p) where
    (x1 :*: y1) <= (x2 :*: y2) =
      if x1 == x2
         then y1 <= y2
         else x1 <= x2

  instance (Ord (f p), Ord (g p)) => Ord ((f :+: g) p) where
    L1 x <= L1 y = x <= y
    R1 x <= R1 y = x <= y
    L1 _ <= R1 _ = True
    R1 _ <= L1 _ = False
  |])

class (Ord a, POrd a, SOrd a, VEq a) => VOrd a where
  leqTransitive :: forall (x :: a) (y :: a) (z :: a).
                   Sing x -> Sing y -> Sing z
                -> So (x <= y) -> So (y <= z) -> So (x <= z)

  leqReflexive :: forall (x :: a). Sing x -> So (x <= x)

  leqAntisymmetric :: forall (x :: a) (y :: a).
                      Sing x -> Sing y
                   -> So (x <= y) -> So (y <= x) -> So (x == y)

  leqTotal :: forall (x :: a) (y :: a).
              Sing x -> Sing y
           -> Either (So (x <= y)) (So (y <= x), So (Not (y == x)))

defaultLeqTransitive :: forall a (x :: a) (y :: a) (z :: a).
                        (SGeneric a, VOrd (Rep a ()), GOrd a)
                     => Sing x -> Sing y -> Sing z
                     -> So (x <= y) -> So (y <= z) -> So (x <= z)
defaultLeqTransitive sx sy sz
  | Refl <- genericLeqC sx sy
  , Refl <- genericLeqC sy sz
  , Refl <- genericLeqC sx sz
  = leqTransitive (sFrom @a @() sx) (sFrom sy) (sFrom sz)

defaultLeqReflexive :: forall a (x :: a).
                       (SGeneric a, VOrd (Rep a ()), GOrd a)
                    => Sing x -> So (x <= x)
defaultLeqReflexive sx
  | Refl <- genericLeqC sx sx
  = leqReflexive (sFrom @a @() sx)

defaultLeqAntisymmetric :: forall a (x :: a) (y :: a).
                           (SGeneric a, VOrd (Rep a ()), GOrd a)
                        => Sing x -> Sing y
                        -> So (x <= y) -> So (y <= x) -> So (x == y)
defaultLeqAntisymmetric sx sy
  | Refl <- genericLeqC sx sy
  , Refl <- genericLeqC sy sx
  , Refl <- genericEqC  sx sy
  = leqAntisymmetric (sFrom @a @() sx) (sFrom sy)

defaultLeqTotal :: forall a (x :: a) (y :: a).
                   (SGeneric a, VOrd (Rep a ()), GOrd a)
                => Sing x -> Sing y
                -> Either (So (x <= y)) (So (y <= x), So (Not (y == x)))
defaultLeqTotal sx sy
  | Refl <- genericLeqC sx sy
  , Refl <- genericLeqC sy sx
  , Refl <- genericEqC  sy sx
  = leqTotal (sFrom @a @() sx) (sFrom sy)

ordClass :: Class
ordClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod '(<=) 'genericLeq TermLevel
                          ]
  }

pOrdClass :: Class
pOrdClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''(<=) ''GenericLeq (TypeLevel 2)
                          ]
  }

sOrdClass :: Class
sOrdClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod '(%<=) 'sGenericLeq TermLevel
                          ]
  }

vOrdClass :: Class
vOrdClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'leqTransitive    'defaultLeqTransitive    TermLevel
                          , ClassMethod 'leqReflexive     'defaultLeqReflexive     TermLevel
                          , ClassMethod 'leqAntisymmetric 'defaultLeqAntisymmetric TermLevel
                          , ClassMethod 'leqTotal         'defaultLeqTotal         TermLevel
                          ]
  }

deriveOrdAll :: Q TH.Type -> Q [Dec]
deriveOrdAll = deriveClasses [ordClass, pOrdClass, sOrdClass, gClass, vOrdClass]

deriveOrdSingletons :: Q TH.Type -> Q [Dec]
deriveOrdSingletons = deriveClasses [pOrdClass, sOrdClass, gClass, vOrdClass]

deriveOrdVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveOrdVerifiedOnly = deriveClass vOrdClass

class ( GEq a
      , Ord a, POrd a, SOrd a
      , Ord (Rep a ()), POrd (Rep a ()), SOrd (Rep a ()) )
      => GOrd a where
  genericLeqC :: forall (x :: a) (y :: a).
                 Sing x -> Sing y
              -> (x <= y) :~: (x `GenericLeq` y)
  default genericLeqC :: forall (x :: a) (y :: a).
                         (x <= y) ~ (x `GenericLeq` y)
                      => Sing x -> Sing y
                      -> (x <= y) :~: (x `GenericLeq` y)
  genericLeqC _ _ = Refl

instance VOrd (V1 p) where
  leqTransitive _ _ _ _ _ = Oh
  leqReflexive _ = Oh
  leqAntisymmetric _ _ _ _ = Oh
  leqTotal _ _ = Left Oh

instance VOrd (U1 p) where
  leqTransitive _ _ _ _ _ = Oh
  leqReflexive _ = Oh
  leqAntisymmetric _ _ _ _ = Oh
  leqTotal _ _ = Left Oh

instance VOrd c => VOrd (K1 i c p) where
  leqTransitive (SK1 x) (SK1 y) (SK1 z) Oh Oh = leqTransitive x y z Oh Oh
  leqReflexive (SK1 c) = leqReflexive c
  leqAntisymmetric (SK1 x) (SK1 y) Oh Oh = leqAntisymmetric x y Oh Oh
  leqTotal (SK1 x) (SK1 y) = leqTotal x y

instance VOrd (f p) => VOrd (M1 i c f p) where
  leqTransitive (SM1 x) (SM1 y) (SM1 z) Oh Oh = leqTransitive x y z Oh Oh
  leqReflexive (SM1 c) = leqReflexive c
  leqAntisymmetric (SM1 x) (SM1 y) Oh Oh = leqAntisymmetric x y Oh Oh
  leqTotal (SM1 x) (SM1 y) = leqTotal x y

ltTransitive :: forall a (x1 :: a) (x2 :: a) (x3 :: a). VOrd a
             => Sing x1 -> Sing x2 -> Sing x3
             -> So (x1 <= x2) -> So (Not (x1 == x2))
             -> So (x2 <= x3) -> So (Not (x2 == x3))
             -> (So (x1 <= x3), So (Not (x1 == x3)))
ltTransitive x1 x2 x3 x1Leq2 _ x2Leq3 x2Neq3 =
  case leqTotal x3 x1 of
    Left x3Leq1 ->
      let x3Leq2 :: So (x3 <= x2)
          x3Leq2 = leqTransitive x3 x1 x2 x3Leq1 x1Leq2
      in soNotSo (leqAntisymmetric x2 x3 x2Leq3 x3Leq2) x2Neq3
    Right x -> x

instance (VOrd (f p), VOrd (g p)) => VOrd ((f :*: g) p) where
  leqTransitive (x1 :%*: y1) (x2 :%*: y2) (x3 :%*: y3) Oh Oh =
    case x1 %== x2 of
      STrue ->
        case x2 %== x3 of
          STrue |  Oh <-  eqTransitive x1 x2 x3 Oh Oh
                ,  Oh <- leqTransitive y1 y2 y3 Oh Oh
                -> Oh
          SFalse
            |  Refl <- soNotToEq (x1 %== x3) $
                       contra (x1 %== x3) (x2 %== x3)
                              (eqTransitive x2 x1 x3 (eqSymmetric x1 x2 Oh)) Oh
            -> case leqTotal x1 x2 of
                 Left Oh -> leqTransitive x1 x2 x3 Oh Oh
                 Right (_, x2Neq1) -> soNotSo Oh (notEqSymmetric x2 x1 x2Neq1)
      SFalse ->
        case x2 %== x3 of
          STrue
            |  Refl <- soNotToEq (x1 %== x3) $
                       contra (x1 %== x3) (x1 %== x2)
                              (\x1Eq3 -> eqTransitive x1 x3 x2 x1Eq3 (eqSymmetric x2 x3 Oh))
                              Oh
            -> case leqTotal x2 x3 of
                 Left Oh -> leqTransitive x1 x2 x3 Oh Oh
                 Right (_, x3Neq2) -> soNotSo Oh (notEqSymmetric x3 x2 x3Neq2)
          SFalse ->
            case x1 %== x3 of
              STrue
                |  (_, x1Neq3) <- ltTransitive x1 x2 x3 Oh Oh Oh Oh
                -> soNotSo Oh x1Neq3
              SFalse -> leqTransitive x1 x2 x3 Oh Oh

  leqReflexive (x :%*: y)
    | Oh <-  eqReflexive x
    , Oh <- leqReflexive y
    = Oh

  leqAntisymmetric (x1 :%*: y1) (x2 :%*: y2) Oh Oh =
    case x1 %== x2 of
      STrue
        |  Oh <- eqSymmetric x1 x2 Oh
        -> leqAntisymmetric y1 y2 Oh Oh
      SFalse
        |  Refl <- soNotToEq (x2 %== x1) (notEqSymmetric x1 x2 Oh)
        -> soNotSo (leqAntisymmetric x1 x2 Oh Oh) Oh

  leqTotal (x1 :%*: y1) (x2 :%*: y2) =
    case x1 %== x2 of
      STrue ->
        case leqTotal y1 y2 of
          Left Oh -> Left Oh
          Right (Oh, Oh) ->
            case x2 %== x1 of
              STrue -> Right (Oh, Oh)
              SFalse ->
                case leqTotal x2 x1 of
                  Left Oh -> Right (Oh, Oh)
                  Right (_, wat) -> case wat of {}
      SFalse ->
        case leqTotal x1 x2 of
          Left Oh -> Left Oh
          Right (Oh, x2Neq1)
            |  Refl <- soNotToEq (x2 %== x1) x2Neq1
            -> Right (Oh, Oh)

instance (VOrd (f p), VOrd (g p)) => VOrd ((f :+: g) p) where
  leqTransitive x y z Oh Oh =
    case (x, y, z) of
      (SL1 lx, SL1 ly, SL1 lz) -> leqTransitive lx ly lz Oh Oh
      (SR1 rx, SR1 ry, SR1 rz) -> leqTransitive rx ry rz Oh Oh
      (SL1{},  _,      SR1{})  -> Oh

  leqReflexive (SL1 l) = leqReflexive l
  leqReflexive (SR1 r) = leqReflexive r

  leqAntisymmetric x y Oh Oh =
    case (x, y) of
      (SL1 lx, SL1 ly) -> leqAntisymmetric lx ly Oh Oh
      (SR1 rx, SR1 ry) -> leqAntisymmetric rx ry Oh Oh

  leqTotal (SL1 lx) (SL1 ly) = leqTotal lx ly
  leqTotal (SR1 rx) (SR1 ry) = leqTotal rx ry
  leqTotal SL1{} SR1{} = Left Oh
  leqTotal SR1{} SL1{} = Right (Oh, Oh)
