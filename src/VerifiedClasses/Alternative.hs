{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Alternative (
    Alternative(..), PAlternative(..), SAlternative(..), GAlternative(..), VAlternative(..)
  , genericEmpty, GenericEmpty, sGenericEmpty
  , genericAlt, GenericAlt, sGenericAlt
  , defaultAlternativeLeftIdentity, defaultAlternativeRightIdentity
  , defaultAlternativeAssociativity

    -- * Template Haskell
  , deriveAlternativeAll, deriveAlternativeSingletons, deriveAlternativeVerifiedOnly
  ) where

import Control.Applicative
import Control.Applicative.Singletons
import Data.Kind
import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import VerifiedClasses.Applicative
import VerifiedClasses.Generics
import VerifiedClasses.TH.Deriving

genericEmpty :: forall f a. (Generic1 f, Alternative (Rep1 f))
             => f a
genericEmpty = to1 (empty :: Rep1 f a)

infixl 3 `genericAlt`
genericAlt :: forall f a. (Generic1 f, Alternative (Rep1 f))
           => f a -> f a -> f a
x `genericAlt` y = to1 (from1 x <|> from1 y)

type GenericEmpty :: forall (f :: Type -> Type) a. f a
type family GenericEmpty @f @a where
  GenericEmpty @f @a = (To1 Empty :: f a)

infixl 3 `GenericAlt`
type GenericAlt :: forall (f :: Type -> Type) a. f a -> f a -> f a
type family x `GenericAlt` y where
  x `GenericAlt` y = To1 (From1 x <|> From1 y)

sGenericEmpty :: (SGeneric1 f, SAlternative (Rep1 f))
              => Sing (GenericEmpty :: f a)
sGenericEmpty = sTo1 sEmpty

infixl 3 `sGenericAlt`
sGenericAlt :: forall f a (x :: f a) (y :: f a).
               (SGeneric1 f, SAlternative (Rep1 f))
            => Sing x -> Sing y -> Sing (x `GenericAlt` y)
sx `sGenericAlt` sy = sTo1 (sFrom1 sx %<|> sFrom1 sy)

$(singletonsOnly [d|
  instance Alternative U1 where
    empty = U1
    _ <|> _ = U1

  instance Alternative f => Alternative (M1 i c f) where
    empty = M1 empty
    M1 x <|> M1 y = M1 (x <|> y)

  instance (Alternative f, Alternative g) => Alternative (f :*: g) where
    empty = empty :*: empty
    (x1 :*: y1) <|> (x2 :*: y2) = (x1 <|> x2) :*: (y1 <|> y2)

  instance Alternative f => Alternative (Rec1 f) where
    empty = Rec1 empty
    Rec1 x <|> Rec1 y = Rec1 (x <|> y)

  instance (Alternative f, Applicative g) => Alternative ((f :: Type -> Type) :.: g) where
    empty = Comp1 empty
    Comp1 x <|> Comp1 y = Comp1 (x <|> y)
  |])

class (Alternative f, PAlternative f, SAlternative f, VApplicative f) => VAlternative f where
  alternativeLeftIdentity  :: forall a (x :: f a).
                              Sing x -> (Empty <|> x) :~: x

  alternativeRightIdentity :: forall a (x :: f a).
                              Sing x -> (x <|> Empty) :~: x

  alternativeAssociativity :: forall a (x :: f a) (y :: f a) (z :: f a).
                              Sing x -> Sing y -> Sing z
                           -> (x <|> (y <|> z)) :~: ((x <|> y) <|> z)

defaultAlternativeLeftIdentity :: forall f a (x :: f a).
                                  (VGeneric1 f, VAlternative (Rep1 f), GAlternative f)
                               => Sing x -> (Empty <|> x) :~: x
defaultAlternativeLeftIdentity sx
  | Refl <- genericEmptyC @f @a
  , Refl <- genericAltC (sEmpty @f @a) sx
  , Refl <- sFot1 @Type @f (sEmpty @(Rep1 f) @a)
  , Refl <- alternativeLeftIdentity (sFrom1 sx)
  , Refl <- sTof1 sx
  = Refl

defaultAlternativeRightIdentity :: forall f a (x :: f a).
                                   (VGeneric1 f, VAlternative (Rep1 f), GAlternative f)
                                => Sing x -> (x <|> Empty) :~: x
defaultAlternativeRightIdentity sx
  | Refl <- genericEmptyC @f @a
  , Refl <- genericAltC sx (sEmpty @f @a)
  , Refl <- sFot1 @Type @f (sEmpty @(Rep1 f) @a)
  , Refl <- alternativeRightIdentity (sFrom1 sx)
  , Refl <- sTof1 sx
  = Refl

defaultAlternativeAssociativity :: forall f a (x :: f a) (y :: f a) (z :: f a).
                                   (VGeneric1 f, VAlternative (Rep1 f), GAlternative f)
                                => Sing x -> Sing y -> Sing z
                                -> (x <|> (y <|> z)) :~: ((x <|> y) <|> z)
defaultAlternativeAssociativity sx sy sz
  | Refl <- genericAltC sx (sy %<|> sz)
  , Refl <- genericAltC sy sz
  , Refl <- genericAltC sx sy
  , Refl <- genericAltC (sx %<|> sy) sz
  , Refl <- sFot1 @Type @f (sfy %<|> sfz)
  , Refl <- alternativeAssociativity sfx sfy sfz
  , Refl <- sFot1 @Type @f (sfx %<|> sfy)
  = Refl
  where
    sfx :: Sing (From1 x)
    sfx = sFrom1 sx

    sfy :: Sing (From1 y)
    sfy = sFrom1 sy

    sfz :: Sing (From1 z)
    sfz = sFrom1 sz

alternativeClass :: Class
alternativeClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod 'empty 'genericEmpty TermLevel
                          , ClassMethod '(<|>) 'genericAlt   TermLevel
                          ]
  }

pAlternativeClass :: Class
pAlternativeClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''Empty ''GenericEmpty (TypeLevel 0)
                          , ClassMethod ''(<|>) ''GenericAlt   (TypeLevel 2)
                          ]
  }

sAlternativeClass :: Class
sAlternativeClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod 'sEmpty 'sGenericEmpty TermLevel
                          , ClassMethod '(%<|>) 'sGenericAlt   TermLevel
                          ]
  }

vAlternativeClass :: Class
vAlternativeClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'alternativeLeftIdentity  'defaultAlternativeLeftIdentity  TermLevel
                          , ClassMethod 'alternativeRightIdentity 'defaultAlternativeRightIdentity TermLevel
                          , ClassMethod 'alternativeAssociativity 'defaultAlternativeAssociativity TermLevel
                          ]
  }

deriveAlternativeAll :: Q TH.Type -> Q [Dec]
deriveAlternativeAll = deriveClasses [alternativeClass, pAlternativeClass, sAlternativeClass, gClass, vAlternativeClass]

deriveAlternativeSingletons :: Q TH.Type -> Q [Dec]
deriveAlternativeSingletons = deriveClasses [pAlternativeClass, sAlternativeClass, gClass, vAlternativeClass]

deriveAlternativeVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveAlternativeVerifiedOnly = deriveClass vAlternativeClass

class ( GApplicative f
      , Alternative f, PAlternative f, SAlternative f
      , Alternative (Rep1 f), PAlternative (Rep1 f), SAlternative (Rep1 f) )
      => GAlternative f where
  genericEmptyC         :: forall a.
                           (Empty :: f a) :~: GenericEmpty
  default genericEmptyC :: forall a.
                           (Empty :: f a)  ~  GenericEmpty
                        => (Empty :: f a) :~: GenericEmpty
  genericEmptyC = Refl

  genericAltC         :: forall a (x :: f a) (y :: f a).
                         Sing x -> Sing y
                      -> (x <|> y) :~: (x `GenericAlt` y)
  default genericAltC :: forall a (x :: f a) (y :: f a).
                         (x <|> y) ~ (x `GenericAlt` y)
                      => Sing x -> Sing y
                      -> (x <|> y) :~: (x `GenericAlt` y)
  genericAltC _ _ = Refl

instance VAlternative U1 where
  alternativeLeftIdentity SU1 = Refl
  alternativeRightIdentity SU1 = Refl
  alternativeAssociativity _ _ _ = Refl

instance VAlternative f => VAlternative (M1 i c f) where
  alternativeLeftIdentity  (SM1 x) | Refl <- alternativeLeftIdentity  x = Refl
  alternativeRightIdentity (SM1 x) | Refl <- alternativeRightIdentity x = Refl
  alternativeAssociativity (SM1 x) (SM1 y) (SM1 z)
    | Refl <- alternativeAssociativity x y z
    = Refl

instance (VAlternative f, VAlternative g) => VAlternative (f :*: g) where
  alternativeLeftIdentity (x :%*: y)
    | Refl <- alternativeLeftIdentity x
    , Refl <- alternativeLeftIdentity y
    = Refl
  alternativeRightIdentity (x :%*: y)
    | Refl <- alternativeRightIdentity x
    , Refl <- alternativeRightIdentity y
    = Refl
  alternativeAssociativity (x1 :%*: y1) (x2 :%*: y2) (x3 :%*: y3)
    | Refl <- alternativeAssociativity x1 x2 x3
    , Refl <- alternativeAssociativity y1 y2 y3
    = Refl

instance VAlternative f => VAlternative (Rec1 f) where
  alternativeLeftIdentity  (SRec1 x) | Refl <- alternativeLeftIdentity  x = Refl
  alternativeRightIdentity (SRec1 x) | Refl <- alternativeRightIdentity x = Refl
  alternativeAssociativity (SRec1 x) (SRec1 y) (SRec1 z)
    | Refl <- alternativeAssociativity x y z
    = Refl

instance (VAlternative f, VApplicative g) => VAlternative (f :.: g) where
  alternativeLeftIdentity  (SComp1 x) | Refl <- alternativeLeftIdentity  x = Refl
  alternativeRightIdentity (SComp1 x) | Refl <- alternativeRightIdentity x = Refl
  alternativeAssociativity (SComp1 x) (SComp1 y) (SComp1 z)
    | Refl <- alternativeAssociativity x y z
    = Refl
