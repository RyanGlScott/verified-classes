{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Applicative (
    Applicative(..), PApplicative(..), SApplicative(..), GApplicative(..), VApplicative(..)
  , genericPure, GenericPure, sGenericPure
  , genericAp, GenericAp, sGenericAp
  , defaultApplicativeIdentity, defaultApplicativeComposition
  , defaultApplicativeHomomorphism, defaultApplicativeInterchange
  , defaultApplicativeFmap, defaultApplicativeLiftA2

    -- * Template Haskell
  , deriveApplicativeAll, deriveApplicativeSingletons, deriveApplicativeVerifiedOnly

    -- * Miscellaneous utilities
  , fmapPure
  ) where

import Data.Function.Singletons
import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Kind
import Data.Singletons.Base.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import VerifiedClasses.Functor
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.Monoid
import VerifiedClasses.Semigroup
import VerifiedClasses.TH.Deriving

genericPure :: forall f a. (Generic1 f, Applicative (Rep1 f))
            => a -> f a
genericPure x = to1 (pure x)

infixl 4 `genericAp`
genericAp :: forall f a b. (Generic1 f, Applicative (Rep1 f))
          => f (a -> b) -> f a -> f b
g `genericAp` x = to1 (from1 g <*> from1 x)

type GenericPure :: a -> f a
type family GenericPure x where
  GenericPure x = To1 (Pure x)

infixl 4 `GenericAp`
type GenericAp :: f (a ~> b) -> f a -> f b
type family g `GenericAp` x where
  g `GenericAp` x = To1 (From1 g <*> From1 x)

sGenericPure :: forall f a (x :: a).
                (SGeneric1 f, SApplicative (Rep1 f))
             => Sing x -> Sing (GenericPure x :: f a)
sGenericPure = sTo1 . sPure

infixl 4 `sGenericAp`
sGenericAp :: forall f a b (g :: f (a ~> b)) (x :: f a).
              (SGeneric1 f, SApplicative (Rep1 f))
           => Sing g -> Sing x -> Sing (g `GenericAp` x)
sg `sGenericAp` sx = sTo1 (sFrom1 sg %<*> sFrom1 sx)

$(singletonsOnly [d|
  instance Applicative U1 where
    pure _ = U1
    _ <*> _ = U1

  instance Monoid c => Applicative (K1 i c) where
    pure _ = K1 mempty
    K1 x <*> K1 y = K1 (x <> y)

  instance Applicative f => Applicative (M1 i c f) where
    -- #15703
    -- pure = M1 . pure
    pure x = M1 (pure x)
    M1 f <*> M1 x = M1 (f <*> x)

  instance (Applicative f, Applicative g) => Applicative (f :*: g) where
    pure a = pure a :*: pure a
    (f :*: g) <*> (x :*: y) = (f <*> x) :*: (g <*> y)

  instance Applicative Par1 where
    pure = Par1
    Par1 f <*> Par1 x = Par1 (f x)

  instance Applicative f => Applicative (Rec1 f) where
    -- #15703
    -- pure = Rec1 . pure
    pure x = Rec1 (pure x)
    Rec1 f <*> Rec1 x = Rec1 (f <*> x)

  instance (Applicative f, Applicative g) => Applicative ((f :: Type -> Type) :.: g) where
    -- #15703
    -- pure = Comp1 . pure . pure
    pure x = Comp1 (pure (pure x))
    Comp1 f <*> Comp1 x = Comp1 (liftA2 (<*>) f x)
  |])


class (Applicative f, PApplicative f, SApplicative f, VFunctor f) => VApplicative f where
  applicativeIdentity :: forall a (v :: f a).
                         Sing v -> (Pure IdSym0 <*> v) :~: v

  applicativeComposition :: forall a b c (u :: f (b ~> c)) (v :: f (a ~> b)) (w :: f a).
                            Sing u -> Sing v -> Sing w
                         -> (Pure (.@#@$) <*> u <*> v <*> w) :~: (u <*> (v <*> w))

  applicativeHomomorphism :: forall a b (g :: a ~> b) (x :: a).
                             Sing g -> Sing x
                          -> (Pure g <*> Pure x) :~: (Pure (g `Apply` x) :: f b)

  applicativeInterchange :: forall a b (u :: f (a ~> b)) (y :: a).
                            Sing u -> Sing y
                         -> (u <*> Pure y) :~: (Pure ((&@#@$$) y) <*> u)

  applicativeFmap :: forall a b (g :: a ~> b) (x :: f a).
                     Sing g -> Sing x
                  -> Fmap g x :~: (Pure g <*> x)

  applicativeLiftA2 :: forall a b c (g :: a ~> b ~> c) (x :: f a) (y :: f b).
                       Sing g -> Sing x -> Sing y
                    -> LiftA2 g x y :~: (Fmap g x <*> y)

defaultApplicativeIdentity :: forall f a (v :: f a).
                              (VGeneric1 f, VApplicative (Rep1 f), GApplicative f)
                           => Sing v -> (Pure IdSym0 <*> v) :~: v
defaultApplicativeIdentity sv
  | Refl <- genericPureC @f sIdFun
  , Refl <- genericApC (sPure sIdFun) sv
  , Refl <- sFot1 @Type @f (sPure sIdFun)
  , Refl <- applicativeIdentity (sFrom1 sv)
  , Refl <- sTof1 sv
  = Refl
  where
    sIdFun :: Sing (IdSym0 :: a ~> a)
    sIdFun = sing

defaultApplicativeComposition :: forall f a b c (u :: f (b ~> c)) (v :: f (a ~> b)) (w :: f a).
                                 (VGeneric1 f, VApplicative (Rep1 f), GApplicative f)
                              => Sing u -> Sing v -> Sing w
                              -> (Pure (.@#@$) <*> u <*> v <*> w) :~: (u <*> (v <*> w))
defaultApplicativeComposition su sv sw
  | Refl <- genericPureC @f comp
  , Refl <- genericApC pureComp su
  , Refl <- genericApC pureCompApU sv
  , Refl <- genericApC (pureCompApU %<*> sv) sw
  , Refl <- genericApC su (sv %<*> sw)
  , Refl <- genericApC sv sw
  , Refl <- sFot1 @Type @f pureRepComp
  , Refl <- sFot1 @Type @f pureRepCompApFu
  , Refl <- sFot1 @Type @f (pureRepCompApFu %<*> fv)
  , Refl <- applicativeComposition fu fv fw
  , Refl <- sFot1 @Type @f (fv %<*> fw)
  = Refl
  where
    fu :: Sing (From1 u :: Rep1 f (b ~> c))
    fu = sFrom1 su

    fv :: Sing (From1 v :: Rep1 f (a ~> b))
    fv = sFrom1 sv

    fw :: Sing (From1 w :: Rep1 f a)
    fw = sFrom1 sw

    comp :: Sing ((.@#@$) :: (b ~> c) ~> (a ~> b) ~> a ~> c)
    comp = singFun3 (%.)

    pureComp :: Sing (Pure (.@#@$) :: f ((b ~> c) ~> (a ~> b) ~> a ~> c))
    pureComp = sPure comp

    pureCompApU :: Sing (Pure (.@#@$) <*> u :: f ((a ~> b) ~> a ~> c))
    pureCompApU = pureComp %<*> su

    pureRepComp :: Sing (Pure (.@#@$) :: Rep1 f ((b ~> c) ~> (a ~> b) ~> a ~> c))
    pureRepComp = sPure comp

    pureRepCompApFu :: Sing (Pure (.@#@$) <*> From1 u :: Rep1 f ((a ~> b) ~> a ~> c))
    pureRepCompApFu = pureRepComp %<*> fu

defaultApplicativeHomomorphism :: forall f a b (g :: a ~> b) (x :: a).
                                  (VGeneric1 f, VApplicative (Rep1 f), GApplicative f)
                               => Sing g -> Sing x
                               -> (Pure g <*> Pure x) :~: (Pure (g `Apply` x) :: f b)
defaultApplicativeHomomorphism sg sx
  | Refl <- genericPureC @f sg
  , Refl <- genericPureC @f sx
  , Refl <- genericPureC @f (sg @@ sx)
  , Refl <- genericApC (sPure sg) (sPure @f sx)
  , Refl <- sFot1 @Type @f (sPure sg)
  , Refl <- sFot1 @Type @f (sPure sx)
  , Refl <- applicativeHomomorphism @(Rep1 f) sg sx
  , Refl <- sFot1 @Type @f pureGX
  , Refl <- sTof1 @Type @f (sTo1 pureGX)
  = Refl
  where
    pureGX :: Sing (Pure (Apply g x) :: Rep1 f b)
    pureGX = sPure (sg @@ sx)

defaultApplicativeInterchange :: forall f a b (u :: f (a ~> b)) (y :: a).
                                 (VGeneric1 f, VApplicative (Rep1 f), GApplicative f)
                              => Sing u -> Sing y
                              -> (u <*> Pure y) :~: (Pure ((&@#@$$) y) <*> u)
defaultApplicativeInterchange su sy
  | Refl <- genericPureC @f sy
  , Refl <- genericPureC @f sAmpY
  , Refl <- genericApC su (sPure sy)
  , Refl <- genericApC (sPure sAmpY) su
  , Refl <- sFot1 @Type @f (sPure sy)
  , Refl <- applicativeInterchange (sFrom1 su) sy
  , Refl <- sFot1 @Type @f (sPure sAmpY)
  = Refl
  where
    sAmpY :: Sing (((&@#@$$) y) :: (a ~> b) ~> b)
    sAmpY = singFun1 @((&@#@$$) y) (sy %&)

defaultApplicativeFmap :: forall f a b (g :: a ~> b) (x :: f a).
                          (VGeneric1 f, VApplicative (Rep1 f), GApplicative f)
                       => Sing g -> Sing x
                       -> Fmap g x :~: (Pure g <*> x)
defaultApplicativeFmap sg sx
  | Refl <- genericFmapC sg sx
  , Refl <- genericPureC @f sg
  , Refl <- genericApC (sPure sg) sx
  , Refl <- applicativeFmap sg (sFrom1 sx)
  , Refl <- sFot1 @Type @f (sPure sg)
  = Refl

defaultApplicativeLiftA2 :: forall f a b c (g :: a ~> b ~> c) (x :: f a) (y :: f b).
                            GApplicative f
                         => Sing g -> Sing x -> Sing y
                         -> LiftA2 g x y :~: (Fmap g x <*> y)
defaultApplicativeLiftA2 = liftA2FmapApC

applicativeClass :: Class
applicativeClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod 'pure  'genericPure TermLevel
                          , ClassMethod '(<*>) 'genericAp   TermLevel
                          ]
  }

pApplicativeClass :: Class
pApplicativeClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''Pure  ''GenericPure (TypeLevel 1)
                          , ClassMethod ''(<*>) ''GenericAp   (TypeLevel 2)
                          ]
  }

sApplicativeClass :: Class
sApplicativeClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod 'sPure  'sGenericPure TermLevel
                          , ClassMethod '(%<*>) 'sGenericAp   TermLevel
                          ]
  }

vApplicativeClass :: Class
vApplicativeClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'applicativeIdentity     'defaultApplicativeIdentity     TermLevel
                          , ClassMethod 'applicativeComposition  'defaultApplicativeComposition  TermLevel
                          , ClassMethod 'applicativeHomomorphism 'defaultApplicativeHomomorphism TermLevel
                          , ClassMethod 'applicativeInterchange  'defaultApplicativeInterchange  TermLevel
                          , ClassMethod 'applicativeFmap         'defaultApplicativeFmap         TermLevel
                          , ClassMethod 'applicativeLiftA2       'defaultApplicativeLiftA2       TermLevel
                          ]
  }

deriveApplicativeAll :: Q TH.Type -> Q [Dec]
deriveApplicativeAll = deriveClasses [applicativeClass, pApplicativeClass, sApplicativeClass, gClass, vApplicativeClass]

deriveApplicativeSingletons :: Q TH.Type -> Q [Dec]
deriveApplicativeSingletons = deriveClasses [pApplicativeClass, sApplicativeClass, gClass, vApplicativeClass]

deriveApplicativeVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveApplicativeVerifiedOnly = deriveClass vApplicativeClass

class ( GFunctor f
      , Applicative f, PApplicative f, SApplicative f
      , Applicative (Rep1 f), PApplicative (Rep1 f), SApplicative (Rep1 f) )
      => GApplicative f where
  genericPureC         :: forall a (x :: a).
                          Sing x -> (Pure x :: f a) :~: GenericPure x
  default genericPureC :: forall a (x :: a).
                          (Pure x :: f a) ~ GenericPure x
                       => Sing x -> (Pure x :: f a) :~: GenericPure x
  genericPureC _ = Refl

  genericApC         :: forall a b (g :: f (a ~> b)) (x :: f a).
                        Sing g -> Sing x
                     -> (g <*> x) :~: (g `GenericAp` x)
  default genericApC :: forall a b (g :: f (a ~> b)) (x :: f a).
                        (g <*> x) ~ (g `GenericAp` x)
                     => Sing g -> Sing x
                     -> (g <*> x) :~: (g `GenericAp` x)
  genericApC _ _ = Refl

  liftA2FmapApC         :: forall a b c (g :: a ~> b ~> c) (x :: f a) (y :: f b).
                           Sing g -> Sing x -> Sing y
                        -> LiftA2 g x y :~: (Fmap g x <*> y)
  default liftA2FmapApC :: forall a b c (g :: a ~> b ~> c) (x :: f a) (y :: f b).
                           LiftA2 g x y ~ (Fmap g x <*> y)
                        => Sing g -> Sing x -> Sing y
                        -> LiftA2 g x y :~: (Fmap g x <*> y)
  liftA2FmapApC _ _ _ = Refl

fmapPure :: forall f a b (g :: a ~> b) (x :: a).
            VApplicative f
         => Sing g -> Sing x
         -> Fmap g (Pure x :: f a) :~: Pure (g @@ x)
fmapPure sg sx
  | Refl <- applicativeFmap @f sg (sPure sx)
  , Refl <- applicativeHomomorphism @f sg sx
  = Refl

instance VApplicative U1 where
  applicativeIdentity SU1 = Refl
  applicativeComposition _ _ _ = Refl
  applicativeHomomorphism _ _ = Refl
  applicativeInterchange _ _ = Refl
  applicativeFmap _ _ = Refl
  applicativeLiftA2 _ _ _ = Refl

instance VMonoid c => VApplicative (K1 i c) where
  applicativeIdentity (SK1 c) | Refl <- monoidLeftIdentity c = Refl
  applicativeComposition (SK1 c1) (SK1 c2) (SK1 c3)
    | Refl <- monoidLeftIdentity c1
    , Refl <- semigroupAssociative c1 c2 c3
    = Refl
  applicativeHomomorphism _ _ | Refl <- monoidLeftIdentity (sMempty @c) = Refl
  applicativeInterchange (SK1 c) _
    | Refl <- monoidLeftIdentity c
    , Refl <- monoidRightIdentity c
    = Refl
  applicativeFmap _ (SK1 c) | Refl <- monoidLeftIdentity c = Refl
  applicativeLiftA2 _ _ _ = Refl

instance VApplicative f => VApplicative (M1 i c f) where
  applicativeIdentity (SM1 x)
    | Refl <- applicativeIdentity x
    = Refl
  applicativeComposition (SM1 x) (SM1 y) (SM1 z)
    | Refl <- applicativeComposition x y z
    = Refl
  applicativeHomomorphism sg sx
    | Refl <- applicativeHomomorphism @f sg sx
    = Refl
  applicativeInterchange (SM1 x) sy
    | Refl <- applicativeInterchange x sy
    = Refl
  applicativeFmap sg (SM1 x)
    | Refl <- applicativeFmap sg x
    = Refl
  applicativeLiftA2 _ _ _ = Refl

instance (VApplicative f, VApplicative g) => VApplicative (f :*: g) where
  applicativeIdentity (x :%*: y)
    | Refl <- applicativeIdentity x
    , Refl <- applicativeIdentity y
    = Refl
  applicativeComposition (x1 :%*: y1) (x2 :%*: y2) (x3 :%*: y3)
    | Refl <- applicativeComposition x1 x2 x3
    , Refl <- applicativeComposition y1 y2 y3
    = Refl
  applicativeHomomorphism sg sx
    | Refl <- applicativeHomomorphism @f sg sx
    , Refl <- applicativeHomomorphism @g sg sx
    = Refl
  applicativeInterchange (f :%*: g) sy
    | Refl <- applicativeInterchange f sy
    , Refl <- applicativeInterchange g sy
    = Refl
  applicativeFmap sg (x :%*: y)
    | Refl <- applicativeFmap sg x
    , Refl <- applicativeFmap sg y
    = Refl
  applicativeLiftA2 _ _ _ = Refl

instance VApplicative Par1 where
  applicativeIdentity SPar1{} = Refl
  applicativeComposition SPar1{} SPar1{} SPar1{} = Refl
  applicativeHomomorphism _ _ = Refl
  applicativeInterchange SPar1{} _ = Refl
  applicativeFmap _ SPar1{} = Refl
  applicativeLiftA2 _ _ _ = Refl

instance VApplicative f => VApplicative (Rec1 f) where
  applicativeIdentity (SRec1 x)
    | Refl <- applicativeIdentity x
    = Refl
  applicativeComposition (SRec1 x) (SRec1 y) (SRec1 z)
    | Refl <- applicativeComposition x y z
    = Refl
  applicativeHomomorphism sg sx
    | Refl <- applicativeHomomorphism @f sg sx
    = Refl
  applicativeInterchange (SRec1 x) sy
    | Refl <- applicativeInterchange x sy
    = Refl
  applicativeFmap sg (SRec1 x)
    | Refl <- applicativeFmap sg x
    = Refl
  applicativeLiftA2 _ _ _ = Refl

instance (VApplicative f, VApplicative g) => VApplicative (f :.: g) where
  applicativeIdentity :: forall p (v :: (f :.: g) p).
                         Sing v -> (Pure IdSym0 <*> v) :~: v
  applicativeIdentity (SComp1 (x :: Sing fgp))
    | Refl <- applicativeFmap @f gap (sPure gpure)
    , Refl <- applicativeHomomorphism @f gap gpure
    , -- (Pure ((<*>@#@$$) (Pure IdSym0)) <*> fgp) :~: (Pure IdSym0 <*> fgp)
      Refl <- Refl @(<*>@#@$)
                `apply` (Refl @PureSym0
                          `apply` funExt @(g p) @(g p)
                                         @((<*>@#@$$) (Pure IdSym0))
                                         @IdSym0
                                         applicativeIdentity)
                `apply` Refl @fgp
    , Refl <- applicativeIdentity @f x
    , Refl <- applicativeLiftA2 @f gap (sPure gpure) x
    = Refl
    where
      gap :: Sing ((<*>@#@$) :: g (p ~> p) ~> g p ~> g p)
      gap = singFun2 @(<*>@#@$) (%<*>)

      gpure :: Sing (Pure IdSym0 :: g (p ~> p))
      gpure = sPure (sing @IdSym0)

  applicativeComposition :: forall p q r (u :: (f :.: g) (q ~> r)) (v :: (f :.: g) (p ~> q)) (w :: (f :.: g) p).
                            Sing u -> Sing v -> Sing w
                         -> (Pure (.@#@$) <*> u <*> v <*> w) :~: (u <*> (v <*> w))
  applicativeComposition (SComp1 (x1 :: Sing x1)) (SComp1 (x2 :: Sing x2)) (SComp1 (x3 :: Sing x3))
{-
(1)  fmap (<*>) (fmap (<*>) (fmap (<*>) (pure (pure (.))) <*> x1) <*> x2) <*> x3                 [apFmap (3)]
(2)  (pure (<*>) <*> ((pure (<*>) <*> ((pure (<*>) <*> pure (pure (.))) <*> x1)) <*> x2)) <*> x3 [apHom]
(3)  (pure (<*>) <*> ((pure (<*>) <*> (pure ((<*>) (pure (.))) <*> x1)) <*> x2)) <*> x3          [apComp]
(4)  (pure (<*>) <*> (pure (.) <*> pure (<*>) <*> pure ((<*>) (pure (.))) <*> x1 <*> x2)) <*> x3 [apHom (2)]
(5)  (pure (<*>) <*> (pure ((.) (<*>) ((<*>) (pure (.)))) <*> x1 <*> x2)) <*> x3                 [apComp]
(6)  pure (.) <*> pure (<*>) <*> (pure ((.) (<*>) ((<*>) (pure (.)))) <*> x1) <*> x2 <*> x3      [apHom]
(7)  pure ((.) (<*>)) <*> (pure ((.) (<*>) ((<*>) (pure (.)))) <*> x1) <*> x2 <*> x3             [apComp]
(8)  pure (.) <*> pure ((.) (<*>)) <*> pure ((.) (<*>) ((<*>) (pure (.)))) <*> x1 <*> x2 <*> x3  [apHom (2)]
(9)  pure ((.) ((.) (<*>)) ((.) (<*>) ((<*>) (pure (.))))) <*> x1 <*> x2 <*> x3                  [funExt (3), apComp]
(10) pure ((.) ((<*>) &) ((.) (.) ((.) (.) (<*>)))) <*> x1 <*> x2 <*> x3                         [apHom (2)]
(11) pure (.) <*> pure ((<*>) &) <*> pure ((.) (.) ((.) (.) (<*>))) <*> x1 <*> x2 <*> x3         [apComp]
(12) pure ((<*>) &) <*> (pure ((.) (.) ((.) (.) (<*>))) <*> x1) <*> x2 <*> x3                    [apInt]
(13) pure ((.) (.) ((.) (.) (<*>))) <*> x1 <*> pure (<*>) <*> x2 <*> x3                          [apHom (2)]
(14) pure (.) <*> pure (.) <*> pure ((.) (.) (<*>)) <*> x1 <*> pure (<*>) <*> x2 <*> x3          [apComp]
(15) pure (.) <*> (pure ((.) (.) (<*>)) <*> x1) <*> pure (<*>) <*> x2 <*> x3                     [apComp]
(16) pure ((.) (.) (<*>)) <*> x1 <*> (pure (<*>) <*> x2) <*> x3                                  [apHom (2)]
(17) (pure (.) <*> pure (.) <*> pure (<*>) <*> x1) <*> (pure (<*>) <*> x2) <*> x3                [apComp]
(18) pure (.) <*> (pure (<*>) <*> x1) <*> (pure (<*>) <*> x2) <*> x3                             [apFmap (2)]
(19) pure (.) <*> fmap (<*>) x1 <*> fmap (<*>) x2 <*> x3                                         [apComp]
(20) fmap (<*>) x1 <*> (fmap (<*>) x2 <*> x3)
-}
    | -- (1)
      Refl <- applicativeFmap @f gap1 (sFmap gap2 fmapGap3FpureGpureComp1ApX1 %<*> x2)
    , Refl <- applicativeFmap @f gap2 fmapGap3FpureGpureComp1ApX1
    , Refl <- applicativeFmap @f gap3 fpureGpureComp1
      -- (2)
    , Refl <- applicativeHomomorphism @f gap3 gpureComp1
      -- (3)
    , Refl <- applicativeComposition @f (sPure @f gap2) (sPure @f (gap3 @@ gpureComp1)) x1
      -- (4)
    , Refl <- applicativeHomomorphism @f comp2 gap2
    , Refl <- applicativeHomomorphism @f comp2Gap2 gap3PureComp
      -- (5)
    , Refl <- applicativeComposition @f (sPure gap1) (pureComp2Gap2Gap3PureComp %<*> x1) x2
      -- (6)
    , Refl <- applicativeHomomorphism @f comp3 gap1
      -- (7)
    , Refl <- applicativeComposition @f (sPure comp3Gap1) pureComp2Gap2Gap3PureComp x1
      -- (8)
    , Refl <- applicativeHomomorphism @f comp4 comp3Gap1
    , Refl <- applicativeHomomorphism @f (comp4 @@ comp3Gap1) comp2Gap2Gap3PureComp
      -- (9)
    , Refl <- --     (Pure ((.@#@$$$) ((.@#@$$) (<*>@#@$)) ((.@#@$$$) (<*>@#@$) ((<*>@#@$$) (Pure (.@#@$))))) <*> x1 <*> x2 <*> x3)
              -- :~: (Pure ((.@#@$$$) ((&@#@$$) (<*>@#@$)) ((.@#@$$$) (.@#@$) ((.@#@$$$) (.@#@$) (<*>@#@$)))) <*> x1 <*> x2 <*> x3)
              Refl @(<*>@#@$)
                `apply` (Refl @(<*>@#@$)
                          `apply` (Refl @(<*>@#@$)
                                    `apply` (Refl @PureSym0
                                              `apply` (funExt3 @(g (q ~> r)) @(g (p ~> q)) @(g p) @(g r)
                                                               @((.@#@$$$) ((.@#@$$) (<*>@#@$)) ((.@#@$$$) (<*>@#@$) ((<*>@#@$$) (Pure (.@#@$)))))
                                                               @((.@#@$$$) ((&@#@$$) (<*>@#@$)) ((.@#@$$$) (.@#@$) ((.@#@$$$) (.@#@$) (<*>@#@$))))
                                                               lemma))
                                    `apply` Refl @x1)
                          `apply` Refl @x2)
                `apply` Refl @x3
      -- (10)
    , Refl <- applicativeHomomorphism @f (comp5 @@ ampGap4) caterpillar
    , Refl <- applicativeHomomorphism @f comp5 ampGap4
      -- (11)
    , Refl <- applicativeComposition @f (sPure ampGap4) (pureCaterpillar) x1
      -- (12)
    , Refl <- applicativeInterchange @f (pureCaterpillar %<*> x1) gap4
      -- (13)
    , Refl <- applicativeHomomorphism @f caterpillarHead caterpillarTail
    , Refl <- applicativeHomomorphism @f comp6 comp7
      -- (14)
    , Refl <- applicativeComposition @f (sPure comp7) (sPure caterpillarTail) x1
      -- (15)
    , Refl <- applicativeComposition @f (sPure caterpillarTail %<*> x1) (sPure gap4) x2
      -- (16)
    , Refl <- applicativeHomomorphism @f (comp8 @@ comp9) gap5
    , Refl <- applicativeHomomorphism @f comp8 comp9
      -- (17)
    , Refl <- applicativeComposition @f (sPure comp9) (sPure gap5) x1
      -- (18)
    , Refl <- applicativeFmap @f gap5 x1
    , Refl <- applicativeFmap @f gap4 x2
      -- (19)
    , Refl <- applicativeComposition @f (sFmap gap5 x1) (sFmap gap4 x2) x3

    , Refl <- applicativeLiftA2 @f gap1 (sFmap gap2 fmapGap3FpureGpureComp1ApX1 %<*> x2) x3
    , Refl <- applicativeLiftA2 @f gap2 fmapGap3FpureGpureComp1ApX1 x2
    , Refl <- applicativeLiftA2 @f gap3 fpureGpureComp1 x1
    , Refl <- applicativeLiftA2 @f gap4 x2 x3
    , Refl <- applicativeLiftA2 @f gap5 x1 (sPure gap4 %<*> x2 %<*> x3)
    = Refl
    where
      gap1 :: Sing ((<*>@#@$) :: g (p ~> r) ~> g p ~> g r)
      gap1 = singFun2 @(<*>@#@$) (%<*>)

      gap2 :: Sing ((<*>@#@$) :: g ((p ~> q) ~> p ~> r) ~> g (p ~> q) ~> g (p ~> r))
      gap2 = singFun2 @(<*>@#@$) (%<*>)

      gap3 :: Sing ((<*>@#@$) :: g ((q ~> r) ~> (p ~> q) ~> p ~> r) ~> g (q ~> r) ~> g ((p ~> q) ~> p ~> r))
      gap3 = singFun2 @(<*>@#@$) (%<*>)

      gap4 :: Sing ((<*>@#@$) :: g (p ~> q) ~> g p ~> g q)
      gap4 = singFun2 @(<*>@#@$) (%<*>)

      gap5 :: Sing ((<*>@#@$) :: g (q ~> r) ~> g q ~> g r)
      gap5 = singFun2 @(<*>@#@$) (%<*>)

      comp1 :: Sing ((.@#@$) :: (q ~> r) ~> (p ~> q) ~> p ~> r)
      comp1 = singFun3 @(.@#@$) (%.)

      comp2 :: Sing ((.@#@$) :: (g ((p ~> q) ~> p ~> r) ~> g (p ~> q) ~> g (p ~> r)) ~> (g (q ~> r) ~> g ((p ~> q) ~> p ~> r)) ~> (g (q ~> r) ~> g (p ~> q) ~> g (p ~> r)))
      comp2 = singFun3 @(.@#@$) (%.)

      comp3 :: Sing ((.@#@$) :: (g (p ~> r) ~> g p ~> g r) ~> (g (p ~> q) ~> g (p ~> r)) ~> g (p ~> q) ~> g p ~> g r)
      comp3 = singFun3 @(.@#@$) (%.)

      comp4 :: Sing ((.@#@$) :: ((g (p ~> q) ~> g (p ~> r)) ~> g (p ~> q) ~> g p ~> g r) ~> (g (q ~> r) ~> g (p ~> q) ~> g (p ~> r)) ~> g (q ~> r) ~> g (p ~> q) ~> g p ~> g r)
      comp4 = singFun3 @(.@#@$) (%.)

      comp5 :: Sing ((.@#@$) :: (((g (p ~> q) ~> g p ~> g q) ~> g (p ~> q) ~> g p ~> g r) ~> g (p ~> q) ~> g p ~> g r)
                             ~> (g (q ~> r) ~> (g (p ~> q) ~> g p ~> g q) ~> g (p ~> q) ~> g p ~> g r) ~> g (q ~> r) ~> g (p ~> q) ~> g p ~> g r)
      comp5 = singFun3 @(.@#@$) (%.)

      comp6 :: Sing ((.@#@$) :: (((g p ~> g q) ~> g p ~> g r) ~> (g (p ~> q) ~> (g p ~> g q)) ~> g (p ~> q) ~> g p ~> g r)
                             ~> (g (q ~> r) ~> (g p ~> g q) ~> g p ~> g r) ~> g (q ~> r) ~> (g (p ~> q) ~> (g p ~> g q)) ~> g (p ~> q) ~> g p ~> g r)
      comp6 = singFun3 @(.@#@$) (%.)

      comp7 :: Sing ((.@#@$) :: ((g p ~> g q) ~> g p ~> g r) ~> (g (p ~> q) ~> g p ~> g q) ~> g (p ~> q) ~> g p ~> g r)
      comp7 = singFun3 @(.@#@$) (%.)

      comp8 :: Sing ((.@#@$) :: ((g q ~> g r) ~> (g p ~> g q) ~> g p ~> g r) ~> (g (q ~> r) ~> g q ~> g r) ~> g (q ~> r) ~> (g p ~> g q) ~> g p ~> g r)
      comp8 = singFun3 @(.@#@$) (%.)

      comp9 :: Sing ((.@#@$) :: (g q ~> g r) ~> (g p ~> g q) ~> g p ~> g r)
      comp9 = singFun3 @(.@#@$) (%.)

      comp3Gap1 :: Sing ((.@#@$$) (<*>@#@$) :: (g (p ~> q) ~> g (p ~> r)) ~> g (p ~> q) ~> g p ~> g r)
      comp3Gap1 = comp3 @@ gap1

      comp2Gap2 :: Sing ((.@#@$$) (<*>@#@$) :: (g (q ~> r) ~> g ((p ~> q) ~> p ~> r)) ~> (g (q ~> r) ~> g (p ~> q) ~> g (p ~> r)))
      comp2Gap2 = comp2 @@ gap2

      gpureComp1 :: Sing (Pure (.@#@$) :: g ((q ~> r) ~> (p ~> q) ~> p ~> r))
      gpureComp1 = sPure comp1

      fpureGpureComp1 :: Sing (Pure (Pure (.@#@$)) :: f (g ((q ~> r) ~> (p ~> q) ~> p ~> r)))
      fpureGpureComp1 = sPure gpureComp1

      fmapGap3FpureGpureComp1ApX1 :: Sing (Fmap (<*>@#@$) (Pure (Pure (.@#@$))) <*> x1 :: f (g ((p ~> q) ~> p ~> r)))
      fmapGap3FpureGpureComp1ApX1 = sFmap gap3 fpureGpureComp1 %<*> x1

      gap3PureComp :: Sing ((<*>@#@$$) (Pure (.@#@$)) :: g (q ~> r) ~> g ((p ~> q) ~> p ~> r))
      gap3PureComp = gap3 @@ sPure (singFun3 @(.@#@$) (%.))

      comp2Gap2Gap3PureComp :: Sing ((.@#@$$$) (<*>@#@$) ((<*>@#@$$) (Pure (.@#@$))) :: g (q ~> r) ~> g (p ~> q) ~> g (p ~> r))
      comp2Gap2Gap3PureComp = comp2Gap2 @@ gap3PureComp

      pureComp2Gap2Gap3PureComp :: Sing (Pure ((.@#@$$$) (<*>@#@$) ((<*>@#@$$) (Pure (.@#@$)))) :: f (g (q ~> r) ~> g (p ~> q) ~> g (p ~> r)))
      pureComp2Gap2Gap3PureComp = sPure comp2Gap2Gap3PureComp

      -- The main induction hypothesis.
      lemma :: forall (y1 :: g (q ~> r)) (y2 :: g (p ~> q)) (y3 :: g p).
               Sing y1 -> Sing y2 -> Sing y3
            -> (Pure (.@#@$) <*> y1 <*> y2 <*> y3) :~: (y1 <*> (y2 <*> y3))
      lemma = applicativeComposition @g

      ampGap4 :: Sing ((&@#@$$) (<*>@#@$) :: ((g (p ~> q) ~> g p ~> g q) ~> g (p ~> q) ~> g p ~> g r) ~> g (p ~> q) ~> g p ~> g r)
      ampGap4 = singFun1 (gap4 %&)

      caterpillarHead :: Sing ((.@#@$$) (.@#@$) :: (g (q ~> r) ~> ((g p ~> g q) ~> (g p ~> g r))) ~> g (q ~> r) ~> (g (p ~> q) ~> (g p ~> g q)) ~> g (p ~> q) ~> (g p ~> g r))
      caterpillarHead = comp6 @@ comp7

      caterpillarTail :: Sing ((.@#@$$$) (.@#@$) (<*>@#@$) :: g (q ~> r) ~> (g p ~> g q) ~> g p ~> g r)
      caterpillarTail = comp8 @@ comp9 @@ gap5

      caterpillar :: Sing ((.@#@$$$) (.@#@$) ((.@#@$$$) (.@#@$) (<*>@#@$)) :: g (q ~> r) ~> (g (p ~> q) ~> (g p ~> g q)) ~> g (p ~> q) ~> (g p ~> g r))
      caterpillar = caterpillarHead @@ caterpillarTail

      pureCaterpillar :: Sing (Pure ((.@#@$$$) (.@#@$) ((.@#@$$$) (.@#@$) (<*>@#@$))) :: f (g (q ~> r) ~> (g (p ~> q) ~> (g p ~> g q)) ~> g (p ~> q) ~> (g p ~> g r)))
      pureCaterpillar = sPure caterpillar

  applicativeHomomorphism :: forall p q (h :: p ~> q) (x :: p).
                             Sing h -> Sing x
                          -> (Pure h <*> Pure x) :~: (Pure (h `Apply` x) :: (f :.: g) q)
  applicativeHomomorphism sh sx
    | Refl <- applicativeFmap @f gap (sPure gpure)
    , Refl <- applicativeHomomorphism @f gap gpure
    , Refl <- applicativeHomomorphism @f (singFun1 @((<*>@#@$$) (Pure h)) (gpure %<*>))
                                         (sPure @g sx)
    , Refl <- applicativeHomomorphism @g sh sx
    , Refl <- applicativeLiftA2 @f gap (sPure gpure) (sPure (sPure sx))
    = Refl
    where
      gap :: Sing ((<*>@#@$) :: g (p ~> q) ~> g p ~> g q)
      gap = singFun2 @(<*>@#@$) (%<*>)

      gpure :: Sing (Pure h :: g (p ~> q))
      gpure = sPure sh

  applicativeInterchange :: forall p q (u :: (f :.: g) (p ~> q)) (y :: p).
                            Sing u -> Sing y
                         -> (u <*> Pure y) :~: (Pure ((&@#@$$) y) <*> u)
  applicativeInterchange (SComp1 (x :: Sing fgp)) sy
    | Refl <- applicativeFmap @f gap1 x
    , Refl <- applicativeInterchange @f (fpureGap1 %<*> x) gpureY
    , Refl <- applicativeComposition @f (sPure ampGpureY) fpureGap1 x
    , Refl <- applicativeHomomorphism @f @_ @((g (p ~> q) ~> _) ~> _)
                                      (singFun3 @(.@#@$) (%.)) ampGpureY
    , Refl <- applicativeHomomorphism @f (singFun2 @((.@#@$$) ((&@#@$$) (Pure y))) (ampGpureY %.)) gap1
    {-
        (Pure ((&@#@$$) (Pure y) .@#@$$$ (<*>@#@$)) <*> fgp)
    :~: (Pure ((<*>@#@$$) (Pure ((&@#@$$) y))) <*> fgp))
    -}
    , Refl <- Refl @(<*>@#@$)
                `apply` (Refl @PureSym0
                          `apply` funExt @(g (p ~> q)) @(g q)
                                         @((&@#@$$) (Pure y) .@#@$$$ (<*>@#@$))
                                         @((<*>@#@$$) (Pure ((&@#@$$) y)))
                                         (`applicativeInterchange` sy))
                `apply` Refl @fgp
    , Refl <- applicativeHomomorphism @f gap2 gpureAmpY
    , Refl <- applicativeFmap @f gap2 (sPure gpureAmpY)
    , Refl <- applicativeLiftA2 @f gap1 x (sPure gpureY)
    , Refl <- applicativeLiftA2 @f gap2 (sPure gpureAmpY) x
    = Refl
    where
      gap1 :: Sing ((<*>@#@$) :: g (p ~> q) ~> g p ~> g q)
      gap1 = singFun2 @(<*>@#@$) (%<*>)

      gap2 :: Sing ((<*>@#@$) :: g ((p ~> q) ~> q) ~> g (p ~> q) ~> g q)
      gap2 = singFun2 @(<*>@#@$) (%<*>)

      gpureY :: Sing (Pure y :: g p)
      gpureY = sPure sy

      fpureGap1 :: Sing (Pure (<*>@#@$) :: f (g (p ~> q) ~> g p ~> g q))
      fpureGap1 = sPure gap1

      ampGpureY :: Sing ((&@#@$$) (Pure y) :: (g p ~> g q) ~> g q)
      ampGpureY = singFun1 @((&@#@$$) (Pure y)) (gpureY %&)

      gpureAmpY :: Sing (Pure ((&@#@$$) y) :: g ((p ~> q) ~> q))
      gpureAmpY = sPure (singFun1 @((&@#@$$) y) (sy %&))

  applicativeFmap :: forall p q (h :: p ~> q) (x :: (f :.: g) p).
                     Sing h -> Sing x
                  -> Fmap h x :~: (Pure h <*> x)
  applicativeFmap sh (SComp1 (x :: Sing fgp))
    | Refl <- applicativeFmap @f (singFun1 @(FmapSym1 h) (sFmap sh)) x
    -- (Pure (FmapSym1 h) <*> fgp) :~: (Pure ((<*>@#@$$) (Pure h)) <*> fgp)
    , Refl <- Refl @(<*>@#@$)
                `apply` (Refl @PureSym0
                          `apply` funExt @(g p) @(g q)
                                         @(FmapSym1 h)
                                         @((<*>@#@$$) (Pure h))
                                         (applicativeFmap sh))
                `apply` Refl @fgp
    , Refl <- applicativeHomomorphism @f gap gpure
    , Refl <- applicativeFmap @f gap (sPure gpure)
    , Refl <- applicativeLiftA2 @f gap (sPure gpure) x
    = Refl
    where
      gap :: Sing ((<*>@#@$) :: g (p ~> q) ~> g p ~> g q)
      gap = singFun2 @(<*>@#@$) (%<*>)

      gpure :: Sing (Pure h :: g (p ~> q))
      gpure = sPure sh

  applicativeLiftA2 :: forall p q r (h :: p ~> q ~> r)
                              (x :: (f :.: g) p) (y :: (f :.: g) q).
                       Sing h -> Sing x -> Sing y
                    -> LiftA2 h x y :~: (Fmap h x <*> y)
  applicativeLiftA2 _ _ _ = Refl

instance GApplicative Identity where
  genericPureC _ = Refl
  genericApC SIdentity{} SIdentity{} = Refl
  liftA2FmapApC _ SIdentity{} SIdentity{} = Refl
-- Defined by hand due to TH staging restrictions. Blegh.
-- TODO: Rearrange modules to make the use of TH here possible.
instance VApplicative Identity where
  applicativeIdentity     = defaultApplicativeIdentity
  applicativeComposition  = defaultApplicativeComposition
  applicativeHomomorphism = defaultApplicativeHomomorphism
  applicativeInterchange  = defaultApplicativeInterchange
  applicativeFmap         = defaultApplicativeFmap
  applicativeLiftA2       = defaultApplicativeLiftA2
