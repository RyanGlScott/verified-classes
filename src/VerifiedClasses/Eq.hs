{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Eq (
    Eq(..), PEq(..), SEq(..), GEq(..), VEq(..)
  , genericEq, GenericEq, sGenericEq
  , defaultEqReflexive, defaultEqSymmetric, defaultEqTransitive

    -- * Template Haskell
  , deriveEqAll, deriveEqSingletons, deriveEqVerifiedOnly

    -- * Miscellaneous utilities
  , notEqSymmetric
  ) where

import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Generics
import VerifiedClasses.So
import VerifiedClasses.TH.Deriving

infix 4 `genericEq`
genericEq :: forall a. (Generic a, Eq (Rep a ()))
          => a -> a -> Bool
genericEq x y = from @a @() x == from y

infix 4 `GenericEq`
type GenericEq :: a -> a -> Bool
type family x `GenericEq` y where
  (x :: a) `GenericEq` y = (From x :: Rep a ()) == From y

infix 4 `sGenericEq`
sGenericEq :: forall a (x :: a) (y :: a).
              (SGeneric a, SEq (Rep a ()))
           => Sing x -> Sing y -> Sing (x `GenericEq` y)
sx `sGenericEq` sy = sFrom @a @() @x sx %== sFrom sy

$(singletonsOnly [d|
  instance Eq (V1 p) where
    _ == _ = True

  instance Eq (U1 p) where
    _ == _ = True

  instance Eq c => Eq (K1 i c p) where
    K1 x == K1 y = x == y

  instance Eq (f p) => Eq (M1 i c f p) where
    M1 x == M1 y = x == y

  instance (Eq (f p), Eq (g p)) => Eq ((f :*: g) p) where
    (x1 :*: y1) == (x2 :*: y2) = (x1 == x2) && (y1 == y2)

  instance (Eq (f p), Eq (g p)) => Eq ((f :+: g) p) where
    L1 x == L1 y = x == y
    R1 x == R1 y = x == y
    L1 _ == R1 _ = False
    R1 _ == L1 _ = False
  |])


class (Eq a, PEq a, SEq a) => VEq a where
  eqReflexive :: forall (x :: a).
                 Sing x -> So (x == x)

  eqSymmetric :: forall (x :: a) (y :: a).
                 Sing x -> Sing y
              -> So (x == y) -> So (y == x)

  eqTransitive :: forall (x :: a) (y :: a) (z :: a).
                  Sing x -> Sing y -> Sing z
               -> So (x == y) -> So (y == z) -> So (x == z)

defaultEqReflexive :: forall a (x :: a).
                      (SGeneric a, VEq (Rep a ()), GEq a)
                   => Sing x -> So (x == x)
defaultEqReflexive sx
    | Refl <- genericEqC sx sx
    = eqReflexive $ sFrom @a @() sx

defaultEqSymmetric :: forall a (x :: a) (y :: a).
                      (SGeneric a, VEq (Rep a ()), GEq a)
                   => Sing x -> Sing y
                   -> So (x == y) -> So (y == x)
defaultEqSymmetric sx sy
  | Refl <- genericEqC sx sy
  , Refl <- genericEqC sy sx
  = eqSymmetric (sFrom @a @() sx) (sFrom sy)

defaultEqTransitive :: forall a (x :: a) (y :: a) (z :: a).
                       (SGeneric a, VEq (Rep a ()), GEq a)
                    => Sing x -> Sing y -> Sing z
                    -> So (x == y) -> So (y == z) -> So (x == z)
defaultEqTransitive sx sy sz
  | Refl <- genericEqC sx sy
  , Refl <- genericEqC sy sz
  , Refl <- genericEqC sx sz
  = eqTransitive (sFrom @a @() sx) (sFrom sy) (sFrom sz)

eqClass :: Class
eqClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod '(==) 'genericEq TermLevel
                          ]
  }

pEqClass :: Class
pEqClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''(==) ''GenericEq (TypeLevel 2)
                          ]
  }

sEqClass :: Class
sEqClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod '(%==) 'sGenericEq TermLevel
                          ]
  }

vEqClass :: Class
vEqClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'eqReflexive  'defaultEqReflexive  TermLevel
                          , ClassMethod 'eqSymmetric  'defaultEqSymmetric  TermLevel
                          , ClassMethod 'eqTransitive 'defaultEqTransitive TermLevel
                          ]
  }

deriveEqAll :: Q TH.Type -> Q [Dec]
deriveEqAll = deriveClasses [eqClass, pEqClass, sEqClass, gClass, vEqClass]

deriveEqSingletons :: Q TH.Type -> Q [Dec]
deriveEqSingletons = deriveClasses [pEqClass, sEqClass, gClass, vEqClass]

deriveEqVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveEqVerifiedOnly = deriveClass vEqClass

class ( Eq a, PEq a, SEq a
      , Eq (Rep a ()), PEq (Rep a ()), SEq (Rep a ())
      , Generic a, PGeneric a, SGeneric a )
      => GEq a where
  genericEqC         :: forall (x :: a) (y :: a).
                        Sing x -> Sing y
                     -> (x == y) :~: (x `GenericEq` y)
  default genericEqC :: forall (x :: a) (y :: a).
                        (x == y) ~ (x `GenericEq` y)
                     => Sing x -> Sing y
                     -> (x == y) :~: (x `GenericEq` y)
  genericEqC _ _ = Refl

notEqSymmetric :: forall a (x :: a) (y :: a). VEq a
               => Sing x -> Sing y
               -> So (Not (x == y)) -> So (Not (y == x))
notEqSymmetric sx sy = contra (sy %== sx) (sx %== sy) (eqSymmetric sy sx)

instance VEq (V1 p) where
  eqReflexive _ = Oh
  eqSymmetric _ _ _ = Oh
  eqTransitive _ _ _ _ _ = Oh

instance VEq (U1 p) where
  eqReflexive _ = Oh
  eqSymmetric _ _ _ = Oh
  eqTransitive _ _ _ _ _ = Oh

instance VEq c => VEq (K1 i c p) where
  eqReflexive (SK1 x) = eqReflexive x
  eqSymmetric (SK1 x) (SK1 y) Oh = eqSymmetric x y Oh
  eqTransitive (SK1 x) (SK1 y) (SK1 z) Oh Oh = eqTransitive x y z Oh Oh

instance VEq (f p) => VEq (M1 i c f p) where
  eqReflexive (SM1 x) = eqReflexive x
  eqSymmetric (SM1 x) (SM1 y) Oh = eqSymmetric x y Oh
  eqTransitive (SM1 x) (SM1 y) (SM1 z) Oh Oh = eqTransitive x y z Oh Oh

instance (VEq (f p), VEq (g p)) => VEq ((f :+: g) p) where
  eqReflexive (SL1 l) = eqReflexive l
  eqReflexive (SR1 r) = eqReflexive r

  eqSymmetric x y Oh =
    case (x, y) of
      (SL1 lx, SL1 ly) -> eqSymmetric lx ly Oh
      (SR1 rx, SR1 ry) -> eqSymmetric rx ry Oh

  eqTransitive x y z Oh Oh =
    case (x, y, z) of
      (SL1 lx, SL1 ly, SL1 lz) -> eqTransitive lx ly lz Oh Oh
      (SR1 rx, SR1 ry, SR1 rz) -> eqTransitive rx ry rz Oh Oh

instance (VEq (f p), VEq (g p)) => VEq ((f :*: g) p) where
  eqReflexive (l :%*: r)
    | Oh <- eqReflexive l
    , Oh <- eqReflexive r
    = Oh

  eqSymmetric (l1 :%*: r1) (l2 :%*: r2) Oh =
    case (l1 %== l2, r1 %== r2) of
      (STrue, STrue)
        |  Oh <- eqSymmetric l1 l2 Oh
        ,  Oh <- eqSymmetric r1 r2 Oh
        -> Oh

  eqTransitive (l1 :%*: r1) (l2 :%*: r2) (l3 :%*: r3) Oh Oh =
    case (l1 %== l2, l2 %== l3, r1 %== r2, r2 %== r3) of
      (STrue, STrue, STrue, STrue)
        |  Oh <- eqTransitive l1 l2 l3 Oh Oh
        ,  Oh <- eqTransitive r1 r2 r3 Oh Oh
        -> Oh
