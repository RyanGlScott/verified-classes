{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Functor (
    Functor(..), PFunctor(..), SFunctor(..), GFunctor(..), VFunctor(..)
  , genericFmap, GenericFmap, sGenericFmap
  , defaultFmapId, defaultFmapCompose, defaultFunctorReplace

    -- * Template Haskell
  , deriveFunctorAll, deriveFunctorSingletons, deriveFunctorVerifiedOnly
  ) where

import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Kind
import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.TH.Deriving

genericFmap :: forall f a b. (Generic1 f, Functor (Rep1 f))
            => (a -> b) -> f a -> f b
genericFmap g x = to1 (fmap g (from1 x))

type GenericFmap :: (a ~> b) -> f a -> f b
type family GenericFmap g x where
  GenericFmap g x = To1 (Fmap g (From1 x))

sGenericFmap :: forall f a b (g :: a ~> b) (x :: f a).
                (SGeneric1 f, SFunctor (Rep1 f))
             => Sing g -> Sing x -> Sing (GenericFmap g x)
sGenericFmap sg sx = sTo1 (sFmap sg (sFrom1 sx))

$(singletonsOnly [d|
  instance Functor V1 where
    fmap _ x = case x of {}

  instance Functor U1 where
    fmap _ _ = U1

  instance Functor (K1 i c) where
    fmap _ (K1 x) = K1 x

  instance Functor f => Functor (M1 i c f) where
    fmap f (M1 x) = M1 (fmap f x)

  instance (Functor f, Functor g) => Functor (f :*: g) where
    fmap f (x :*: y) = fmap f x :*: fmap f y

  instance (Functor f, Functor g) => Functor (f :+: g) where
    fmap f (L1 x) = L1 (fmap f x)
    fmap f (R1 y) = R1 (fmap f y)

  instance Functor Par1 where
    fmap f (Par1 x) = Par1 (f x)

  instance Functor f => Functor (Rec1 f) where
    fmap f (Rec1 x) = Rec1 (fmap f x)

  instance (Functor f, Functor g) => Functor ((f :: Type -> Type) :.: g) where
    fmap f (Comp1 x) = Comp1 (fmap (fmap f) x)
  |])

class (Functor f, PFunctor f, SFunctor f) => VFunctor f where
  fmapId :: forall a (x :: f a). Sing x -> Fmap IdSym0 x :~: x

  fmapCompose :: forall a b c (g :: b ~> c) (h :: a ~> b) (x :: f a).
                 Sing g -> Sing h -> Sing x
              -> Fmap g (Fmap h x) :~: Fmap (g .@#@$$$ h) x

  functorReplace :: forall a b (x :: a) (y :: f b).
                    Sing x -> Sing y
                 -> (x <$ y) :~: Fmap (ConstSym1 x) y

defaultFmapId :: forall f a (x :: f a).
                 (VGeneric1 f, VFunctor (Rep1 f), GFunctor f)
              => Sing x -> Fmap IdSym0 x :~: x
defaultFmapId sx
  | Refl <- genericFmapC (sing @IdSym0) sx
  , Refl <- fmapId (sFrom1 sx)
  , Refl <- sTof1 sx
  = Refl

defaultFmapCompose :: forall f a b c (g :: b ~> c) (h :: a ~> b) (x :: f a).
                      (VGeneric1 f, VFunctor (Rep1 f), GFunctor f)
                   => Sing g -> Sing h -> Sing x
                   -> Fmap g (Fmap h x) :~: Fmap (g .@#@$$$ h) x
defaultFmapCompose sg sh sx
  | Refl <- genericFmapC sh sx
  , Refl <- genericFmapC sg (sFmap sh sx)
  , Refl <- genericFmapC (singFun1 @(g .@#@$$$ h) (sg %. sh)) sx
  , Refl <- sFot1 @Type @f (sFmap sh sf1x)
  , Refl <- fmapCompose sg sh sf1x
  = Refl
  where
    sf1x :: Sing (From1 x)
    sf1x = sFrom1 sx

defaultFunctorReplace :: forall f a b (x :: a) (y :: f b).
                         GFunctor f
                      => Sing x -> Sing y
                      -> (x <$ y) :~: Fmap (ConstSym1 x) y
defaultFunctorReplace = replaceFmapConstC

functorClass :: Class
functorClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod 'fmap 'genericFmap TermLevel
                          ]
  }

pFunctorClass :: Class
pFunctorClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''Fmap ''GenericFmap (TypeLevel 2)
                          ]
  }

sFunctorClass :: Class
sFunctorClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod 'sFmap 'sGenericFmap TermLevel
                          ]
  }

vFunctorClass :: Class
vFunctorClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'fmapId         'defaultFmapId         TermLevel
                          , ClassMethod 'fmapCompose    'defaultFmapCompose    TermLevel
                          , ClassMethod 'functorReplace 'defaultFunctorReplace TermLevel
                          ]
  }

deriveFunctorAll :: Q TH.Type -> Q [Dec]
deriveFunctorAll = deriveClasses [functorClass, pFunctorClass, sFunctorClass, gClass, vFunctorClass]

deriveFunctorSingletons :: Q TH.Type -> Q [Dec]
deriveFunctorSingletons = deriveClasses [pFunctorClass, sFunctorClass, gClass, vFunctorClass]

deriveFunctorVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveFunctorVerifiedOnly = deriveClass vFunctorClass

class ( Generic1 f
      , Functor f, PFunctor f, SFunctor f
      , Functor (Rep1 f), PFunctor (Rep1 f), SFunctor (Rep1 f) )
      => GFunctor f where
  genericFmapC :: forall a b (g :: a ~> b) (x :: f a).
                  Sing g -> Sing x
               -> Fmap g x :~: GenericFmap g x
  default genericFmapC :: forall a b (g :: a ~> b) (x :: f a).
                          Fmap g x ~ GenericFmap g x
                       => Sing g -> Sing x
                       -> Fmap g x :~: GenericFmap g x
  genericFmapC _ _ = Refl

  replaceFmapConstC         :: forall a b (x :: a) (y :: f b).
                               Sing x -> Sing y
                            -> (x <$ y) :~: Fmap (ConstSym1 x) y
  default replaceFmapConstC :: forall a b (x :: a) (y :: f b).
                               (x <$ y) ~ Fmap (ConstSym1 x) y
                            => Sing x -> Sing y
                            -> (x <$ y) :~: Fmap (ConstSym1 x) y
  replaceFmapConstC _ _ = Refl

instance VFunctor V1 where
  fmapId x = case x of {}
  fmapCompose _ _ x = case x of {}
  functorReplace _ _ = Refl

instance VFunctor U1 where
  fmapId SU1 = Refl
  fmapCompose _ _ SU1 = Refl
  functorReplace _ _ = Refl

instance VFunctor (K1 i c) where
  fmapId SK1{} = Refl
  fmapCompose _ _ SK1{} = Refl
  functorReplace _ _ = Refl

instance VFunctor f => VFunctor (M1 i c f) where
  fmapId (SM1 x) | Refl <- fmapId x = Refl
  fmapCompose sg sh (SM1 x) | Refl <- fmapCompose sg sh x = Refl
  functorReplace _ _ = Refl

instance (VFunctor f, VFunctor g) => VFunctor (f :*: g) where
  fmapId (x :%*: y)
    | Refl <- fmapId x
    , Refl <- fmapId y
    = Refl
  fmapCompose sg sh (x :%*: y)
    | Refl <- fmapCompose sg sh x
    , Refl <- fmapCompose sg sh y
    = Refl
  functorReplace _ _ = Refl

instance (VFunctor f, VFunctor g) => VFunctor (f :+: g) where
  fmapId (SL1 x) | Refl <- fmapId x = Refl
  fmapId (SR1 y) | Refl <- fmapId y = Refl

  fmapCompose sg sh (SL1 x) | Refl <- fmapCompose sg sh x = Refl
  fmapCompose sg sh (SR1 y) | Refl <- fmapCompose sg sh y = Refl

  functorReplace _ _ = Refl

instance VFunctor Par1 where
  fmapId SPar1{} = Refl
  fmapCompose _ _ SPar1{} = Refl
  functorReplace _ _ = Refl

instance VFunctor f => VFunctor (Rec1 f) where
  fmapId (SRec1 x) | Refl <- fmapId x = Refl
  fmapCompose sg sh (SRec1 x) | Refl <- fmapCompose sg sh x = Refl
  functorReplace _ _ = Refl

instance (VFunctor f, VFunctor g) => VFunctor (f :.: g) where
  fmapId (SComp1 (fgp :: Sing (fgp :: f (g p))))
    | -- Fmap (FmapSym1 IdSym0) fgp :~: Fmap IdSym0 fgp
      Refl <- Refl @FmapSym0
                `apply` funExt @(g p) @(g p) @(FmapSym1 IdSym0) @IdSym0 fmapId
                `apply` Refl @fgp
    , Refl <- fmapId fgp
    = Refl

  fmapCompose :: forall p q r (h :: q ~> r) (i :: p ~> q) (x :: (f :.: g) p).
                 Sing h -> Sing i -> Sing x
              -> Fmap h (Fmap i x) :~: Fmap (h .@#@$$$ i) x
  fmapCompose sh si (SComp1 (fgp :: Sing fgp))
    | -- Fmap (FmapSym1 h .@#@$$$ FmapSym1 i) fgp :~: Fmap (FmapSym1 (h .@#@$$$ i)) fgp
      Refl <- Refl @FmapSym0
                `apply` funExt @(g p) @(g r)
                               @(FmapSym1 h .@#@$$$ FmapSym1 i)
                               @(FmapSym1 (h .@#@$$$ i))
                               (fmapCompose sh si)
                `apply` Refl @fgp
    , Refl <- fmapCompose (singFun1 @(FmapSym1 h) (sFmap sh))
                          (singFun1 @(FmapSym1 i) (sFmap si))
                          fgp
    = Refl

  functorReplace :: forall p q (x :: p) (y :: (f :.: g) q).
                    Sing x -> Sing y
                 -> (x <$ y) :~: Fmap (ConstSym1 x) y
  functorReplace _ _ = Refl

instance GFunctor Identity where
  genericFmapC _ SIdentity{} = Refl
  replaceFmapConstC _ SIdentity{} = Refl
-- Defined by hand due to TH staging restrictions. Blegh.
-- TODO: Rearrange modules to make the use of TH here possible.
instance VFunctor Identity where
  fmapId         = defaultFmapId
  fmapCompose    = defaultFmapCompose
  functorReplace = defaultFunctorReplace
