{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TemplateHaskell #-}

module VerifiedClasses.AbelianSemigroup (
    AbelianSemigroup, PAbelianSemigroup, SAbelianSemigroup, VAbelianSemigroup(..)
  , defaultSemigroupCommutative

    -- * TemplateHaskell
  , deriveAbelianSemigroupAll
  , deriveAbelianSemigroupSingletons
  , deriveAbelianSemigroupVerifiedOnly
  ) where

import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import VerifiedClasses.Generics
import VerifiedClasses.Semigroup
import VerifiedClasses.TH.Deriving

$(singletons [d|
  class Semigroup a => AbelianSemigroup a

  instance AbelianSemigroup (V1 p)
  instance AbelianSemigroup (U1 p)
  instance AbelianSemigroup c => AbelianSemigroup (K1 i c p)
  instance AbelianSemigroup (f p) => AbelianSemigroup (M1 i c f p)
  instance (AbelianSemigroup (f p), AbelianSemigroup (g p)) => AbelianSemigroup ((f :*: g) p)
  |])

class (AbelianSemigroup a, PAbelianSemigroup a, SAbelianSemigroup a, VSemigroup a)
    => VAbelianSemigroup a where
  semigroupCommutative :: forall (x :: a) (y :: a).
                          Sing x -> Sing y
                       -> (x <> y) :~: (y <> x)

defaultSemigroupCommutative :: forall a (x :: a) (y :: a).
                               (VGeneric a, VAbelianSemigroup (Rep a ()), GSemigroup a)
                            => Sing x -> Sing y
                            -> (x <> y) :~: (y <> x)
defaultSemigroupCommutative sx sy
  | Refl <- genericAppendC sx sy
  , Refl <- genericAppendC sy sx
  , Refl <- semigroupCommutative (sFrom @a @() @x sx) (sFrom sy)
  = Refl

vAbelianSemigroupClass :: Class
vAbelianSemigroupClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'semigroupCommutative 'defaultSemigroupCommutative TermLevel
                          ]
  }

abelianSemigroupClass :: Class
abelianSemigroupClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = []
  }

pAbelianSemigroupClass :: Class
pAbelianSemigroupClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = []
  }

sAbelianSemigroupClass :: Class
sAbelianSemigroupClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = []
  }

deriveAbelianSemigroupAll :: Q TH.Type -> Q [Dec]
deriveAbelianSemigroupAll = deriveClasses [abelianSemigroupClass, pAbelianSemigroupClass, sAbelianSemigroupClass, vAbelianSemigroupClass]

deriveAbelianSemigroupSingletons :: Q TH.Type -> Q [Dec]
deriveAbelianSemigroupSingletons = deriveClasses [pAbelianSemigroupClass, sAbelianSemigroupClass, vAbelianSemigroupClass]

deriveAbelianSemigroupVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveAbelianSemigroupVerifiedOnly = deriveClass vAbelianSemigroupClass

instance VAbelianSemigroup (V1 p) where
  semigroupCommutative x _ = case x of {}

instance VAbelianSemigroup (U1 p) where
  semigroupCommutative _ _ = Refl

instance VAbelianSemigroup c => VAbelianSemigroup (K1 i c p) where
  semigroupCommutative (SK1 x) (SK1 y)
    | Refl <- semigroupCommutative x y
    = Refl

instance VAbelianSemigroup (f p) => VAbelianSemigroup (M1 i c f p) where
  semigroupCommutative (SM1 x) (SM1 y)
    | Refl <- semigroupCommutative x y
    = Refl

instance (VAbelianSemigroup (f p), VAbelianSemigroup (g p)) => VAbelianSemigroup ((f :*: g) p) where
  semigroupCommutative (x1 :%*: y1) (x2 :%*: y2)
    | Refl <- semigroupCommutative x1 x2
    , Refl <- semigroupCommutative y1 y2
    = Refl
