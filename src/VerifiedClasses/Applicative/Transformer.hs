{-# LANGUAGE DataKinds #-}

module VerifiedClasses.Applicative.Transformer (VApplicativeTransformer(..)) where

import Data.Kind
import Data.Singletons
import Data.Type.Equality

import VerifiedClasses.Applicative

class (VApplicative f, VApplicative g) =>
  VApplicativeTransformer (t :: forall (z :: Type). f z ~> g z) where
    applicativeTransformerPure :: forall a (x :: a).
                                  (forall z. Sing (t :: f z ~> g z)) -> Sing x
                               -> t @@ (Pure x) :~: Pure x
    applicativeTransformerAp :: forall a b (x :: f (a ~> b)) (y :: f a).
                                (forall z. Sing (t :: f z ~> g z)) -> Sing x -> Sing y
                             -> t @@ (x <*> y) :~: (t @@ x <*> t @@ y)
