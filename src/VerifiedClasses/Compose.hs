{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module VerifiedClasses.Compose (
  -- * The 'Compose' singleton
  Sing, SCompose(..), GetCompose,

  -- * Defunctionalization symbols
  ComposeSym0, ComposeSym1,
  GetComposeSym0, GetComposeSym1
  ) where

import Control.Applicative

import Data.Function.Singletons
import Data.Functor.Compose
import Data.Kind
import Data.Singletons.Base.TH
import Data.Singletons.TH.Options

import GHC.Generics

import VerifiedClasses.Alternative
import VerifiedClasses.Applicative
import VerifiedClasses.Functor
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.TH.Generics
import VerifiedClasses.Utils

$(withOptions defaultOptions{genSingKindInsts = False} $
  genSingletons [''Compose])
$(singletonsOnly [d| deriving instance Show (f (g a)) => Show (Compose f g a) |])
$(verifyGeneric0 ''Compose)
$(genSingletonsWithTypeGeneric
  [t|    forall f g. Functor f
      => Generic1 (Compose (f :: Type -> Type) g)
    |])

instance VFunctor f => VGeneric1 (Compose f g) where
  sTof1 (SCompose (sx :: Sing (x :: f (g a))))
    | Refl <- fmapCompose (sing @UnRec1Sym0) (sing @Rec1Sym0) sx
    , Refl <- -- Fmap (UnRec1Sym0 .@#@$$$ Rec1Sym0) x :~: Fmap IdSym0 x
              Refl @FmapSym0
                `apply` funExt @(g a) @(g a)
                               @(UnRec1Sym0 .@#@$$$ Rec1Sym0) @IdSym0
                               lemma
                `apply` Refl @x
    , Refl <- fmapId sx
    = Refl
    where
      lemma :: forall (z :: g a).
               Sing z -> UnRec1 ('Rec1 z) :~: z
      lemma _ = Refl

  sFot1 (SM1 (SM1 (SM1 (SComp1 (sx :: Sing (x :: f (Rec1 g a)))))))
    | Refl <- fmapCompose (sing @Rec1Sym0) (sing @UnRec1Sym0) sx
    , Refl <- -- Fmap (Rec1Sym0 .@#@$$$ UnRec1Sym0) x :~: Fmap IdSym0 x
              Refl @FmapSym0
                `apply` funExt @(Rec1 g a) @(Rec1 g a)
                               @(Rec1Sym0 .@#@$$$ UnRec1Sym0) @IdSym0
                               lemma
                `apply` Refl @x
    , Refl <- fmapId sx
    = Refl
    where
      lemma :: forall (z :: Rec1 g a).
               Sing z -> 'Rec1 (UnRec1 z) :~: z
      lemma SRec1{} = Refl

$(singletonsOnly [d|
  instance (Functor f, Functor g) => Functor (Compose (f :: Type -> Type) g) where
    fmap f (Compose x) = Compose (fmap (fmap f) x)

  instance (Applicative f, Applicative g) => Applicative (Compose (f :: Type -> Type) g) where
    -- #15703
    -- pure = Compose . pure . pure
    pure x = Compose (pure (pure x))
    Compose f <*> Compose x = Compose (liftA2 (<*>) f x)
  |])

instance (VFunctor f, VFunctor g) => GFunctor (Compose f g) where
  genericFmapC :: forall a b (h :: a ~> b) (x :: Compose f g a).
                  Sing h -> Sing x
               -> Fmap h x :~: GenericFmap h x
  genericFmapC sh (SCompose (sfga :: Sing fga))
    | Refl <- fmapCompose sFmapHFun sRec1Fun sfga
    , Refl <- fmapCompose (singFun1 @UnRec1Sym0 sUnRec1) (sFmapHFun %.$$$ sRec1Fun) sfga
    , Refl <- -- Fmap (FmapSym1 h) fga :~: Fmap (UnRec1Sym0 .@#@$$$ FmapSym1 h .@#@$$$ Rec1Sym0) sfga
              Refl @FmapSym0
                `apply` funExt @(g a) @(g b)
                               @(FmapSym1 h)
                               @(UnRec1Sym0 .@#@$$$ FmapSym1 h .@#@$$$ Rec1Sym0)
                               (const Refl)
                `apply` Refl @fga
    = Refl
    where
      sFmapHFun :: Sing (FmapSym1 h :: Rec1 g a ~> Rec1 g b)
      sFmapHFun = singFun1 (sFmap sh)

      sRec1Fun :: Sing Rec1Sym0
      sRec1Fun = singFun1 SRec1

instance (VApplicative f, VApplicative g) => GApplicative (Compose f g) where
  genericPureC :: forall a (x :: a).
                  Sing x -> (Pure x :: Compose f g a) :~: GenericPure x
  genericPureC sx
    | Refl <- applicativeFmap sUnRec1Fun (sPure @f sRec1PureX)
    , Refl <- applicativeHomomorphism @f sUnRec1Fun sRec1PureX
    = Refl
    where
      sRec1PureX :: Sing ('Rec1 (Pure x) :: Rec1 g a)
      sRec1PureX = SRec1 (sPure sx)

      sUnRec1Fun :: Sing UnRec1Sym0
      sUnRec1Fun = singFun1 sUnRec1

  genericApC :: forall a b (h :: Compose f g (a ~> b)) (x :: Compose f g a).
                Sing h -> Sing x
             -> (h <*> x) :~: (h `GenericAp` x)
  genericApC (SCompose (x1 :: Sing x1)) (SCompose (x2 :: Sing x2))
    {-
    (1)  fmap unRec1 (fmap (<*>) (fmap Rec1 x1) <*> fmap Rec1 x2)                                             [apFmap]
    (2)  pure unRec1 <*> (fmap (<*>) (fmap Rec1 x1) <*> fmap Rec1 x2)                                         [apComp]
    (3)  pure (.) <*> pure unRec1 <*> fmap (<*>) (fmap Rec1 x1) <*> fmap Rec1 x2                              [fmapComp]
    (4)  pure (.) <*> pure unRec1 <*> fmap ((<*>) . Rec1) x1 <*> fmap Rec1 x2                                 [apHom]
    (5)  pure ((.) unRec1) <*> fmap ((<*>) . Rec1) x1 <*> fmap Rec1 x2                                        [apFmap]
    (6)  pure ((.) unRec1) <*> (pure ((<*>) . Rec1) <*> x1) <*> fmap Rec1 x2                                  [apComp]
    (7)  pure (.) <*> pure ((.) unRec1) <*> pure ((<*>) . Rec1) <*> x1 <*> fmap Rec1 x2                       [apHom (2)]
    (8)  pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1 <*> fmap Rec1 x2                                       [apFmap]
    (9)  pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1 <*> (pure Rec1 <*> x2)                                 [apComp]
    (10) pure (.) <*> (pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1) <*> pure Rec1 <*> x2                    [apInt]
    (11) pure ((&) Rec1) <*> (pure (.) <*> (pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1)) <*> x2            [apComp]
    (12) pure (.) <*> pure ((&) Rec1) <*> pure (.) <*> (pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1) <*> x2 [apHom (2)]
    (13) pure ((.) ((&) Rec1) (.)) <*> (pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1) <*> x2                 [apComp]
    (14) pure (.) <*> pure ((.) ((&) Rec1) (.)) <*> pure ((.) ((.) unRec1) ((<*>) . Rec1)) <*> x1 <*> x2      [apHom (2)]
    (15) pure ((.) ((.) ((&) Rec1) (.)) ((.) ((.) unRec1) ((<*>) . Rec1))) <*> x1 <*> x2                      [def, funExt]
    (16) pure (<*>) <*> x1 <*> x2                                                                             [apFmap]
         fmap (<*>) x1 <*> x2
    -}
    | -- (1)
      Refl <- applicativeFmap @f sUnRec1Fun (sFmap sApFun (sFmap sRec1Fun x1) %<*> sFmap sRec1Fun x2)
      -- (2)
    , Refl <- applicativeComposition @f (sPure sUnRec1Fun) (sFmap sApFun (sFmap sRec1Fun x1)) (sFmap sRec1Fun x2)
      -- (3)
    , Refl <- fmapCompose @f sApFun sRec1Fun x1
      -- (4)
    , Refl <- applicativeHomomorphism @f (sCompFun @(Rec1 g a) @(Rec1 g b)) sUnRec1Fun
      -- (5)
    , Refl <- applicativeFmap @f sApCompRec1Fun x1
      -- (6)
    , Refl <- applicativeComposition @f (sPure (sCompFun @@ sUnRec1Fun)) (sPure sApCompRec1Fun) x1
      -- (7)
    , Refl <- applicativeHomomorphism @f (sCompFun @(g (a ~> b))) (sCompFun @(Rec1 g a) @(Rec1 g b) @@ sUnRec1Fun)
    , Refl <- applicativeHomomorphism @f (sCompFun @@ (sCompFun @(Rec1 g a) @(Rec1 g b) @@ sUnRec1Fun)) sApCompRec1Fun
      -- (8)
    , Refl <- applicativeFmap @f sRec1Fun x2
      -- (9)
    , Refl <- applicativeComposition @f (sPure (sCompFun @@ (sCompFun @@ sUnRec1Fun) @@ sApCompRec1Fun) %<*> x1) (sPure sRec1Fun) x2
      -- (10)
    , Refl <- applicativeInterchange @f (sPure sCompFun %<*> (sPure (sCompFun @@ (sCompFun @@ sUnRec1Fun) @@ sApCompRec1Fun) %<*> x1)) sRec1Fun
      -- (11)
    , Refl <- applicativeComposition @f (sPure sAndRec1Fun) (sPure sCompFun) (sPure (sCompFun @@ (sCompFun @@ sUnRec1Fun) @@ sApCompRec1Fun) %<*> x1)
      -- (12)
    , Refl <- applicativeHomomorphism @f (sCompFun @(Rec1 g a ~> g b) @((g a ~> Rec1 g a) ~> g a ~> g b)) sAndRec1Fun
    , Refl <- applicativeHomomorphism @f (sCompFun @(Rec1 g a ~> g b) @((g a ~> Rec1 g a) ~> g a ~> g b) @@ sAndRec1Fun) sCompFun
      -- (13)
    , Refl <- applicativeComposition @f (sPure (sCompFun @@ sAndRec1Fun @@ sCompFun)) (sPure (sCompFun @@ (sCompFun @@ sUnRec1Fun) @@ sApCompRec1Fun)) x1
      -- (14)
    , Refl <- applicativeHomomorphism @f (sCompFun @(g (a ~> b)))
                                         (sCompFun @(Rec1 g a ~> g b) @((g a ~> Rec1 g a) ~> g a ~> g b) @@ sAndRec1Fun @@ sCompFun)
    , Refl <- applicativeHomomorphism @f (sCompFun @(g (a ~> b)) @@ (sCompFun @@ sAndRec1Fun @@ sCompFun))
                                         (sCompFun @@ (sCompFun @(Rec1 g a) @(Rec1 g b) @@ sUnRec1Fun) @@ sApCompRec1Fun)
      -- (15)
    , Refl <- --     (Pure ((.@#@$$$) ((.@#@$$$) ((&@#@$$) Rec1Sym0) (.@#@$)) ((.@#@$$$) ((.@#@$$) UnRec1Sym0) ((<*>@#@$) .@#@$$$ Rec1Sym0))) <*> x1 <*> x2)
              -- :~: (Pure (<*>@#@$) <*> x1 <*> x2)
              Refl @(<*>@#@$)
                `apply` (Refl @(<*>@#@$)
                          `apply` (Refl @PureSym0
                                    `apply` funExt2 @(g (a ~> b)) @(g a) @(g b)
                                                    @((.@#@$$$) ((.@#@$$$) ((&@#@$$) Rec1Sym0) (.@#@$)) ((.@#@$$$) ((.@#@$$) UnRec1Sym0) ((<*>@#@$) .@#@$$$ Rec1Sym0)))
                                                    @(<*>@#@$)
                                                    lemma)
                          `apply` Refl @x1)
                `apply` Refl @x2
      -- (16)
    , Refl <- applicativeFmap @f sApFun x1

    , Refl <- applicativeLiftA2 @f sApFun x1 x2
    , Refl <- applicativeLiftA2 @f sApFun (sFmap sRec1Fun x1) (sFmap sRec1Fun x2)
    = Refl
    where
      lemma :: forall (k :: g (a ~> b)) (z :: g a).
               Sing k -> Sing z
            -> UnRec1 ('Rec1 k <*> 'Rec1 z) :~: (k <*> z)
      lemma _ _ = Refl

      sRec1Fun :: forall j p. Sing (Rec1Sym0 :: j p ~> Rec1 j p)
      sRec1Fun = singFun1 SRec1

      sUnRec1Fun :: forall j p. Sing (UnRec1Sym0 :: Rec1 j p ~> j p)
      sUnRec1Fun = singFun1 sUnRec1

      sCompFun :: forall p q r. Sing ((.@#@$) :: (q ~> r) ~> (p ~> q) ~> p ~> r)
      sCompFun = singFun3 (%.)

      sApFun :: forall j p q. SApplicative j => Sing ((<*>@#@$) :: j (p ~> q) ~> j p ~> j q)
      sApFun = singFun2 (%<*>)

      sAndRec1Fun :: forall j p r. Sing ((&@#@$$) Rec1Sym0 :: ((j p ~> Rec1 j p) ~> r) ~> r)
      sAndRec1Fun = singFun1 (sRec1Fun %&)

      sApCompRec1Fun :: forall j p q. SApplicative j
                     => Sing ((<*>@#@$) .@#@$$$ Rec1Sym0 :: j (p ~> q) ~> Rec1 j p ~> Rec1 j q)
      sApCompRec1Fun = singFun1 (sApFun %. sRec1Fun)

$(deriveFunctorVerifiedOnly [t| forall f g. (Functor f, Functor g) => Functor (Compose f g) |])
$(deriveApplicativeVerifiedOnly [t| forall f g. (Applicative f, Applicative g) => Applicative (Compose f g) |])
$(deriveAlternativeSingletons [t| forall f g. (Alternative f, Applicative g) => Alternative (Compose (f :: Type -> Type) g) |])
