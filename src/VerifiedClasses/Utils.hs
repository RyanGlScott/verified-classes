{-# LANGUAGE DataKinds #-}

module VerifiedClasses.Utils where

import Prelude.Singletons

infixr 9 %.$$$
(%.$$$) :: forall a b c (f :: b ~> c) (g :: a ~> b).
           Sing f -> Sing g -> Sing (f .@#@$$$ g)
sf %.$$$ sg = singFun1 @(f .@#@$$$ g) (sf %. sg)
