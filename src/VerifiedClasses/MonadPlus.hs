{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.MonadPlus (
    MonadPlus(..), PMonadPlus(..), SMonadPlus(..), GMonadPlus(..), VMonadPlus(..)
  , defaultLeftZero, defaultRightZero, defaultMonadPlusMzero, defaultMonadPlusMplus

    -- * Template Haskell
  , deriveMonadPlusAll, deriveMonadPlusSingletons, deriveMonadPlusVerifiedOnly
  ) where

import Control.Applicative.Singletons hiding (Const, ConstSym1)
import Control.Monad
import Control.Monad.Singletons

import Data.Kind
import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Alternative
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.Monad
import VerifiedClasses.TH.Deriving
import VerifiedClasses.Utils

$(singletonsOnly [d|
  instance MonadPlus U1
  instance MonadPlus f => MonadPlus (M1 i c f)
  instance (MonadPlus f, MonadPlus g) => MonadPlus (f :*: g)
  instance MonadPlus f => MonadPlus (Rec1 f)
  |])

class ( MonadPlus m, PMonadPlus m, SMonadPlus m
      , VAlternative m, VMonad m ) => VMonadPlus m where
  leftZero  :: forall a b (f :: a ~> m b).
               Sing f -> (Mzero >>= f) :~: Mzero

  rightZero :: forall a b (x :: m a).
               Sing x -> (x >> Mzero) :~: (Mzero :: m b)

  monadPlusMzero :: forall a. (Mzero :: m a) :~: Empty
  monadPlusMplus :: forall a (x :: m a) (y :: m a).
                    Sing x -> Sing y
                 -> Mplus x y :~: (x <|> y)

defaultLeftZero :: forall m a b (f :: a ~> m b).
                   (VGeneric1 m, VMonadPlus (Rep1 m), GMonadPlus m)
                => Sing f -> (Mzero >>= f) :~: Mzero
defaultLeftZero sf
  | Refl <- genericEmptyC @m @a
  , Refl <- genericEmptyC @m @b
  , Refl <- genericBindC sMzero sf
  , Refl <- sFot1 @Type @m @a sMzero
  , Refl <- leftZero (singFun1 @From1Sym0 sFrom1' %.$$$ sf)

  , Refl <- mzeroEmptyC @m @a
  , Refl <- mzeroEmptyC @m @b
  , Refl <- monadPlusMzero @(Rep1 m) @a
  , Refl <- monadPlusMzero @(Rep1 m) @b
  = Refl

defaultRightZero :: forall m a b (x :: m a).
                    (VGeneric1 m, VMonadPlus (Rep1 m), GMonadPlus m)
                 => Sing x -> (x >> Mzero) :~: (Mzero :: m b)
defaultRightZero sx
  | Refl <- genericEmptyC @m @a
  , Refl <- genericEmptyC @m @b
  , Refl <- genericBindC sx (singFun1 @(ConstSym1 (Mzero :: m b)) (sConst sMzero))
  , Refl <- --     (From1 x >>= From1Sym0 .@#@$$$ ConstSym1 (To1 Mzero :: m b))
            -- :~: (From1 x >>= ConstSym1 (From1 (To1 Mzero :: m b)))
            Refl @((>>=@#@$$) (From1 x))
              `apply` funExt @a @(Rep1 m b)
                             @(From1Sym0 .@#@$$$ ConstSym1 (To1 Mzero :: m b))
                             @(ConstSym1 (From1 (To1 Mzero :: m b)))
                             (const (applyFrom1Sym0 @Type @m @b (sTo1 sMzero)))
  , Refl <- sFot1 @Type @m @b sMzero
  , Refl <- rightZero @(Rep1 m) @a @b (sFrom1 sx)

  , Refl <- mzeroEmptyC @m @a
  , Refl <- mzeroEmptyC @m @b
  , Refl <- monadPlusMzero @(Rep1 m) @a
  , Refl <- monadPlusMzero @(Rep1 m) @b
  , Refl <- monadSeqBindC @m @a @b sx sMzero
  , Refl <- monadSeq @(Rep1 m) @a @b (sFrom1 sx) sMzero
  = Refl

defaultMonadPlusMzero :: forall m a.
                         GMonadPlus m
                      => (Mzero :: m a) :~: Empty
defaultMonadPlusMzero = mzeroEmptyC

defaultMonadPlusMplus :: forall m a (x :: m a) (y :: m a).
                         GMonadPlus m
                      => Sing x -> Sing y
                      -> Mplus x y :~: (x <|> y)
defaultMonadPlusMplus = mplusAltC

monadPlusClass :: Class
monadPlusClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = []
  }

pMonadPlusClass :: Class
pMonadPlusClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = []
  }

sMonadPlusClass :: Class
sMonadPlusClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = []
  }

vMonadPlusClass :: Class
vMonadPlusClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'leftZero       'defaultLeftZero       TermLevel
                          , ClassMethod 'rightZero      'defaultRightZero      TermLevel
                          , ClassMethod 'monadPlusMzero 'defaultMonadPlusMzero TermLevel
                          , ClassMethod 'monadPlusMplus 'defaultMonadPlusMplus TermLevel
                          ]
  }

deriveMonadPlusAll :: Q TH.Type -> Q [Dec]
deriveMonadPlusAll = deriveClasses [monadPlusClass, pMonadPlusClass, sMonadPlusClass, gClass, vMonadPlusClass]

deriveMonadPlusSingletons :: Q TH.Type -> Q [Dec]
deriveMonadPlusSingletons = deriveClasses [pMonadPlusClass, sMonadPlusClass, gClass, vMonadPlusClass]

deriveMonadPlusVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveMonadPlusVerifiedOnly = deriveClass vMonadPlusClass

class ( GAlternative m, GMonad m
      , MonadPlus m, PMonadPlus m, SMonadPlus m
      , MonadPlus (Rep1 m), PMonadPlus (Rep1 m), SMonadPlus (Rep1 m) )
      => GMonadPlus m where
  mzeroEmptyC         :: forall a.
                         (Mzero :: m a) :~: Empty
  default mzeroEmptyC :: forall a.
                         (Mzero :: m a)  ~  Empty
                      => (Mzero :: m a) :~: Empty
  mzeroEmptyC = Refl

  mplusAltC         :: forall a (x :: m a) (y :: m a).
                       Sing x -> Sing y
                    -> Mplus x y :~: (x <|> y)
  default mplusAltC :: forall a (x :: m a) (y :: m a).
                       Mplus x y ~ (x <|> y)
                    => Sing x -> Sing y
                    -> Mplus x y :~: (x <|> y)
  mplusAltC _ _ = Refl

instance VMonadPlus U1 where
  leftZero  _ = Refl
  rightZero _ = Refl
  monadPlusMzero = Refl
  monadPlusMplus _ _ = Refl

instance VMonadPlus f => VMonadPlus (M1 i c f) where
  leftZero :: forall a b (g :: a ~> M1 i c f b).
              Sing g -> (Mzero >>= g) :~: Mzero
  leftZero sf
    | Refl <- leftZero @f (singFun1 @UnM1Sym0 sUnM1 %.$$$ sf)
    , Refl <- monadPlusMzero @f @a
    , Refl <- monadPlusMzero @f @b
    = Refl

  rightZero :: forall a b (x :: M1 i c f a). Sing x
            -> (x >> Mzero) :~: (Mzero :: M1 i c f b)
  rightZero (SM1 (sfa :: Sing fa))
    | Refl <- --     (fa >>= UnM1Sym0 .@#@$$$ ConstSym1 ('M1 Mzero))
              -- :~: (fa >> Mzero)
              rightZeroLemma @a @b
                (singFun1 @(UnM1Sym0 :: M1 i c f _ ~> f _) sUnM1) sfa lemma
    , Refl <- rightZero @f @a @b sfa
    , Refl <- monadPlusMzero @f @b
    = Refl
    where
      lemma :: forall (z :: a). Sing z
            -> UnM1 Mzero :~: (Mzero :: f b)
      lemma _
        | Refl <- monadPlusMzero @f @b
        = Refl

  monadPlusMzero :: forall a. (Mzero :: M1 i c f a) :~: Empty
  monadPlusMzero = Refl

  monadPlusMplus :: forall a (x :: M1 i c f a) (y :: M1 i c f a).
                    Sing x -> Sing y
                 -> Mplus x y :~: (x <|> y)
  monadPlusMplus _ _ = Refl

rightZeroLemma
  :: forall a b
            (outer :: Type -> Type)
            (inner :: Type -> Type)
            (outInSym0 :: forall z. outer z ~> inner z)
            (ia :: inner a).
     VMonadPlus inner
  => (forall z. Sing (outInSym0 :: outer z ~> inner z))
  -> Sing ia
  -> (forall (z :: a). Sing z -> outInSym0 @@ Const Mzero z
                             :~: (Const Mzero z :: inner b))
  ->     (ia >>= outInSym0 .@#@$$$ ConstSym1 Mzero)
     :~: (ia >> (Mzero :: inner b))
rightZeroLemma _ sia lemma
  | Refl <- monadSeq @inner @a @b sia sMzero
  = Refl @((>>=@#@$$) ia)
      `apply` funExt @a @(inner b)
                     @(outInSym0 .@#@$$$ ConstSym1 Mzero)
                     @(ConstSym1 (Mzero :: inner b))
                     lemma

instance (VMonadPlus f, VMonadPlus g) => VMonadPlus (f :*: g) where
  leftZero :: forall a b (h :: a ~> (f :*: g) b).
              Sing h -> (Mzero >>= h) :~: Mzero
  leftZero sf
    | Refl <- leftZero @f (singFun1 @FstPSym0 sFstP %.$$$ sf)
    , Refl <- leftZero @g (singFun1 @SndPSym0 sSndP %.$$$ sf)
    , Refl <- monadPlusMzero @f @a
    , Refl <- monadPlusMzero @f @b
    , Refl <- monadPlusMzero @g @a
    , Refl <- monadPlusMzero @g @b
    = Refl

  rightZero :: forall a b (x :: (f :*: g) a). Sing x
            -> (x >> Mzero) :~: (Mzero :: (f :*: g) b)
  rightZero ((sfa :: Sing fa) :%*: (sga :: Sing ga))
    | Refl <- --     (fa >>= FstPSym0 .@#@$$$ ConstSym1 ((Mzero :: f b) ':*: (Mzero :: g b)))
              -- :~: (fa >> (Mzero :: f b))
              rightZeroLemma @a @b
                (singFun1 @(FstPSym0 :: (f :*: g) _ ~> f _) sFstP) sfa fstPLemma
    , Refl <- --     (ga >>= SndPSym0 .@#@$$$ ConstSym1 ((Mzero :: f b) ':*: (Mzero :: g b)))
              -- :~: (ga >> (Mzero :: g b))
              rightZeroLemma @a @b
                (singFun1 @(SndPSym0 :: (f :*: g) _ ~> g _) sSndP) sga sndPLemma
    , Refl <- rightZero @f @a @b sfa
    , Refl <- rightZero @g @a @b sga
    , Refl <- monadPlusMzero @f @b
    , Refl <- monadPlusMzero @g @b
    = Refl
    where
      fstPLemma :: forall (z :: a). Sing z
                -> FstP Mzero :~: (Mzero :: f b)
      fstPLemma _
        | Refl <- monadPlusMzero @f @b
        = Refl

      sndPLemma :: forall (z :: a). Sing z
                -> SndP Mzero :~: (Mzero :: g b)
      sndPLemma _
        | Refl <- monadPlusMzero @g @b
        = Refl

  monadPlusMzero :: forall a. (Mzero :: (f :*: g) a) :~: Empty
  monadPlusMzero = Refl

  monadPlusMplus :: forall a (x :: (f :*: g) a) (y :: (f :*: g) a).
                    Sing x -> Sing y
                 -> Mplus x y :~: (x <|> y)
  monadPlusMplus _ _ = Refl

instance VMonadPlus f => VMonadPlus (Rec1 f) where
  leftZero :: forall a b (g :: a ~> Rec1 f b).
              Sing g -> (Mzero >>= g) :~: Mzero
  leftZero sf
    | Refl <- leftZero @f (singFun1 @UnRec1Sym0 sUnRec1 %.$$$ sf)
    , Refl <- monadPlusMzero @f @a
    , Refl <- monadPlusMzero @f @b
    = Refl

  rightZero :: forall a b (x :: Rec1 f a). Sing x
            -> (x >> Mzero) :~: (Mzero :: Rec1 f b)
  rightZero (SRec1 (sfa :: Sing fa))
    | Refl <- --     (fa >>= UnRec1Sym0 .@#@$$$ ConstSym1 ('Rec1 Mzero))
              -- :~: (fa >> Mzero)
              rightZeroLemma @a @b
                (singFun1 @UnRec1Sym0 sUnRec1) sfa lemma
    , Refl <- rightZero @f @a @b sfa
    , Refl <- monadPlusMzero @f @b
    = Refl
    where
      lemma :: forall (z :: a). Sing z
            -> UnRec1 Mzero :~: (Mzero :: f b)
      lemma _
        | Refl <- monadPlusMzero @f @b
        = Refl

  monadPlusMzero :: forall a. (Mzero :: Rec1 f a) :~: Empty
  monadPlusMzero = Refl

  monadPlusMplus :: forall a (x :: Rec1 f a) (y :: Rec1 f a).
                    Sing x -> Sing y
                 -> Mplus x y :~: (x <|> y)
  monadPlusMplus _ _ = Refl
