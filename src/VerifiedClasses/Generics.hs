{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Generics where

import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Kind
import Data.Singletons.TH
import Data.Singletons.TH.Options

import GHC.Generics

import Prelude.Singletons

import VerifiedClasses.Axiom
import VerifiedClasses.TH.Generics

$(withOptions defaultOptions{genSingKindInsts = False} $
  genSingletons [''V1, ''U1, ''K1, ''M1, ''(:*:), ''(:+:), ''Par1, ''Rec1, ''(:.:)])

$(singletonsOnly [d|
  deriving instance Show (V1 p)
  deriving instance Show (U1 p)
  deriving instance Show c => Show (K1 i c p)
  deriving instance Show (f p) => Show (M1 i c f p)
  deriving instance (Show (f p), Show (g p)) => Show ((f :+: g) p)
  deriving instance (Show (f p), Show (g p)) => Show ((f :*: g) p)
  deriving instance Show p => Show (Par1 p)
  deriving instance Show (f p) => Show (Rec1 f p)
  deriving instance Show (f (g p)) => Show ((f :.: g) p)
  |])

class PGeneric a where
  type From (z :: a) :: Rep a x
  type To   (z :: Rep a x) :: a

class SGeneric a where
  sFrom :: forall x (z :: a).       Sing z -> Sing (From z :: Rep a x)
  sTo   :: forall x (r :: Rep a x). Sing r -> Sing (To   r :: a)

class (Generic a, PGeneric a, SGeneric a) => VGeneric a where
  sTof :: forall x (z :: a).       Sing z -> To (From z :: Rep a x) :~: z
  sFot :: forall x (r :: Rep a x). Sing r -> From (To r :: a)       :~: r

class PGeneric1 (f :: k -> Type) where
  type From1 (z :: f a)      :: Rep1 f a
  type To1   (z :: Rep1 f a) :: f a

class SGeneric1 (f :: k -> Type) where
  sFrom1 :: forall (a :: k) (z :: f a).      Sing z -> Sing (From1 z)
  sTo1   :: forall (a :: k) (r :: Rep1 f a). Sing r -> Sing (To1 r :: f a)

class (Generic1 f, PGeneric1 f, SGeneric1 f) => VGeneric1 (f :: k -> Type) where
  sTof1 :: forall (a :: k) (z :: f a).      Sing z -> To1 (From1 z)        :~: z
  sFot1 :: forall (a :: k) (r :: Rep1 f a). Sing r -> From1 (To1 r :: f a) :~: r

-----
-- Instances
-----

$(verifyGeneric0And1 ''Identity)

-----
-- Defunctionalization symbols
-----

{-
Note [Defunctionalizing From/To]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In an ideal world, defunctionalizing the promoted from/to methods of the
Generic class would be no different from defunctionalizing any other type
family. But alas, we do not live an ideal world. Trac #12564 prevents us
from writing Apply instances for FromSym0/ToSym0, since we would be writing
a type family instance where one of the kind arguments mentions a type family.
Ugh.

To work around this, we define FromSym0/ToSym0 by hand and avoid writing Apply
instances for them. Instead, we define axioms:

  applyFromSym0 :: Apply FromSym0 x :~: From x
  applyToSym0   :: Apply ToSym0   x :~: To   x

And use these in the appropriate places. It's annoying, but necessary (at least
for the time being.)
-}

data FromSym0 :: forall a x. a ~> Rep a x
data ToSym0   :: forall a x. Rep a x ~> a

applyFromSym0 :: forall a (z :: Type) (x :: a). Sing x
              -> (Apply FromSym0 x :: Rep a z) :~: From x
applyFromSym0 _ = axiom

sFrom' :: forall a (z :: Type) (x :: a).
          SGeneric a
       => Sing x -> Sing (FromSym0 @@ x :: Rep a z)
sFrom' sx | Refl <- applyFromSym0 @a @z sx
          = sFrom sx

applyToSym0 :: forall a (z :: Type) (x :: Rep a z). Sing x
            -> (Apply ToSym0 x :: a) :~: To x
applyToSym0 _ = axiom

sTo' :: forall a (z :: Type) (x :: Rep a z).
        SGeneric a
     => Sing x -> Sing (ToSym0 @@ x :: a)
sTo' sx | Refl <- applyToSym0 @a @z sx
        = sTo sx

data From1Sym0 :: forall k (f :: k -> Type) (a :: k). f a ~> Rep1 f a
data To1Sym0   :: forall k (f :: k -> Type) (a :: k). Rep1 f a ~> f a

applyFrom1Sym0 :: forall k (f :: k -> Type) (a :: k) (x :: f a). Sing x
               -> (Apply From1Sym0 x :: Rep1 f a) :~: From1 x
applyFrom1Sym0 _ = axiom

sFrom1' :: forall k (f :: k -> Type) (a :: k) (x :: f a).
           SGeneric1 f
        => Sing x -> Sing (From1Sym0 @@ x :: Rep1 f a)
sFrom1' sx | Refl <- applyFrom1Sym0 @k @f sx
           = sFrom1 sx

applyTo1Sym0 :: forall k (f :: k -> Type) (a :: k) (x :: Rep1 f a). Sing x
             -> (Apply To1Sym0 x :: f a) :~: To1 x
applyTo1Sym0 _ = axiom

sTo1' :: forall k (f :: k -> Type) (a :: k) (x :: Rep1 f a).
         SGeneric1 f
      => Sing x -> Sing (To1Sym0 @@ x :: f a)
sTo1' sx | Refl <- applyTo1Sym0 @k @f sx
         = sTo1 sx
