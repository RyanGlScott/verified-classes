{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.MonadZip (
    MonadZip(..), PMonadZip(..), SMonadZip(..), GMonadZip(..), VMonadZip(..)
  , genericMzip, GenericMzip, sGenericMzip
  , munzipLiftM', MunzipLiftM', sMunzipLiftM', MunzipLiftM'Sym0, MunzipLiftM'Sym1
  , defaultMonadZipNaturality, defaultMonadZipInformationPreservation
  , defaultMonadZipMunzip

    -- * Template Haskell
  , deriveMonadZipAll, deriveMonadZipSingletons, deriveMonadZipVerifiedOnly

    -- * Miscellaneous utilities
  , (***), type (***), (%***)
  , type (***@#@$), type (***@#@$$), type (***@#@$$$), type (***@#@$$$$)
  ) where

import Control.Monad.Singletons
import Control.Monad.Zip
import Control.Monad.Zip.Singletons

import Data.Kind
import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Applicative
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.Monad
import VerifiedClasses.TH.Deriving
import VerifiedClasses.Utils

genericMzip :: forall m a b. (Generic1 m, MonadZip (Rep1 m))
            => m a -> m b -> m (a, b)
genericMzip x y = to1 (mzip (from1 x) (from1 y))

type GenericMzip :: m a -> m b -> m (a, b)
type family GenericMzip x y where
  GenericMzip x y = To1 (Mzip (From1 x) (From1 y))

sGenericMzip :: forall m a b (x :: m a) (y :: m b).
                (SGeneric1 m, SMonadZip (Rep1 m))
             => Sing x -> Sing y -> Sing (GenericMzip x y)
sGenericMzip sx sy = sTo1 (sMzip (sFrom1 sx) (sFrom1 sy))

$(singletons [d|
  infixr 3 ***
  (***) :: (b -> c) -> (b' -> c') -> (b, b') -> (c, c')
  (f *** g) (x, y) = (f x, g y)

  -- TODO RGS: Explain why we need this
  munzipLiftM' :: Monad m => m (a, b) -> (m a, m b)
  munzipLiftM' mab = (liftM' fst mab, liftM' snd mab)
  |])

$(singletonsOnly [d|
  instance MonadZip U1 where
    mzip = liftM2 (,)
    munzip = munzipLiftM'

  instance MonadZip f => MonadZip (M1 i c f) where
    mzip (M1 fa) (M1 fb) = M1 (mzip fa fb)
    munzip = munzipLiftM'

  instance (MonadZip f, MonadZip g) => MonadZip (f :*: g) where
    mzip (x1 :*: y1) (x2 :*: y2) = mzip x1 x2 :*: mzip y1 y2
    munzip = munzipLiftM'

  instance MonadZip Par1 where
    mzip = liftM2 (,)
    munzip = munzipLiftM'

  instance MonadZip f => MonadZip (Rec1 f) where
    mzip (Rec1 fa) (Rec1 fb) = Rec1 (mzip fa fb)
    munzip = munzipLiftM'
  |])

class (MonadZip m, PMonadZip m, SMonadZip m, VMonad m) => VMonadZip m where
  monadZipNaturality
    :: forall a b c d (f :: a ~> c) (g :: b ~> d)
              (ma :: m a) (mb :: m b).
       Sing f -> Sing g -> Sing ma -> Sing mb
    -> LiftM' (f ***@#@$$$ g) (Mzip ma mb) :~: Mzip (LiftM' f ma) (LiftM' g mb)

  monadZipInformationPreservation
    :: forall a b (ma :: m a) (mb :: m b).
       Sing ma -> Sing mb
    -> LiftM' (ConstSym1 '()) ma :~: LiftM' (ConstSym1 '()) mb
    -> Munzip (Mzip ma mb) :~: '(ma, mb)

  monadZipMunzip
    :: forall a b (mab :: m (a, b)).
       Sing mab
    -> Munzip mab :~: '(LiftM' FstSym0 mab, LiftM' SndSym0 mab)

defaultMonadZipNaturality :: forall m a b c d (f :: a ~> c) (g :: b ~> d)
                                    (ma :: m a) (mb :: m b).
                             (VGeneric1 m, VMonadZip (Rep1 m), GMonadZip m)
                          => Sing f -> Sing g -> Sing ma -> Sing mb
                          ->     LiftM' (f ***@#@$$$ g) (Mzip ma mb)
                             :~: Mzip (LiftM' f ma) (LiftM' g mb)
defaultMonadZipNaturality sf sg sma smb
  {-
  (1) mzip ma mb >>= return . (f *** g)                                                   [assumption]
      to1 (from1 (to1 (mzip (from1 ma) (from1 mb))) >>= from1 . return . (f *** g))       [genericPureC, funExt]
  (2) to1 (from1 (to1 (mzip (from1 ma) (from1 mb))) >>= from1 . to1 . return . (f *** g)) [fot1]
  (3) to1 (mzip (from1 ma) (from1 mb) >>= from1 . to1 . return . (f *** g))               [fot1, funExt]
  (4) to1 (mzip (from1 ma) (from1 mb) >>= return . (f *** g))                             [def]
      to1 (liftM (f *** g) (mzip (from1 ma) (from1 mb)))                                  [mzNat]
  (5) to1 (mzip (liftM f (from1 ma)) (liftM g (from1 mb)))                                [def]
      to1 (mzip (from1 ma >>= return . f) (from1 mb >>= return . g))                      [fot1 (2)]
  (6) to1 (mzip (from1 (to1 (from1 ma >>= return . f)))
                (from1 (to1 (from1 mb >>= return . g))))                                  [fot1 (2), funExt]
  (7) to1 (mzip (from1 (to1 (from1 ma >>= from1 . to1 . return . f)))
                (from1 (to1 (from1 mb >>= from1 . to1 . return . g))))                    [genericPureC (2), funExt]
      to1 (mzip (from1 (to1 (from1 ma >>= from1 . return . f)))
                (from1 (to1 (from1 mb >>= from1 . return . g))))                          [assumption]
      mzip (ma >>= return . f) (mb >>= return . g)
  -}
  | -- (1)
    Refl <- genericMzipC sma smb
  , Refl <- genericBindC (sMzip sma smb) (sReturnFun %.$$$ sFSplitG)
  , Refl <- genericPureCLemma @m sFSplitG sMzipAB
    -- (2)
  , Refl <- sFot1 @Type @m sMzipAB
    -- (3)
  , Refl <- fot1Lemma @m sFSplitG sMzipAB
    -- (4)
  , Refl <- monadZipNaturality sf sg sf1ma sf1mb
    -- (5)
  , Refl <- sFot1 @Type @m (sf1ma %>>= sFrom1Fun %.$$$ sReturnFun %.$$$ sf)
  , Refl <- sFot1 @Type @m (sf1mb %>>= sFrom1Fun %.$$$ sReturnFun %.$$$ sg)
    -- (6)
  , Refl <- fot1Lemma @m sf sf1ma
  , Refl <- fot1Lemma @m sg sf1mb
    -- (7)
  , Refl <- genericPureCLemma @m sf sf1ma
  , Refl <- genericPureCLemma @m sg sf1mb
  , Refl <- genericBindC sma (sReturnFun %.$$$ sf)
  , Refl <- genericBindC smb (sReturnFun %.$$$ sg)
  , Refl <- genericMzipC (sLiftM' sf sma) (sLiftM' sg smb)
  = Refl
  where
    sf1ma :: Sing (From1 ma)
    sf1ma = sFrom1 sma

    sf1mb :: Sing (From1 mb)
    sf1mb = sFrom1 smb

    sMzipAB :: Sing (Mzip (From1 ma) (From1 mb))
    sMzipAB = sMzip sf1ma sf1mb

    sFSplitG :: Sing (f ***@#@$$$ g)
    sFSplitG = singFun1 (sf %*** sg)

    sReturnFun :: forall ab. Sing (ReturnSym0 :: ab ~> m ab)
    sReturnFun = singFun1 sReturn

    sFrom1Fun :: forall ab. Sing (From1Sym0 :: m ab ~> Rep1 m ab)
    sFrom1Fun = singFun1 sFrom1'

defaultMonadZipInformationPreservation
  :: forall m a b (ma :: m a) (mb :: m b).
     (VGeneric1 m, VMonadZip (Rep1 m), GMonadZip m)
  => Sing ma -> Sing mb
  -> LiftM' (ConstSym1 '()) ma :~: LiftM' (ConstSym1 '()) mb
  -> Munzip (Mzip ma mb) :~: '(ma, mb)
defaultMonadZipInformationPreservation sma smb r
  {-
  (This shows the part of the proof for ma. The corresponding proof for mb is analogous.)

  (1) mzip ma mb >>= return . fst                                             [assumption]
      to1 (from1 (to1 (mzip (from1 ma) (from1 mb))) >>= from1 . return . fst) [fot1]
  (2) to1 (mzip (from1 ma) (from1 mb) >>= from1 . return . fst)               [genericPureC, funExt]
  (3) to1 (mzip (from1 ma) (from1 mb) >>= from1 . to1 . return . fst)         [fot1, funExt]
  (4) to1 (mzip (from1 ma) (from1 mb) >>= return . fst)                       [mzInfoPres, ihLemma]
  (5) to1 (from1 ma)                                                          [tof1]
      ma
  -}
  | -- (1)
    Refl <- genericMzipC sma smb
  , Refl <- genericBindC sMzipAB (sReturnFun %.$$$ sFstFun)
  , Refl <- genericBindC sMzipAB (sReturnFun %.$$$ sSndFun)
  , Refl <- sFot1 @Type @m sMzipF1AB
    -- (2)
  , Refl <- genericPureCLemma @m sFstFun sMzipF1AB
  , Refl <- genericPureCLemma @m sSndFun sMzipF1AB
    -- (3)
  , Refl <- fot1Lemma @m sFstFun sMzipF1AB
  , Refl <- fot1Lemma @m sSndFun sMzipF1AB
    -- (4)
  , Refl <- monadZipInformationPreservation sf1ma sf1mb ihLemma
    -- (5)
  , Refl <- sTof1 sma
  , Refl <- sTof1 smb

  , Refl <- munzipLiftM'C sMzipAB
  , Refl <- monadZipMunzip sMzipF1AB
  = Refl
  where
    sf1ma :: Sing (From1 ma)
    sf1ma = sFrom1 sma

    sf1mb :: Sing (From1 mb)
    sf1mb = sFrom1 smb

    sFstFun :: Sing FstSym0
    sFstFun = singFun1 sFst

    sSndFun :: Sing SndSym0
    sSndFun = singFun1 sSnd

    sMzipAB :: Sing (Mzip ma mb)
    sMzipAB = sMzip sma smb

    sMzipF1AB :: Sing (Mzip (From1 ma) (From1 mb))
    sMzipF1AB = sMzip sf1ma sf1mb

    sReturnFun :: forall ab m'. SMonad m' => Sing (ReturnSym0 :: ab ~> m' ab)
    sReturnFun = singFun1 sReturn

    sConstUnit :: Sing (ConstSym1 '())
    sConstUnit = singFun1 (sConst STuple0)

    {-
    (1) liftM (const ()) (from1 ma)                                = liftM (const ()) (from1 mb)                                [def]
        from1 ma >>= return . const ()                             = from1 mb >>= return . const ()                             [fot1]
    (2) from1 (to1 (from1 ma >>= return . const ()))               = from1 (to1 (from1 mb >>= return . const ()))               [fot1, funExt]
    (3) from1 (to1 (from1 ma >>= from1 . to1 . return . const ())) = from1 (to1 (from1 mb >>= from1 . to1 . return . const ())) [genericPureC, funExt]
    (4) from1 (to1 (from1 ma >>= from1 . return . const ()))       = from1 (to1 (from1 mb >>= from1 . return . const ()))       [assumption]
        from1 (ma >>= return . const ())                           = from1 (ma >>= return . const ())                           [def]
        from1 (liftM (const ()) ma)                                = from1 (liftM (const ()) mb)                                [assumption]
    -}
    ihLemma :: LiftM' (ConstSym1 '()) (From1 ma) :~: LiftM' (ConstSym1 '()) (From1 mb)
    ihLemma
      | -- (1)
        Refl <- sFot1 @Type @m (sf1ma %>>= sReturnFun %.$$$ sConstUnit)
      , Refl <- sFot1 @Type @m (sf1mb %>>= sReturnFun %.$$$ sConstUnit)
        -- (2)
      , Refl <- fot1Lemma @m sConstUnit sf1ma
      , Refl <- fot1Lemma @m sConstUnit sf1mb
        -- (3)
      , Refl <- genericPureCLemma @m sConstUnit sf1ma
      , Refl <- genericPureCLemma @m sConstUnit sf1mb
        -- (4)
      , Refl <- genericBindC sma (sReturnFun %.$$$ sConstUnit)
      , Refl <- genericBindC smb (sReturnFun %.$$$ sConstUnit)
      , Refl <- r
      = Refl

defaultMonadZipMunzip :: forall m a b (mab :: m (a, b)).
                         GMonadZip m
                      => Sing mab
                      -> Munzip mab :~: '(LiftM' FstSym0 mab, LiftM' SndSym0 mab)
defaultMonadZipMunzip = munzipLiftM'C

monadZipClass :: Class
monadZipClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod 'mzip   'genericMzip  TermLevel
                          , ClassMethod 'munzip 'munzipLiftM' TermLevel
                          ]
  }

pMonadZipClass :: Class
pMonadZipClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''Mzip   ''GenericMzip  (TypeLevel 2)
                          , ClassMethod ''Munzip ''MunzipLiftM' (TypeLevel 1)
                          ]
  }

sMonadZipClass :: Class
sMonadZipClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod 'sMzip   'sGenericMzip  TermLevel
                          , ClassMethod 'sMunzip 'sMunzipLiftM' TermLevel
                          ]
  }

vMonadZipClass :: Class
vMonadZipClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'monadZipNaturality              'defaultMonadZipNaturality              TermLevel
                          , ClassMethod 'monadZipInformationPreservation 'defaultMonadZipInformationPreservation TermLevel
                          , ClassMethod 'monadZipMunzip                  'defaultMonadZipMunzip                  TermLevel
                          ]
  }

deriveMonadZipAll :: Q TH.Type -> Q [Dec]
deriveMonadZipAll = deriveClasses [monadZipClass, pMonadZipClass, sMonadZipClass, gClass, vMonadZipClass]

deriveMonadZipSingletons :: Q TH.Type -> Q [Dec]
deriveMonadZipSingletons = deriveClasses [pMonadZipClass, sMonadZipClass, gClass, vMonadZipClass]

deriveMonadZipVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveMonadZipVerifiedOnly = deriveClass vMonadZipClass

class ( GMonad m
      , MonadZip m, PMonadZip m, SMonadZip m
      , MonadZip (Rep1 m), PMonadZip (Rep1 m), SMonadZip (Rep1 m) )
      => GMonadZip m where
  genericMzipC         :: forall a b (ma :: m a) (mb :: m b).
                          Sing ma -> Sing mb
                       -> Mzip ma mb :~: GenericMzip ma mb
  default genericMzipC :: forall a b (ma :: m a) (mb :: m b).
                          Mzip ma mb ~ GenericMzip ma mb
                       => Sing ma -> Sing mb
                       -> Mzip ma mb :~: GenericMzip ma mb
  genericMzipC _ _ = Refl

  munzipLiftM'C         :: forall a b (mab :: m (a, b)).
                           Sing mab
                        -> Munzip mab :~: '(LiftM' FstSym0 mab, LiftM' SndSym0 mab)
  default munzipLiftM'C :: forall a b (mab :: m (a, b)).
                           Munzip mab ~ '(LiftM' FstSym0 mab, LiftM' SndSym0 mab)
                        => Sing mab
                        -> Munzip mab :~: '(LiftM' FstSym0 mab, LiftM' SndSym0 mab)
  munzipLiftM'C _ = Refl

-- genericPureCLemma and fot1Lemma (below) are both used frequently in the
-- generic defaults for each MonadZip law.

genericPureCLemma :: forall (m :: Type -> Type) ab cd (j :: ab ~> cd) (r1m :: Rep1 m ab).
                     (GMonad m, VMonad (Rep1 m))
                  => Sing j -> Sing r1m
                  ->     (r1m >>= (From1Sym0 :: m cd ~> Rep1 m cd) .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
                     :~: (r1m >>= (From1Sym0 :: m cd ~> Rep1 m cd)                 .@#@$$$ ReturnSym0 .@#@$$$ j)
genericPureCLemma sj _
  = Refl @((>>=@#@$$) r1m)
      `apply` funExt @ab @(Rep1 m cd)
                     @((From1Sym0 :: m cd ~> Rep1 m cd) .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
                     @((From1Sym0 :: m cd ~> Rep1 m cd)                 .@#@$$$ ReturnSym0 .@#@$$$ j)
                     lemma
  where
    lemma :: forall (z :: ab). Sing z
          ->     (From1Sym0 @@ (To1Sym0 @@ (Return (j @@ z)) :: m cd))
             :~: (From1Sym0 @@ (Return (j @@ z) :: m cd))
    lemma sz | Refl <- applyTo1Sym0 @Type @m (sReturn sjz)
             , Refl <- genericPureC @m sjz
             , Refl <- returnPureC @m sjz
             , Refl <- monadReturn @(Rep1 m) sjz
             = Refl
      where
        sjz :: Sing (j @@ z)
        sjz = sj @@ sz

fot1Lemma :: forall (m :: Type -> Type) ab cd (j :: ab ~> cd) (r1m :: Rep1 m ab).
             (VGeneric1 m, SMonad (Rep1 m))
          => Sing j -> Sing r1m
          ->     (r1m >>= (From1Sym0 :: m cd ~> Rep1 m cd) .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
             :~: (r1m >>=                                                          ReturnSym0 .@#@$$$ j)
fot1Lemma sj _
  = Refl @((>>=@#@$$) r1m)
      `apply` funExt @ab @(Rep1 m cd)
                     @((From1Sym0 :: m cd ~> Rep1 m cd) .@#@$$$ To1Sym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
                     @(                                                         ReturnSym0 .@#@$$$ j)
                     lemma
  where
    lemma :: forall (z :: ab). Sing z
          ->     (From1Sym0 @@ (To1Sym0 @@ (Return (j @@ z)) :: m cd))
             :~: Return (j @@ z)
    lemma sz
      | Refl <- applyTo1Sym0   @Type @m sReturnJZ
      , Refl <- applyFrom1Sym0 @Type @m (sTo1 sReturnJZ)
      , Refl <- sFot1          @Type @m sReturnJZ
      = Refl
      where
        sReturnJZ :: Sing (Return (j @@ z) :: Rep1 m cd)
        sReturnJZ = sReturn (sj @@ sz)

instance VMonadZip U1 where
  monadZipNaturality _ _ _ _ = Refl
  monadZipInformationPreservation SU1 SU1 _ = Refl
  monadZipMunzip _ = Refl

instance VMonadZip f => VMonadZip (M1 i c' f) where
  monadZipNaturality sj sk (SM1 sfa) (SM1 sfb)
    | Refl <- monadZipNaturalityLemma sj sk
                (singFun1 @(UnM1Sym0 :: M1 i c' f _ ~> f _) sUnM1)
                sfa sfb unM1Lemma
    = Refl
  monadZipInformationPreservation (SM1 sfa) (SM1 sfb) Refl
    | (Refl, Refl) <- monadZipInformationPreservationLemma
                        (singFun1 @(UnM1Sym0 :: M1 i c' f _ ~> f _) sUnM1)
                        sfa sfb Refl unM1Lemma
    = Refl
  monadZipMunzip _ = Refl

unM1Lemma :: forall f ab cd (j :: ab ~> cd) (z :: ab).
             VMonad f
          => Sing j -> Sing z
          -> (Return (j @@ z) :: f cd) :~: UnM1 (Return (j @@ z))
unM1Lemma sj sx
  | Refl <- monadReturn @f (sj @@ sx)
  = Refl

-- A particular lemma that is reused in several naturality proofs.
monadZipNaturalityLemma
  :: forall a b c d
            (f :: a ~> c) (g :: b ~> d)
            (outer :: Type -> Type) (inner :: Type -> Type)
            (outInSym0 :: forall z. outer z ~> inner z)
            (ia :: inner a) (ib :: inner b).
     VMonadZip inner
  => Sing f -> Sing g
  -> (forall z. Sing (outInSym0 :: outer z ~> inner z))
  -> Sing ia -> Sing ib
  -> (forall ab cd (j :: ab ~> cd) (z :: ab).
           Sing j -> Sing z
        -> Return (j @@ z) :~: outInSym0 @@ (Return (j @@ z)))
  ->     (Mzip ia ib >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ (f ***@#@$$$ g))
     :~: (Mzip (ia >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ f)
               (ib >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ g))
monadZipNaturalityLemma sf sg sOIFun sia sib lemma
  | Refl <- helper (singFun1 @(f ***@#@$$$ g) (sf %*** sg)) (sMzip sia sib)
  , Refl <- monadZipNaturality sf sg sia sib
  , Refl <- helper sf sia
  , Refl <- helper sg sib
  = Refl
  where
    helper :: forall ab cd (j :: ab ~> cd) (iab :: inner ab).
              Sing j -> Sing iab
           ->     (iab >>= ReturnSym0 .@#@$$$ j)
              :~: (iab >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
    helper = lemmaHelper sOIFun lemma

-- A particular lemma that is reused in several information preservation proofs.
monadZipInformationPreservationLemma
  :: forall a b
            (outer :: Type -> Type) (inner :: Type -> Type)
            (outInSym0 :: forall z. outer z ~> inner z)
            (ia :: inner a) (ib :: inner b).
     VMonadZip inner
  => (forall z. Sing (outInSym0 :: outer z ~> inner z))
  -> Sing ia -> Sing ib
  ->     (ia >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ ConstSym1 '())
     :~: (ib >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ ConstSym1 '())
  -> (forall ab cd (j :: ab ~> cd) (z :: ab).
           Sing j -> Sing z
        -> Return (j @@ z) :~: outInSym0 @@ (Return (j @@ z)))
  -> ( (Mzip ia ib >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ FstSym0) :~: ia
     , (Mzip ia ib >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ SndSym0) :~: ib
     )
monadZipInformationPreservationLemma sOIFun sia sib Refl lemma
  | Refl <- helper sConstUnit sia
  , Refl <- helper sConstUnit sib
  , Refl <- helper (singFun1 @FstSym0 sFst) sMzipAB
  , Refl <- helper (singFun1 @SndSym0 sSnd) sMzipAB
  , Refl <- monadZipInformationPreservation sia sib Refl
  , Refl <- monadZipMunzip sMzipAB
  = (Refl, Refl)
  where
    sConstUnit :: Sing (ConstSym1 '())
    sConstUnit = singFun1 (sConst STuple0)

    sMzipAB :: Sing (Mzip ia ib)
    sMzipAB = sMzip sia sib

    helper :: forall ab cd (j :: ab ~> cd) (iab :: inner ab).
              Sing j -> Sing iab
           ->     (iab >>= ReturnSym0 .@#@$$$ j)
              :~: (iab >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
    helper = lemmaHelper sOIFun lemma

lemmaHelper :: forall ab cd (j :: ab ~> cd)
                      (outer :: Type -> Type) (inner :: Type -> Type)
                      (outInSym0 :: forall z. outer z ~> inner z)
                      (iab :: inner ab).
               (forall z. Sing (outInSym0 :: outer z ~> inner z))
            -> (forall ab' cd' (j' :: ab' ~> cd') (z :: ab').
                     Sing j' -> Sing z
                  -> Return (j' @@ z) :~: outInSym0 @@ (Return (j' @@ z)))
            -> Sing j -> Sing iab
            ->     (iab >>= ReturnSym0 .@#@$$$ j)
               :~: (iab >>= outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
lemmaHelper _ lemma sj _ =
  Refl @((>>=@#@$$) iab)
    `apply` funExt @ab @(inner cd)
                   @(ReturnSym0 .@#@$$$ j)
                   @(outInSym0 .@#@$$$ ReturnSym0 .@#@$$$ j)
                   (lemma sj)

instance (VMonadZip f, VMonadZip g) => VMonadZip (f :*: g) where
  monadZipNaturality sj sk (sfa :%*: sga) (sfb :%*: sgb)
    | Refl <- monadZipNaturalityLemma sj sk
                (singFun1 @(FstPSym0 :: (f :*: g) _ ~> f _) sFstP)
                sfa sfb fstPLemma
    , Refl <- monadZipNaturalityLemma sj sk
                (singFun1 @(SndPSym0 :: (f :*: g) _ ~> g _) sSndP)
                sga sgb sndPLemma
    = Refl
  monadZipInformationPreservation (sfa :%*: sga) (sfb :%*: sgb) Refl
    | (Refl, Refl) <- monadZipInformationPreservationLemma
                        (singFun1 @(FstPSym0 :: (f :*: g) _ ~> f _) sFstP)
                        sfa sfb Refl fstPLemma
    , (Refl, Refl) <- monadZipInformationPreservationLemma
                        (singFun1 @(SndPSym0 :: (f :*: g) _ ~> g _) sSndP)
                        sga sgb Refl sndPLemma
    = Refl
  monadZipMunzip _ = Refl

fstPLemma :: forall f ab cd (j :: ab ~> cd) (z :: ab).
             VMonad f
          => Sing j -> Sing z
          -> (Return (j @@ z) :: f cd) :~: FstP (Return (j @@ z))
fstPLemma sj sx
  | Refl <- monadReturn @f (sj @@ sx)
  = Refl

sndPLemma :: forall f ab cd (j :: ab ~> cd) (z :: ab).
             VMonad f
          => Sing j -> Sing z
          -> (Return (j @@ z) :: f cd) :~: SndP (Return (j @@ z))
sndPLemma sj sx
  | Refl <- monadReturn @f (sj @@ sx)
  = Refl

instance VMonadZip Par1 where
  monadZipNaturality _ _ SPar1{} SPar1{} = Refl
  monadZipInformationPreservation SPar1{} SPar1{} _ = Refl
  monadZipMunzip _ = Refl

instance VMonadZip f => VMonadZip (Rec1 f) where
  monadZipNaturality sj sk (SRec1 sfa) (SRec1 sfb)
    | Refl <- monadZipNaturalityLemma sj sk
                (singFun1 @UnRec1Sym0 sUnRec1) sfa sfb unRec1Lemma
    = Refl
  monadZipInformationPreservation sRec1a@(SRec1 sfa) sRec1b@(SRec1 sfb) Refl
    | (Refl, Refl) <- monadZipInformationPreservationLemma
                        (singFun1 @UnRec1Sym0 sUnRec1)
                        sfa sfb Refl unRec1Lemma
    , Refl <- monadZipMunzip (sMzip sRec1a sRec1b)
    = Refl
  monadZipMunzip _ = Refl

unRec1Lemma :: forall f ab cd (j :: ab ~> cd) (z :: ab).
               VMonad f
            => Sing j -> Sing z
            -> (Return (j @@ z) :: f cd) :~: UnRec1 (Return (j @@ z))
unRec1Lemma sj sx
  | Refl <- monadReturn @f (sj @@ sx)
  = Refl
