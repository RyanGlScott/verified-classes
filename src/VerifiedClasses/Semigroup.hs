{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Semigroup (
    Semigroup(..), PSemigroup(..), SSemigroup(..), GSemigroup(..), VSemigroup(..)
  , genericAppend, GenericAppend, sGenericAppend
  , defaultSemigroupAssociative

    -- * Template Haskell
  , deriveSemigroupAll, deriveSemigroupSingletons, deriveSemigroupVerifiedOnly
  ) where

import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Generics
import VerifiedClasses.TH.Deriving

infixr 6 `genericAppend`
genericAppend :: forall a. (Generic a, Semigroup (Rep a ()))
              => a -> a -> a
genericAppend x y = to (from @a @() x <> from y)

infixr 6 `GenericAppend`
type GenericAppend :: a -> a -> a
type family x `GenericAppend` y where
  (x :: a) `GenericAppend` y = To ((From x :: Rep a ()) <> From y)

infixr 6 `sGenericAppend`
sGenericAppend :: forall a (x :: a) (y :: a).
                  (SGeneric a, SSemigroup (Rep a ()))
               => Sing x -> Sing y -> Sing (x `GenericAppend` y)
sx `sGenericAppend` sy = sTo (sFrom @a @() @x sx %<> sFrom sy)

$(singletonsOnly [d|
  instance Semigroup (V1 p) where
    v <> _ = v

  instance Semigroup (U1 p) where
    _ <> _ = U1

  instance Semigroup c => Semigroup (K1 i c p) where
    K1 x <> K1 y = K1 (x <> y)

  instance Semigroup (f p) => Semigroup (M1 i c f p) where
    M1 x <> M1 y = M1 (x <> y)

  instance (Semigroup (f p), Semigroup (g p)) => Semigroup ((f :*: g) p) where
    (x1 :*: y1) <> (x2 :*: y2) = (x1 <> x2) :*: (y1 <> y2)
  |])

class (Semigroup a, PSemigroup a, SSemigroup a) => VSemigroup a where
  semigroupAssociative :: forall (x :: a) (y :: a) (z :: a).
                          Sing x -> Sing y -> Sing z
                       -> (x <> (y <> z)) :~: ((x <> y) <> z)

defaultSemigroupAssociative :: forall a (x :: a) (y :: a) (z :: a).
                               (VGeneric a, VSemigroup (Rep a ()), GSemigroup a)
                            => Sing x -> Sing y -> Sing z
                            -> (x <> (y <> z)) :~: ((x <> y) <> z)
defaultSemigroupAssociative sx sy sz
  | Refl <- genericAppendC sx sy
  , Refl <- genericAppendC sy sz
  , Refl <- genericAppendC sx (sy %<> sz)
  , Refl <- genericAppendC (sx %<> sy) sz
  , Refl <- sFot @a (sfx %<> sfy)
  , Refl <- sFot @a (sfy %<> sfz)
  , Refl <- semigroupAssociative sfx sfy sfz
  = Refl
  where
    sfx :: Sing (From x :: Rep a ())
    sfx = sFrom @a @() @x sx

    sfy :: Sing (From y :: Rep a ())
    sfy = sFrom @a @() @y sy

    sfz :: Sing (From z :: Rep a ())
    sfz = sFrom @a @() @z sz

semigroupClass :: Class
semigroupClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod '(<>) 'genericAppend TermLevel
                          ]
  }

pSemigroupClass :: Class
pSemigroupClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''(<>) ''GenericAppend (TypeLevel 2)
                          ]
  }

sSemigroupClass :: Class
sSemigroupClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod '(%<>) 'sGenericAppend TermLevel
                          ]
  }

vSemigroupClass :: Class
vSemigroupClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'semigroupAssociative 'defaultSemigroupAssociative TermLevel
                          ]
  }

deriveSemigroupAll :: Q TH.Type -> Q [Dec]
deriveSemigroupAll = deriveClasses [semigroupClass, pSemigroupClass, sSemigroupClass, gClass, vSemigroupClass]

deriveSemigroupSingletons :: Q TH.Type -> Q [Dec]
deriveSemigroupSingletons = deriveClasses [pSemigroupClass, sSemigroupClass, gClass, vSemigroupClass]

deriveSemigroupVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveSemigroupVerifiedOnly = deriveClass vSemigroupClass

class ( Semigroup a, PSemigroup a, SSemigroup a
      , Semigroup (Rep a ()), PSemigroup (Rep a ()), SSemigroup (Rep a ())
      , Generic a, PGeneric a, SGeneric a )
      => GSemigroup a where
  genericAppendC :: forall (x :: a) (y :: a).
                    Sing x -> Sing y
                 -> (x <> y) :~: (x `GenericAppend` y)
  default genericAppendC :: forall (x :: a) (y :: a).
                            (x <> y) ~ (x `GenericAppend` y)
                         => Sing x -> Sing y
                         -> (x <> y) :~: (x `GenericAppend` y)
  genericAppendC _ _ = Refl

instance VSemigroup (V1 p) where
  semigroupAssociative _ _ _ = Refl

instance VSemigroup (U1 p) where
  semigroupAssociative _ _ _ = Refl

instance VSemigroup c => VSemigroup (K1 i c p) where
  semigroupAssociative (SK1 c1) (SK1 c2) (SK1 c3)
    | Refl <- semigroupAssociative c1 c2 c3 = Refl

instance VSemigroup (f p) => VSemigroup (M1 i c f p) where
  semigroupAssociative (SM1 c1) (SM1 c2) (SM1 c3)
    | Refl <- semigroupAssociative c1 c2 c3 = Refl

instance (VSemigroup (f p), VSemigroup (g p)) => VSemigroup ((f :*: g) p) where
  semigroupAssociative (x1 :%*: y1) (x2 :%*: y2) (x3 :%*: y3)
    | Refl <- semigroupAssociative x1 x2 x3
    , Refl <- semigroupAssociative y1 y2 y3
    = Refl
