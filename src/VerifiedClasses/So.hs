{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}

module VerifiedClasses.So where

import Control.DeepSeq
import Data.Kind
import Data.Type.Bool
import Data.Type.Equality
import Prelude.Singletons (Sing, SBool(..))

data So :: Bool -> Type where
  Oh :: So True

instance NFData (So b) where
  rnf Oh = ()

soNotSo :: So b -> So (Not b) -> void
soNotSo Oh x = case x of {}

contra :: Sing p -> Sing q -> (So p -> So q) -> So (Not q) -> So (Not p)
contra sp sq pq Oh
  | SFalse <- sq
  = case sp of
      SFalse -> Oh
      STrue  -> soNotSo Oh (pq Oh)

soToEq :: So b -> b :~: True
soToEq Oh = Refl

soNotToEq :: Sing b -> So (Not b) -> b :~: False
soNotToEq sb Oh | SFalse <- sb = Refl
