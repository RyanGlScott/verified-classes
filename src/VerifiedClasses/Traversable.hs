{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeAbstractions #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module VerifiedClasses.Traversable (
    Traversable(..), PTraversable(..), STraversable(..), GTraversable(..), VTraversable(..)
  , genericTraverse, GenericTraverse, sGenericTraverse
  , defaultTraversableNaturality, defaultTraversableIdentity, defaultTraversableComposition

    -- * Template Haskell
  , deriveTraversableAll, deriveTraversableSingletons, deriveTraversableVerifiedOnly
  ) where

import Data.Function.Singletons
import Data.Functor.Compose
import Data.Functor.Identity
import Data.Functor.Identity.Singletons
import Data.Kind
import Data.Singletons.TH

import GHC.Generics

import Language.Haskell.TH (Q, Dec)
import qualified Language.Haskell.TH as TH (Type)

import Prelude.Singletons

import VerifiedClasses.Applicative
import VerifiedClasses.Applicative.Transformer
import VerifiedClasses.Compose
import VerifiedClasses.Foldable ()
import VerifiedClasses.Functor
import VerifiedClasses.FunExt
import VerifiedClasses.Generics
import VerifiedClasses.TH.Deriving
import VerifiedClasses.Utils

genericTraverse :: forall t f a b. (Generic1 t, Traversable (Rep1 t), Applicative f)
                => (a -> f b) -> t a -> f (t b)
genericTraverse g x = to1 `fmap` traverse g (from1 x)

type GenericTraverse :: (a ~> f b) -> t a -> f (t b)
type family GenericTraverse g x where
  GenericTraverse g x = To1Sym0 `Fmap` Traverse g (From1 x)

sGenericTraverse :: forall t f a b (g :: a ~> f b) (x :: t a).
                    SApplicative f
                 => (SGeneric1 t, STraversable (Rep1 t))
                 => Sing g -> Sing x -> Sing (GenericTraverse g x)
sGenericTraverse sg sx = sFmap (singFun1 @To1Sym0 sTo1') (sTraverse sg (sFrom1 sx))

$(singletonsOnly [d|
  instance Traversable V1 where
    traverse _ x = pure (case x of {})

  instance Traversable U1 where
    traverse _ _ = pure U1

  instance Traversable (K1 i c) where
    traverse _ (K1 x) = pure (K1 x)

  instance Traversable f => Traversable (M1 i c f) where
    traverse f (M1 x) = M1 `fmap` traverse f x

  instance (Traversable f, Traversable g) => Traversable (f :*: g) where
    traverse f (x :*: y) = (:*:) `fmap` traverse f x <*> traverse f y

  instance (Traversable f, Traversable g) => Traversable (f :+: g) where
    traverse f (L1 x) = L1 `fmap` traverse f x
    traverse f (R1 y) = R1 `fmap` traverse f y

  instance Traversable Par1 where
    traverse f (Par1 x) = Par1 `fmap` f x

  instance Traversable f => Traversable (Rec1 f) where
    traverse f (Rec1 x) = Rec1 `fmap` traverse f x

  instance (Traversable f, Traversable g) => Traversable ((f :: Type -> Type) :.: g) where
    traverse f (Comp1 x) = Comp1 `fmap` traverse (traverse f) x
  |])

class (Traversable t, PTraversable t, STraversable t, VFunctor t) => VTraversable t where
  traversableNaturality
    :: forall a b (f :: Type -> Type) (g :: Type -> Type) (h :: a ~> f b)
              (transform :: forall z. f z ~> g z) (x :: t a).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: f z ~> g z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x

  traversableIdentity :: forall a (x :: t a).
                         Sing x
                      -> Traverse IdentitySym0 x :~: 'Identity x

  traversableComposition :: forall a b c (f :: Type -> Type) (g :: Type -> Type)
                                   (h :: a ~> f b) (i :: b ~> g c) (x :: t a).
                            (VApplicative f, VApplicative g)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))

defaultTraversableNaturality
  :: forall t a b (f :: Type -> Type) (g :: Type -> Type) (h :: a ~> f b)
            (transform :: forall z. f z ~> g z) (x :: t a).
     (VGeneric1 t, VTraversable (Rep1 t), GTraversable t)
  => VApplicativeTransformer transform
  => (forall z. Sing (transform :: f z ~> g z)) -> Sing h -> Sing x
  -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
defaultTraversableNaturality strans sh sx
  | Refl <- genericTraverseC sh sx
  , Refl <- genericTraverseC (singFun1 @(transform .@#@$$$ h) (strans %. sh)) sx
  , Refl <- traversableNaturalityPush strans
              (singFun1 @(To1Sym0 :: Rep1 t b ~> t b) sTo1') (sTraverse sh sf1x)
  , Refl <- traversableNaturality strans sh sf1x
  = Refl
  where
    sf1x :: Sing (From1 x)
    sf1x = sFrom1 sx

defaultTraversableIdentity :: forall t a (x :: t a).
                              (VGeneric1 t, VTraversable (Rep1 t), GTraversable t)
                           => Sing x
                           -> Traverse IdentitySym0 x :~: 'Identity x
defaultTraversableIdentity sx
  | Refl <- genericTraverseC (sing @IdentitySym0) sx
  , Refl <- traversableIdentity sf1x
  , Refl <- applyTo1Sym0 @Type @t sf1x
  , Refl <- sTof1 sx
  = Refl
  where
    sf1x :: Sing (From1 x)
    sf1x = sFrom1 sx

defaultTraversableComposition :: forall t a b c (f :: Type -> Type) (g :: Type -> Type)
                                        (h :: a ~> f b) (i :: b ~> g c) (x :: t a).
                                 (VGeneric1 t, VTraversable (Rep1 t), GTraversable t)
                              => (VApplicative f, VApplicative g)
                              => Sing h -> Sing i -> Sing x
                              ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                                 :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
defaultTraversableComposition sh si sx
{-
(1) traverse (Compose . fmap i . h) x                                                [assumption]
    fmap to1 (traverse (Compose . fmap i . h) (from1 x))                             [travComp]
(2) fmap to1 (Compose (fmap (traverse i) (traverse h (from1 x))))                    [def (Functor instance for Compose)]
    Compose (fmap (fmap to1) (fmap (traverse i) (traverse h (from1 x))))             [fmapId]
(3) Compose (fmap (fmap to1) (fmap (traverse i) (fmap id (traverse h (from1 x)))))   [fmapComp]
(4) Compose (fmap (fmap to1) (fmap (traverse i . id) (traverse h (from1 x))))        [fmapComp]
(5) Compose (fmap ( fmap to1 . traverse i . id)           (traverse h (from1 x)))    [fot1, funExt]
(6) Compose (fmap ( fmap to1 . traverse i . from1  . to1) (traverse h (from1 x)))    [def, funExt]
(7) Compose (fmap ((fmap to1 . traverse i . from1) . to1) (traverse h (from1 x)))    [fmapComp]
    Compose (fmap (fmap to1 . traverse i . from1) (fmap to1 (traverse h (from1 x)))) [assumption]
(8) Compose (fmap (fmap to1 . traverse i . from1) (traverse h x)                     [assumption, funExt]
    Compose (fmap (traverse i) (traverse h x)
-}
  | -- (1)
    Refl <- genericTraverseC (singFun1 @ComposeSym0 SCompose
                               %.$$$ singFun1 @(FmapSym1 i) (sFmap si)
                               %.$$$ sh) sx
  , Refl <- traversableComposition @(Rep1 t) sh si sf1x
    -- (2)
  , Refl <- fmapId @f sTraverseHf1x
    -- (3)
  , Refl <- fmapCompose @f sTraverseIFun sIdFun sTraverseHf1x
    -- (4)
  , Refl <- fmapCompose @f (singFun1 @(FmapSym1 (To1Sym0 :: Rep1 t c ~> t c)) (sFmap sTo1Fun))
                           (singFun1 @(TraverseSym1 i .@#@$$$ IdSym0) (sTraverseIFun %. sIdFun))
                           sTraverseHf1x
    -- (5)
  , Refl <- --     Fmap (FmapSym1 To1Sym0 .@#@$$$ TraverseSym1 i .@#@$$$ IdSym0)                    (Traverse h (From1 x))
            -- :~: Fmap (FmapSym1 To1Sym0 .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0 .@#@$$$ To1Sym0) (Traverse h (From1 x))
            Refl @FmapSym0
              `apply` (Refl @((.@#@$$) (FmapSym1 (To1Sym0 :: Rep1 t c ~> t c)))
                        `apply` (Refl @((.@#@$$) (TraverseSym1 i))
                                  `apply` funExt @(Rep1 t b) @(Rep1 t b)
                                                 @(From1Sym0 .@#@$$$ (To1Sym0 :: Rep1 t b ~> t b)) @IdSym0
                                                 lemma1))
              `apply` Refl @(Traverse h (From1 x))
    -- (6)
  , Refl <- --     Fmap ( FmapSym1 To1Sym0 .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0  .@#@$$$ To1Sym0) (Traverse h (From1 x))
            -- :~: Fmap ((FmapSym1 To1Sym0 .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0) .@#@$$$ To1Sym0) (Traverse h (From1 x))
            Refl @FmapSym0
              `apply` funExt @(Rep1 t b) @(g (t c))
                             @( FmapSym1 (To1Sym0 :: Rep1 t c ~> t c) .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0  .@#@$$$ (To1Sym0 :: Rep1 t b ~> t b))
                             @((FmapSym1 (To1Sym0 :: Rep1 t c ~> t c) .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0) .@#@$$$ (To1Sym0 :: Rep1 t b ~> t b))
                             (const Refl)
              `apply` Refl @(Traverse h (From1 x))
    -- (7)
  , Refl <- fmapCompose @f (singFun1 @(FmapSym1 (To1Sym0 :: Rep1 t c ~> t c) .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0)
                                     (singFun1 @(FmapSym1 (To1Sym0 :: Rep1 t c ~> t c)) (sFmap sTo1Fun) %. singFun1 @(TraverseSym1 i .@#@$$$ From1Sym0) (sTraverseIFun %. sFrom1Fun)))
                           sTo1Fun sTraverseHf1x
    -- (8)
  , Refl <- genericTraverseC sh sx
  , Refl <- --     Fmap (FmapSym1 (To1Sym0 :: Rep1 t c ~> t c) .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0) (Traverse h x)
            -- :~: Fmap (TraverseSym1 i) (Traverse h x)
            Refl @FmapSym0
              `apply` funExt @(t b) @(g (t c))
                             @(FmapSym1 (To1Sym0 :: Rep1 t c ~> t c) .@#@$$$ TraverseSym1 i .@#@$$$ From1Sym0)
                             @(TraverseSym1 i)
                             lemma2
              `apply` Refl @(Traverse h x)
  = Refl
  where
    sf1x :: Sing (From1 x)
    sf1x = sFrom1 sx

    sTraverseHf1x :: Sing (Traverse h (From1 x))
    sTraverseHf1x = sTraverse sh sf1x

    sTraverseIFun :: Sing (TraverseSym1 i :: Rep1 t b ~> g (Rep1 t c))
    sTraverseIFun = singFun1 (sTraverse si)

    sIdFun :: Sing IdSym0
    sIdFun = singFun1 sId

    sFrom1Fun :: forall z. Sing (From1Sym0 :: t z ~> Rep1 t z)
    sFrom1Fun = singFun1 sFrom1'

    sTo1Fun :: forall z. Sing (To1Sym0 :: Rep1 t z ~> t z)
    sTo1Fun = singFun1 sTo1'

    lemma1 :: forall (r :: Rep1 t b). Sing r -> From1Sym0 @@ (To1Sym0 @@ r :: t b) :~: r
    lemma1 sr | Refl <- applyTo1Sym0   @Type @t sr
              , Refl <- applyFrom1Sym0 @Type @t (sTo1 sr)
              = sFot1                  @Type @t sr

    lemma2 :: forall (z :: t b). Sing z
           -> Fmap (To1Sym0 :: Rep1 t c ~> t c) (Traverse i (From1Sym0 @@ z)) :~: Traverse i z
    lemma2 sz | Refl <- applyFrom1Sym0 sz
              , Refl <- genericTraverseC si sz
              = Refl

traversableClass :: Class
traversableClass = Class
  { clsSort             = Original
  , clsMethsAndDefaults = [ ClassMethod 'traverse 'genericTraverse TermLevel
                          ]
  }

pTraversableClass :: Class
pTraversableClass = Class
  { clsSort             = Promoted
  , clsMethsAndDefaults = [ ClassMethod ''Traverse ''GenericTraverse (TypeLevel 2)
                          ]
  }

sTraversableClass :: Class
sTraversableClass = Class
  { clsSort             = Singled
  , clsMethsAndDefaults = [ ClassMethod 'sTraverse 'sGenericTraverse TermLevel
                          ]
  }

vTraversableClass :: Class
vTraversableClass = Class
  { clsSort             = Verified
  , clsMethsAndDefaults = [ ClassMethod 'traversableNaturality  'defaultTraversableNaturality  TermLevel
                          , ClassMethod 'traversableIdentity    'defaultTraversableIdentity    TermLevel
                          , ClassMethod 'traversableComposition 'defaultTraversableComposition TermLevel
                          ]
  }

deriveTraversableAll :: Q TH.Type -> Q [Dec]
deriveTraversableAll = deriveClasses [traversableClass, pTraversableClass, sTraversableClass, gClass, vTraversableClass]

deriveTraversableSingletons :: Q TH.Type -> Q [Dec]
deriveTraversableSingletons = deriveClasses [pTraversableClass, sTraversableClass, gClass, vTraversableClass]

deriveTraversableVerifiedOnly :: Q TH.Type -> Q [Dec]
deriveTraversableVerifiedOnly = deriveClass vTraversableClass

class ( GFunctor t
      , Traversable t, PTraversable t, STraversable t
      , Traversable (Rep1 t), PTraversable (Rep1 t), STraversable (Rep1 t) )
      => GTraversable t where
  genericTraverseC         :: forall (f :: Type -> Type) a b (g :: a ~> f b) (x :: t a).
                              VApplicative f
                           => Sing g -> Sing x
                           -> Traverse g x :~: GenericTraverse g x
  default genericTraverseC :: forall (f :: Type -> Type) a b (g :: a ~> f b) (x :: t a).
                              VApplicative f
                           => Traverse g x ~ GenericTraverse g x
                           => Sing g -> Sing x
                           -> Traverse g x :~: GenericTraverse g x
  genericTraverseC _ _ = Refl

instance VTraversable V1 where
  traversableNaturality _ _ x = case x of {}
  traversableIdentity x = case x of {}
  traversableComposition _ _ x = case x of {}

instance VTraversable U1 where
  traversableNaturality
    :: forall a b (f :: Type -> Type) (g :: Type -> Type) (h :: a ~> f b)
              (transform :: forall z. f z ~> g z) (x :: U1 a).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: f z ~> g z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
  traversableNaturality strans _ _
    | Refl <- applicativeTransformerPure strans (SU1 :: Sing ('U1 :: U1 b))
    = Refl

  traversableIdentity SU1 = Refl

  traversableComposition :: forall p q r (f :: Type -> Type) (g :: Type -> Type)
                                   (h :: p ~> f q) (i :: q ~> g r) (x :: U1 p).
                            (VApplicative f, VApplicative g)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition _ si _
    | Refl <- applicativeHomomorphism @f sTraverseI sU1q
    , Refl <- applicativeFmap @f sTraverseI (sPure sU1q)
    = Refl
    where
      sTraverseI :: Sing (TraverseSym1 i :: U1 q ~> g (U1 r))
      sTraverseI = singFun1 (sTraverse si)

      sU1q :: Sing ('U1 :: U1 q)
      sU1q = SU1

instance VTraversable (K1 i' c) where
  traversableNaturality
    :: forall a b (f :: Type -> Type) (g :: Type -> Type) (h :: a ~> f b)
              (transform :: forall (z :: Type). f z ~> g z) (x :: K1 i' c a).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: f z ~> g z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
  traversableNaturality strans _ (SK1 (scc :: Sing (cc :: c)))
    | Refl <- applicativeTransformerPure strans (SK1 scc :: Sing ('K1 cc :: K1 i' c b))
    = Refl

  traversableIdentity SK1{} = Refl

  traversableComposition :: forall p q r (f :: Type -> Type) (g :: Type -> Type)
                                   (h :: p ~> f q) (i :: q ~> g r) (x :: K1 i' c p).
                            (VApplicative f, VApplicative g)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition _ si (SK1 (sc :: Sing xx))
    | Refl <- applicativeHomomorphism @f sTraverseI sK1q
    , Refl <- applicativeFmap @f sTraverseI (sPure sK1q)
    = Refl
    where
      sTraverseI :: Sing (TraverseSym1 i :: K1 i' c q ~> g (K1 i' c r))
      sTraverseI = singFun1 (sTraverse si)

      sK1q :: Sing ('K1 xx :: K1 i' c q)
      sK1q = SK1 sc

instance VTraversable f => VTraversable (M1 i' c' f) where
  traversableNaturality
    :: forall p q (cf :: Type -> Type) (cg :: Type -> Type) (h :: p ~> cf q)
              (transform :: forall (z :: Type). cf z ~> cg z) (x :: M1 i' c' f p).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: cf z ~> cg z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
  traversableNaturality strans sh (SM1 sfp)
    | Refl <- traversableNaturalityPush strans
                (singFun1 @(M1Sym0 :: f q ~> M1 i' c' f q) SM1) (sTraverse sh sfp)
    , Refl <- traversableNaturality @f strans sh sfp
    = Refl

  traversableIdentity (SM1 sx) | Refl <- traversableIdentity sx = Refl

  traversableComposition :: forall p q r (cf :: Type -> Type) (cg :: Type -> Type)
                                   (h :: p ~> cf q) (i :: q ~> cg r) (x :: M1 i' c' f p).
                            (VApplicative cf, VApplicative cg)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition sh si (SM1 sfp)
    | Refl <- traversableComposition sh si sfp
    , Refl <- traversableCompositionWrap sh si
                (singFun1 @(M1Sym0 :: f _ ~> M1 i' c' f _) SM1) sfp (const Refl)
    = Refl

instance (VTraversable f, VTraversable g) => VTraversable (f :*: g) where
  traversableNaturality
    :: forall p q (cf :: Type -> Type) (cg :: Type -> Type) (h :: p ~> cf q)
              (transform :: forall (z :: Type). cf z ~> cg z) (x :: (f :*: g) p).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: cf z ~> cg z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
  traversableNaturality strans sh ((sfp :: Sing fp) :%*: sgp)
    | Refl <- applicativeTransformerAp strans
                (sFmap sProdFun sTraverseHFp) (sTraverseH sgp)
    , Refl <- traversableNaturalityPush strans sProdFun (sTraverseHFp)
    , Refl <- traversableNaturality @f strans sh sfp
    , Refl <- traversableNaturality @g strans sh sgp
    = Refl
    where
      sProdFun :: Sing ((:*:@#@$) :: f q ~> g q ~> (f :*: g) q)
      sProdFun = singFun2 (:%*:)

      sTraverseH :: forall fg (z :: fg p). STraversable fg
                 => Sing z -> Sing (Traverse h z)
      sTraverseH = sTraverse sh

      sTraverseHFp :: Sing (Traverse h fp)
      sTraverseHFp = sTraverseH sfp

  traversableIdentity (sx :%*: sy)
    | Refl <- traversableIdentity sx
    , Refl <- traversableIdentity sy
    = Refl

  traversableComposition :: forall p q r (cf :: Type -> Type) (cg :: Type -> Type)
                                   (h :: p ~> cf q) (i :: q ~> cg r) (x :: (f :*: g) p).
                            (VApplicative cf, VApplicative cg)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition h i ((fp :: Sing fp) :%*: (gp :: Sing gp))
    | Refl <- traversableComposition h i fp
    , Refl <- traversableComposition h i gp
    {-
    (1)  fmap (<*>) (fmap (fmap (:*:)) (fmap (traverse i) (traverse h fp))) <*> fmap (traverse i) (traverse h gp)                                     [apFmap]
    (2)  pure (<*>) <*> fmap (fmap (:*:)) (fmap (traverse i) (traverse h fp)) <*> fmap (traverse i) (traverse h gp)                                   [apFmap]
    (3)  pure (<*>) <*> (pure (fmap (:*:)) <*> fmap (traverse i) (traverse h fp)) <*> fmap (traverse i) (traverse h gp)                               [apComp]
    (4)  pure (.) <*> pure (<*>) <*> pure (fmap (:*:)) <*> fmap (traverse i) (traverse h fp) <*> fmap (traverse i) (traverse h gp)                    [apHom (2)]
    (5)  pure ((.) (<*>) (fmap (:*:))) <*> fmap (traverse i) (traverse h fp) <*> fmap (traverse i) (traverse h gp)                                    [apFmap]
    (6)  pure ((.) (<*>) (fmap (:*:))) <*> (pure (traverse i) <*> traverse h fp) <*> fmap (traverse i) (traverse h gp)                                [apComp]
    (7)  pure (.) <*> pure ((.) (<*>) (fmap (:*:))) <*> pure (traverse i) <*> traverse h fp <*> fmap (traverse i) (traverse h gp)                     [apHom (2)]
    (8)  pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp <*> fmap (traverse i) (traverse h gp)                                     [apFmap]
    (9)  pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp <*> (pure (traverse i) <*> traverse h gp)                                 [apComp]
    (10) pure (.) <*> (pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp) <*> pure (traverse i) <*> traverse h gp                    [apInt]
    (11) pure ((&) (traverse i)) <*> (pure (.) <*> (pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp)) <*> traverse h gp            [apComp]
    (12) pure (.) <*> pure ((&) (traverse i)) <*> pure (.) <*> (pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp) <*> traverse h gp [apHom (2)]
    (13) pure ((.) ((&) (traverse i)) (.)) <*> (pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp) <*> traverse h gp                 [apComp]
    (14) pure (.) <*> pure ((.) ((&) (traverse i)) (.)) <*> pure ((.) ((.) (<*>) (fmap (:*:))) (traverse i)) <*> traverse h fp <*> traverse h gp      [apHom (2)]
    (15) pure ((.) ((.) ((&) (traverse i)) (.)) ((.) ((.) (<*>) (fmap (:*:))) (traverse i))) <*> traverse h fp <*> traverse h gp                      [def ((:*:) traverse), funExt2]
    (16) pure ((.) ((.) (traverse i)) (:*:)) <*> traverse h fp <*> traverse h gp                                                                      [apHom (2)]
    (17) pure (.) <*> pure ((.) (traverse i)) <*> pure (:*:) <*> traverse h fp <*> traverse h gp                                                      [apComp]
    (18) pure ((.) (traverse i)) <*> (pure (:*:) <*> traverse h fp) <*> traverse h gp                                                                 [apFmap]
    (19) pure ((.) (traverse i)) <*> fmap (:*:) (traverse h fp) <*> traverse h gp                                                                     [apHom]
    (20) pure (.) <*> pure (traverse i) <*> fmap (:*:) (traverse h fp) <*> traverse h gp                                                              [apComp]
    (21) pure (traverse i) <*> (fmap (:*:) (traverse h fp) <*> traverse h gp)                                                                         [apFmap]
         fmap (traverse i) (fmap (:*:) (traverse h fp) <*> traverse h gp)
    -}
      -- (1)
    , Refl <- applicativeFmap @cf sApFun (sFmap sFmapProdFun (sFmap sTraverseIFun (sTraverse h fp)))
      -- (2)
    , Refl <- applicativeFmap @cf sFmapProdFun (sFmap sTraverseIFun (sTraverse h fp))
      -- (3)
    , Refl <- applicativeComposition @cf (sPure sApFun) (sPure sFmapProdFun) (sFmap sTraverseIFun (sTraverse h fp))
      -- (4)
    , Refl <- applicativeHomomorphism @cf (sCompFun @(cg (f r))) (sApFun @cg @(g r) @((f :*: g) r))
    , Refl <- applicativeHomomorphism @cf (sCompFun @@ sApFun) sFmapProdFun
      -- (5)
    , Refl <- applicativeFmap @cf sTraverseIFun (sTraverse h fp)
      -- (6)
    , Refl <- applicativeComposition @cf (sPure (sCompFun @@ sApFun @@ sFmapProdFun)) (sPure sTraverseIFun) (sTraverse h fp)
      -- (7)
    , Refl <- applicativeHomomorphism @cf (sCompFun @(f q)) (sCompFun @@ sApFun @@ sFmapProdFun)
    , Refl <- applicativeHomomorphism @cf (sCompFun @@ (sCompFun @@ sApFun @@ sFmapProdFun)) sTraverseIFun
      -- (8)
    , Refl <- applicativeFmap @cf sTraverseIFun (sTraverse h gp)
      -- (9)
    , Refl <- applicativeComposition @cf (sPure (sCompFun @@ (sCompFun @@ sApFun @@ sFmapProdFun) @@ sTraverseIFun) %<*> sTraverse h fp) (sPure sTraverseIFun) (sTraverse h gp)
      -- (10)
    , Refl <- applicativeInterchange @cf (sPure sCompFun %<*> (sPure (sCompFun @@ (sCompFun @@ sApFun @@ sFmapProdFun) @@ sTraverseIFun) %<*> sTraverse h fp)) sTraverseIFun
      -- (11)
    , Refl <- applicativeComposition @cf (sPure sAndTraverseIFun) (sPure sCompFun) (sPure (sCompFun @@ (sCompFun @@ sApFun @@ sFmapProdFun) @@ sTraverseIFun) %<*> sTraverse h fp)
      -- (12)
    , Refl <- applicativeHomomorphism @cf (sCompFun @(cg (g r) ~> cg ((f :*: g) r))) sAndTraverseIFun
    , Refl <- applicativeHomomorphism @cf (sCompFun @@ sAndTraverseIFun) sCompFun
      -- (13)
    , Refl <- applicativeComposition @cf (sPure (sCompFun @@ sAndTraverseIFun @@ sCompFun))
                                         (sPure (sCompFun @@ (sCompFun @@ sApFun @@ sFmapProdFun) @@ sTraverseIFun))
                                         (sTraverse h fp)
      -- (14)
    , Refl <- applicativeHomomorphism @cf (sCompFun @(f q)) (sCompFun @@ sAndTraverseIFun @@ sCompFun)
    , Refl <- applicativeHomomorphism @cf (sCompFun @@ (sCompFun @@ sAndTraverseIFun @@ sCompFun))
                                          (sCompFun @@ (sCompFun @@ sApFun @@ sFmapProdFun) @@ sTraverseIFun)
      -- (15)
    , Refl <- --     (Pure ((.@#@$$$) ((.@#@$$$) ((&@#@$$) (TraverseSym1 i)) (.@#@$)) ((.@#@$$$) ((.@#@$$$) (<*>@#@$) (FmapSym1 (:*:@#@$))) (TraverseSym1 i))) <*> Traverse h fp <*> Traverse h gp)
              -- :~: (Pure ((.@#@$$$) ((.@#@$$) (TraverseSym1 i)) (:*:@#@$)) <*> Traverse h fp <*> Traverse h gp)
              Refl @(<*>@#@$)
                `apply` (Refl @(<*>@#@$)
                          `apply` (Refl @PureSym0
                                    `apply` funExt2 @(f q) @(g q) @(cg ((f :*: g) r))
                                                    @((.@#@$$$) ((.@#@$$$) ((&@#@$$) (TraverseSym1 i)) (.@#@$)) ((.@#@$$$) ((.@#@$$$) (<*>@#@$) (FmapSym1 (:*:@#@$))) (TraverseSym1 i)))
                                                    @((.@#@$$$) ((.@#@$$) (TraverseSym1 i)) (:*:@#@$))
                                                    lemma)
                          `apply` Refl @(Traverse h fp))
                `apply` Refl @(Traverse h gp)
      -- (16)
    , Refl <- applicativeHomomorphism @cf (sCompFun @(f q)) (sCompFun @(g q) @@ sTraverseIFun @(f :*: g))
    , Refl <- applicativeHomomorphism @cf (sCompFun @@ (sCompFun @@ sTraverseIFun)) sProdQFun
      -- (17)
    , Refl <- applicativeComposition @cf (sPure (sCompFun @@ sTraverseIFun)) (sPure sProdQFun) (sTraverse h fp)
      -- (18)
    , Refl <- applicativeFmap @cf sProdQFun (sTraverse h fp)
      -- (19)
    , Refl <- applicativeHomomorphism @cf (sCompFun @(g q)) (sTraverseIFun @(f :*: g))
      -- (20)
    , Refl <- applicativeComposition @cf (sPure sTraverseIFun) (sFmap sProdQFun (sTraverse h fp)) (sTraverse h gp)
      -- (21)
    , Refl <- applicativeFmap @cf sTraverseIFun (sFmap sProdQFun (sTraverse h fp) %<*> sTraverse h gp)

    , Refl <- applicativeLiftA2 @cf sApFun (sFmap sFmapProdFun (sFmap sTraverseIFun (sTraverse h fp)))
                                           (sFmap sTraverseIFun (sTraverse h gp))
    = Refl
    where
      lemma :: forall (z1 :: f q) (z2 :: g q).
               Sing z1 -> Sing z2
            -> (Fmap (:*:@#@$) (Traverse i z1) <*> Traverse i z2) :~: Traverse i (z1 ':*: z2)
      lemma _ _ = Refl

      sCompFun :: forall z1 z2 z3. Sing ((.@#@$) :: (z2 ~> z3) ~> (z1 ~> z2) ~> z1 ~> z3)
      sCompFun = singFun3 (%.)

      sApFun :: forall j z1 z2. SApplicative j => Sing ((<*>@#@$) :: j (z1 ~> z2) ~> j z1 ~> j z2)
      sApFun = singFun2 (%<*>)

      sFmapFun :: forall j z1 z2. SFunctor j => Sing (FmapSym0 :: (z1 ~> z2) ~> j z1 ~> j z2)
      sFmapFun = singFun2 sFmap

      sProdFun :: forall z. Sing ((:*:@#@$) :: f z ~> g z ~> (f :*: g) z)
      sProdFun = singFun2 (:%*:)

      sProdQFun :: Sing ((:*:@#@$) :: f q ~> g q ~> (f :*: g) q)
      sProdQFun = sProdFun

      sFmapProdFun :: Sing (FmapSym1 (:*:@#@$) :: cg (f r) ~> cg (g r ~> (f :*: g) r))
      sFmapProdFun = sFmapFun @@ sProdFun

      sTraverseFun :: forall j app z1 z2. (STraversable j, SApplicative app)
                   => Sing (TraverseSym0 :: (z1 ~> app z2) ~> j z1 ~> app (j z2))
      sTraverseFun = singFun2 sTraverse

      sTraverseIFun :: forall j. STraversable j => Sing (TraverseSym1 i :: j q ~> cg (j r))
      sTraverseIFun = sTraverseFun @@ i

      sAndTraverseIFun :: Sing ((&@#@$$) (TraverseSym1 i) :: ((g q ~> cg (g r)) ~> (g q ~> cg ((f :*: g) r))) ~> g q ~> cg ((f :*: g) r))
      sAndTraverseIFun = singFun1 (sTraverseIFun %&)

instance (VTraversable f, VTraversable g) => VTraversable (f :+: g) where
  traversableNaturality
    :: forall p q (cf :: Type -> Type) (cg :: Type -> Type) (h :: p ~> cf q)
              (transform :: forall (z :: Type). cf z ~> cg z) (x :: (f :+: g) p).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: cf z ~> cg z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
  traversableNaturality strans sh slr =
    case slr of
      SL1 sfp
        |  Refl <- traversableNaturalityPush strans
                     (singFun1 @(L1Sym0 :: f q ~> (f :+: g) q) SL1) (sTraverse sh sfp)
        ,  Refl <- traversableNaturality @f strans sh sfp
        -> Refl
      SR1 sgp
        |  Refl <- traversableNaturalityPush strans
                     (singFun1 @(R1Sym0 :: g q ~> (f :+: g) q) SR1) (sTraverse sh sgp)
        ,  Refl <- traversableNaturality @g strans sh sgp
        -> Refl

  traversableIdentity (SL1 sx) | Refl <- traversableIdentity sx = Refl
  traversableIdentity (SR1 sy) | Refl <- traversableIdentity sy = Refl

  traversableComposition :: forall p q r (cf :: Type -> Type) (cg :: Type -> Type)
                                   (h :: p ~> cf q) (i :: q ~> cg r) (x :: (f :+: g) p).
                            (VApplicative cf, VApplicative cg)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition sh si slr =
    case slr of
      SL1 (sl :: Sing left)
        |  Refl <- traversableComposition @f sh si sl
        ,  Refl <- go (singFun1 @L1Sym0 SL1) sl (const Refl)
        -> Refl
      SR1 (sr :: Sing right)
        |  Refl <- traversableComposition @g sh si sr
        ,  Refl <- go (singFun1 @R1Sym0 SR1) sr (const Refl)
        -> Refl
    where
      go :: forall (fg :: Type -> Type)
                   (lr1Sym0 :: forall z. fg z ~> (f :+: g) z)
                   (lrp :: fg p).
            VTraversable fg
         => (forall z. Sing (lr1Sym0 :: fg z ~> (f :+: g) z))
         -> Sing lrp
         -> (forall (z :: fg q). Sing z -> Fmap lr1Sym0 (Traverse i z) :~: Traverse i (lr1Sym0 @@ z))
         ->     Fmap (FmapSym1 lr1Sym0) (Fmap (TraverseSym1 i) (Traverse h lrp))
            :~: Fmap (TraverseSym1 i) (Fmap lr1Sym0 (Traverse h lrp))
      go = traversableCompositionWrap sh si

-- A particular lemma that is used in the instances for (:+:) and Rec1.
traversableCompositionWrap
  :: forall p q r (cf :: Type -> Type) (cg :: Type -> Type)
            (h :: p ~> cf q) (i :: q ~> cg r)
            (outer :: Type -> Type)
            (inner :: Type -> Type)
            (inOutSym0 :: forall z. inner z ~> outer z)
            (ip :: inner p).
     ( VTraversable inner, VTraversable outer
     , VApplicative cf, VApplicative cg )
  => Sing h -> Sing i
  -> (forall z. Sing (inOutSym0 :: inner z ~> outer z))
  -> Sing ip
  -> (forall (z :: inner q). Sing z -> Fmap inOutSym0 (Traverse i z) :~: Traverse i (inOutSym0 @@ z))
  ->     Fmap (FmapSym1 inOutSym0) (Fmap (TraverseSym1 i) (Traverse h ip))
     :~: Fmap (TraverseSym1 i) (Fmap inOutSym0 (Traverse h ip))
traversableCompositionWrap sh si sIOFun sip lemma
  | Refl <- fmapCompose @cf (singFun1 @(FmapSym1 inOutSym0) (sFmap sIOFun))
                            sTraverseIFun sTraverseHIp
  , Refl <- --     Fmap (FmapSym1 inOutSym0 .@#@$$$ TraverseSym1 i) (Traverse h ip)
            -- :~: Fmap (TraverseSym1 i .@#@$$$ inOutSym0) (Traverse h ip)
            Refl @FmapSym0
              `apply` funExt @(inner q) @(cg (outer r))
                             @(FmapSym1 inOutSym0 .@#@$$$ TraverseSym1 i)
                             @(TraverseSym1 i .@#@$$$ inOutSym0)
                             lemma
              `apply` Refl @(Traverse h ip)
  , Refl <- fmapCompose @cf sTraverseIFun sIOFun sTraverseHIp
  = Refl
  where
    sTraverseHIp :: Sing (Traverse h ip)
    sTraverseHIp = sTraverse sh sip

    sTraverseIFun :: forall hk. STraversable hk
                  => Sing (TraverseSym1 i :: hk q ~> cg (hk r))
    sTraverseIFun = singFun1 (sTraverse si)

-- A particular lemma that is used in several traversableNaturality proofs.
traversableNaturalityPush :: forall a b (f :: Type -> Type) (g :: Type -> Type)
                                    (transform :: forall (z :: Type). f z ~> g z)
                                    (con :: a ~> b) (x :: f a).
                             VApplicativeTransformer transform
                          => (forall z. Sing (transform :: f z ~> g z)) -> Sing con -> Sing x
                          -> transform @@ Fmap con x :~: Fmap con (transform @@ x)
traversableNaturalityPush strans scon sx
  | Refl <- applicativeFmap scon sx
  , Refl <- applicativeTransformerAp strans (sPure scon) sx
  , Refl <- applicativeTransformerPure strans scon
  , Refl <- applicativeFmap scon (strans @@ sx)
  = Refl

instance VTraversable Par1 where
  traversableNaturality strans sh (SPar1 spp)
    | Refl <- traversableNaturalityPush strans (singFun1 @Par1Sym0 SPar1) (sh @@ spp)
    = Refl

  traversableIdentity SPar1{} = Refl

  traversableComposition :: forall p q r (f :: Type -> Type) (g :: Type -> Type)
                                   (h :: p ~> f q) (i :: q ~> g r) (x :: Par1 p).
                            (VApplicative f, VApplicative g)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition sh si (SPar1 (sx :: Sing xx))
    | Refl <- fmapCompose @f (singFun1 @(FmapSym1 Par1Sym0) (sFmap sPar1Fun)) si shx
    , Refl <- --     Fmap (FmapSym1 Par1Sym0 .@#@$$$ i) (h @@ xx)
              -- :~: Fmap (Traverse i .@#@$$$ Par1Sym0) (h @@ xx)
              Refl @FmapSym0
                `apply` funExt @q @(g (Par1 r))
                               @(FmapSym1 Par1Sym0 .@#@$$$ i)
                               @(TraverseSym1 i .@#@$$$ Par1Sym0)
                               lemma
                `apply` Refl @(h @@ xx)
    , Refl <- fmapCompose @f (singFun1 @(TraverseSym1 i) (sTraverse si)) sPar1Fun shx
    = Refl
    where
      lemma :: forall (z :: q). Sing z -> Fmap Par1Sym0 (i @@ z) :~: Traverse i ('Par1 z)
      lemma _ = Refl

      shx :: Sing (h @@ xx)
      shx = sh @@ sx

      sPar1Fun :: Sing Par1Sym0
      sPar1Fun = singFun1 SPar1

instance VTraversable f => VTraversable (Rec1 f) where
  traversableNaturality strans sh (SRec1 sfp)
    | Refl <- traversableNaturalityPush strans
                (singFun1 @Rec1Sym0 SRec1) (sTraverse sh sfp)
    , Refl <- traversableNaturality @f strans sh sfp
    = Refl

  traversableIdentity (SRec1 sx) | Refl <- traversableIdentity sx = Refl

  traversableComposition :: forall p q r (cf :: Type -> Type) (cg :: Type -> Type)
                                   (h :: p ~> cf q) (i :: q ~> cg r) (x :: Rec1 f p).
                            (VApplicative cf, VApplicative cg)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition sh si (SRec1 sfp)
    | Refl <- traversableComposition sh si sfp
    , Refl <- traversableCompositionWrap sh si (singFun1 @Rec1Sym0 SRec1) sfp (const Refl)
    = Refl

instance (VTraversable f, VTraversable g) => VTraversable (f :.: g) where
  traversableNaturality
    :: forall p q (cf :: Type -> Type) (cg :: Type -> Type) (h :: p ~> cf q)
              (transform :: forall (z :: Type). cf z ~> cg z) (x :: (f :.: g) p).
       VApplicativeTransformer transform
    => (forall z. Sing (transform :: cf z ~> cg z)) -> Sing h -> Sing x
    -> (transform @@ Traverse h x) :~: Traverse (transform .@#@$$$ h) x
  traversableNaturality strans sh (SComp1 (sfgp :: Sing (fgp :: f (g p))))
    | Refl <- traversableNaturalityPush strans
                (singFun1 @Comp1Sym0 SComp1) (sTraverse sTraverseHFun sfgp)
    , Refl <- traversableNaturality @f strans sTraverseHFun sfgp
    , Refl <- --     Traverse (transform .@#@$$$ TraverseSym1 h) fgp
              -- :~: Traverse (TraverseSym1 (transform .@#@$$$ h)) fgp
              Refl @TraverseSym0
                `apply` funExt @(g p) @(cg (g q))
                               @(transform .@#@$$$ TraverseSym1 h)
                               @(TraverseSym1 (transform .@#@$$$ h))
                               (traversableNaturality @g strans sh)
                `apply` Refl @fgp
    = Refl
    where
      sTraverseHFun :: Sing (TraverseSym1 h :: g p ~> cf (g q))
      sTraverseHFun = singFun1 (sTraverse sh)

  traversableIdentity (SComp1 (sfgp :: Sing (fgp :: (f (g p)))))
    | Refl <- -- Traverse (TraverseSym1 IdentitySym0) fgp :~: Traverse IdentitySym0 fgp
              Refl @TraverseSym0
                `apply` funExt @(g p) @(Identity (g p))
                               @(TraverseSym1 IdentitySym0) @IdentitySym0
                               traversableIdentity
                `apply` Refl @fgp
    , Refl <- traversableIdentity @f sfgp
    = Refl

  traversableComposition :: forall p q r (cf :: Type -> Type) (cg :: Type -> Type)
                                   (h :: p ~> cf q) (i :: q ~> cg r) (x :: (f :.: g) p).
                            (VApplicative cf, VApplicative cg)
                         => Sing h -> Sing i -> Sing x
                         ->     Traverse (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h) x
                            :~: 'Compose (Fmap (TraverseSym1 i) (Traverse h x))
  traversableComposition sh si (SComp1 (sfgp :: Sing fgp))
  {-
  (1) traverse (Compose . fmap i . h) (Comp1 fgp)                                            [def, Traversable (f :.: g)]
      fmap Comp1 (traverse (traverse (Compose . fmap i . h)) fgp)                            [travComp, funExt]
  (2) fmap Comp1 (traverse (Compose . fmap (traverse i) . traverse h) fgp)                   [travComp]
  (3) fmap Comp1 (Compose (fmap (traverse (traverse i)) (traverse (traverse h) fgp)))        [def, Functor (Compose f g)]
      Compose (fmap (fmap Comp1) (fmap (traverse (traverse i)) (traverse (traverse h) fgp))) [fmapComp]
  (4) Compose (fmap (fmap Comp1 . traverse (traverse i)) (traverse (traverse h) fgp))        [def, Traversable (f :.: g), funExt]
  (5) Compose (fmap (traverse i . Comp1) (traverse (traverse h) fgp))                        [fmapComp]
      Compose (fmap (traverse i) (fmap Comp1 (traverse (traverse h) fgp)))                   [def, Traversable (f :.: g)]
      Compose (fmap (traverse i) (traverse h (Comp1 fgp)))
  -}
    | -- (1)
      Refl <- --     Fmap Comp1Sym0 (Traverse (TraverseSym1 (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h)) fgp)
              -- :~: Fmap Comp1Sym0 (Traverse (ComposeSym0 .@#@$$$ FmapSym1 (TraverseSym1 i) .@#@$$$ TraverseSym1 h) fgp)
              Refl @(FmapSym1 Comp1Sym0)
                `apply` (Refl @TraverseSym0
                          `apply` funExt @(g p) @(Compose cf cg (g r))
                                         @(TraverseSym1 (ComposeSym0 .@#@$$$ FmapSym1 i .@#@$$$ h))
                                         @(ComposeSym0 .@#@$$$ FmapSym1 (TraverseSym1 i) .@#@$$$ TraverseSym1 h)
                                         (traversableComposition sh si)
                          `apply` Refl @fgp)
      -- (2)
    , Refl <- traversableComposition @f sTraverseHFun sTraverseIFun sfgp
      -- (3)
    , Refl <- fmapCompose @cf (singFun1 @(FmapSym1 Comp1Sym0) (sFmap sComp1Fun))
                              (singFun1 @(TraverseSym1 (TraverseSym1 i)) (sTraverse sTraverseIFun))
                              sTraverseSTraverseHfgp
      -- (4)
    , Refl <- --     Fmap (FmapSym1 Comp1Sym0 .@#@$$$ TraverseSym1 (TraverseSym1 i)) (Traverse (TraverseSym1 h) fgp)
              -- :~: Fmap (TraverseSym1 i .@#@$$$ Comp1Sym0)                         (Traverse (TraverseSym1 h) fgp)
              Refl @FmapSym0
                `apply` funExt @(f (g q)) @(cg ((f :.: g) r))
                               @(FmapSym1 Comp1Sym0 .@#@$$$ TraverseSym1 (TraverseSym1 i))
                               @(TraverseSym1 i .@#@$$$ Comp1Sym0)
                               (const Refl)
                `apply` Refl @(Traverse (TraverseSym1 h) fgp)
      -- (5)
    , Refl <- fmapCompose sTraverseIFun sComp1Fun sTraverseSTraverseHfgp
    = Refl
    where
      sTraverseHFun :: Sing (TraverseSym1 h :: g p ~> cf (g q))
      sTraverseHFun = singFun1 (sTraverse sh)

      sTraverseIFun :: forall hk. STraversable hk => Sing (TraverseSym1 i :: hk q ~> cg (hk r))
      sTraverseIFun = singFun1 (sTraverse si)

      sComp1Fun :: Sing Comp1Sym0
      sComp1Fun = singFun1 SComp1

      sTraverseSTraverseHfgp :: Sing (Traverse (TraverseSym1 h) fgp)
      sTraverseSTraverseHfgp = sTraverse sTraverseHFun sfgp
